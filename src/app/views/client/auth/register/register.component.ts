// Angular
import {
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit,
  ViewEncapsulation,
} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
// NGRX
import { Store } from '@ngrx/store';
// Translate
import { TranslateService } from '@ngx-translate/core';
import { Subject } from 'rxjs';
// RxJS
import { finalize, first, takeUntil, tap } from 'rxjs/operators';
// Auth
import { AuthNoticeService, AuthService } from '../../../../core/api/auth';
import { Register } from '../../../../core/api/auth/store/actions';
import { CurrentUser } from '../../../../core/api/auth/types';
import { IAppState } from '../../../../core/reducers';
import { ConfirmPasswordValidator } from './confirm-password.validator';

@Component({
  selector: 'kt-register',
  templateUrl: './register.component.html',
  encapsulation: ViewEncapsulation.None,
})
export class RegisterComponent implements OnInit, OnDestroy {
  registerForm: FormGroup;
  loading = false;
  errors: any = [];

  private unsubscribe: Subject<any>; // Read more: => https://brianflove.com/2016/12/11/anguar-2-unsubscribe-observables/

  constructor(
    private authNoticeService: AuthNoticeService,
    private translate: TranslateService,
    private router: Router,
    private auth: AuthService,
    private store: Store<IAppState>,
    private fb: FormBuilder,
    private cdr: ChangeDetectorRef
  ) {
    this.unsubscribe = new Subject();
  }

  /*
   * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
   */

  /**
   * On init
   */
  ngOnInit() {
    this.initRegisterForm();
  }

  /*
   * On destroy
   */
  ngOnDestroy(): void {
    this.unsubscribe.next();
    this.unsubscribe.complete();
    this.loading = false;
  }

  /**
   * Form initalization
   * Default params, validators
   */
  initRegisterForm() {
    this.registerForm = this.fb.group(
      {
        fullname: [
          '',
          Validators.compose([
            // Validators.required,
            Validators.minLength(3),
            Validators.maxLength(100),
          ]),
        ],
        email: [
          '',
          Validators.compose([
            Validators.required,
            Validators.email,
            Validators.minLength(3),
            // https://stackoverflow.com/questions/386294/what-is-the-maximum-length-of-a-valid-email-address
            Validators.maxLength(320),
          ]),
        ],
        username: [
          '',
          Validators.compose([
            // Validators.required,
            Validators.minLength(3),
            Validators.maxLength(100),
          ]),
        ],
        password: [
          '',
          Validators.compose([
            Validators.required,
            Validators.minLength(3),
            Validators.maxLength(100),
          ]),
        ],
        confirmPassword: [
          '',
          Validators.compose([
            Validators.required,
            Validators.minLength(3),
            Validators.maxLength(100),
          ]),
        ],
        agree: [false, Validators.compose([Validators.required])],
      },
      {
        validator: ConfirmPasswordValidator.MatchPassword,
      }
    );
  }

  /**
   * Form Submit
   */
  submit() {
    const controls = this.registerForm.controls;
    
    // check form
    if (this.registerForm.invalid) {
      Object.keys(controls).forEach((controlName) =>
        controls[controlName].markAsTouched()
      );
      return;
    }

    this.loading = true;

    if (!controls.agree.value) {
      // you must agree the terms and condition
      // checkbox cannot work inside mat-form-field https://github.com/angular/material2/issues/7891
      this.authNoticeService.setNotice(
        'You must agree the terms and condition',
        'danger'
      );
      return;
    }

    // const _user: CurrentUser = new CurrentUser();
    // _user.clear();
    // _user.email = controls.email.value;

    const userData = {
      Email : this.registerForm.get('email').value,
      Password : this.registerForm.get('password').value
    };

    console.log(userData);
    
    this.auth
    .register(userData)
    .pipe(
      takeUntil(this.unsubscribe),
      finalize(() => {
        this.loading = false;
        this.cdr.markForCheck();
      })
    )
    .subscribe(res => {
      if (res['Result'] === 0) {
        this.store.dispatch(new Register({ authToken: res['Token'] }));
        this.authNoticeService.setNotice(
          this.translate.instant('AUTH.REGISTER.SUCCESS'),
          'success'
        );
        this.router.navigateByUrl('/auth/login');
      }
      else {
        this.authNoticeService.setNotice(
          this.translate.instant('AUTH.VALIDATION.INVALID_LOGIN'),
          'danger'
        );
      }
    });
  }

  /**
   * Checking control validation
   *
   * @param controlName: string => Equals to formControlName
   * @param validationType: string => Equals to valitors name
   */
  isControlHasError(controlName: string, validationType: string): boolean {
    const control = this.registerForm.controls[controlName];
    if (!control) {
      return false;
    }

    const result =
      control.hasError(validationType) && (control.dirty || control.touched);
    return result;
  }
}
