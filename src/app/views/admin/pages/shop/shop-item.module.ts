import { NgModule } from '@angular/core';
import { ShopItemListComponent } from './shop-item-list/shop-item-list.component';
import { ShopItemEditComponent } from './shop-item-edit/shop-item-edit.component';

import { EffectsModule } from '@ngrx/effects';
import { InterceptService } from '../../../../core/_base/crud';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { PartialsModule } from '../../../partials/partials.module';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { CommonModule } from '@angular/common';
import { MatIconModule } from '@angular/material/icon';

import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatMenuModule } from '@angular/material/menu';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MatButtonModule } from '@angular/material/button';
import { MatTooltipModule } from '@angular/material/tooltip';
import {
  NgxPermissionsAllowStubModule,
  NgxPermissionsModule,
} from 'ngx-permissions';
import { MatInputModule } from '@angular/material/input';
import { environment } from '../../../../../environments/environment';
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { FakeApiService } from '../../../../core/_base/layout';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { StoreModule } from '@ngrx/store';
import {
  shopItemsReducer,
  ShopItemEffects,
  ShopItemsService,
} from '../../../../core/api';
import { MatTableModule } from '@angular/material/table';
import { RouterModule } from '@angular/router';
import { MaterialFileInputModule } from 'ngx-material-file-input';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    StoreModule.forFeature('shopItems', shopItemsReducer),
    EffectsModule.forFeature([ShopItemEffects]),
    MatTableModule,
    MatSortModule,
    MatFormFieldModule,
    PartialsModule,
    MatSelectModule,
    MatInputModule,
    MatButtonModule,
    CommonModule,
    MatIconModule,
    MatCheckboxModule,
    MatMenuModule,
    MatPaginatorModule,
    MatProgressSpinnerModule,
    MatTooltipModule,
    NgxPermissionsAllowStubModule,
    RouterModule.forChild([
      {
        path: '',
        component: ShopItemListComponent,
      },
      {
        path: 'edit/:id',
        component: ShopItemEditComponent,
      },
    ]),
    MaterialFileInputModule,
    ReactiveFormsModule,
  ],
  declarations: [ShopItemListComponent, ShopItemEditComponent],
  providers: [
    InterceptService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: InterceptService,
      multi: true,
    },
    ShopItemsService,
  ],
})
export class ShopItemAdminModule {}
