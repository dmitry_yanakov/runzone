import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import {
  ManyShopItemsDeleted,
  OneShopItemDeleted,
  ShopItemModel,
  ShopItemsDataSource,
  ShopItemsPageRequested,
  ShopItemsStatusUpdated,
} from '../../../../../core/api';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { SelectionModel } from '@angular/cdk/collections';
import { fromEvent, merge, of, Subscription } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import {
  LayoutUtilsService,
  MessageType,
  QueryParamsModel,
} from '../../../../../core/_base/crud';
import { TranslateService } from '@ngx-translate/core';
import { Store } from '@ngrx/store';
import { IAppState } from '../../../../../core/reducers';
import {
  debounceTime,
  delay,
  distinctUntilChanged,
  skip,
  take,
  tap,
} from 'rxjs/operators';

@Component({
  selector: 'kt-shop-item-list',
  templateUrl: './shop-item-list.component.html',
  styleUrls: ['./shop-item-list.component.scss'],
})
export class ShopItemListComponent implements OnInit {
  // Table fields
  dataSource: ShopItemsDataSource;
  displayedColumns = [
    'select',
    'id',
    'name',
    'quantity',
    'typeGoods',
    'price',
    'typePrice',
    'rotate',
    'priority',
    'type',
    'actions',
  ];
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild('sort1', { static: true }) sort: MatSort;
  // Filter fields
  @ViewChild('searchInput', { static: true }) searchInput: ElementRef;
  filterStatus = '';
  filterType = '';
  // Selection
  selection = new SelectionModel<ShopItemModel>(true, []);
  shopItemsResult: ShopItemModel[] = [];
  // Subscriptions
  private subscriptions: Subscription[] = [];

  /**
   * Component constructor
   *
   * @param dialog: MatDialog
   * @param snackBar: MatSnackBar
   * @param layoutUtilsService: LayoutUtilsService
   * @param translate: TranslateService
   * @param store: Store<AppState>
   */
  constructor(
    public dialog: MatDialog,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    public snackBar: MatSnackBar,
    private layoutUtilsService: LayoutUtilsService,
    private translate: TranslateService,
    private store: Store<IAppState>
  ) {}

  /**
   * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
   */

  /**
   * On init
   */
  ngOnInit() {
    // If the shopItem changes the sort order, reset back to the first page.
    const sortSubscription = this.sort.sortChange.subscribe(
      () => (this.paginator.pageIndex = 0)
    );
    this.subscriptions.push(sortSubscription);

    /* Data load will be triggered in two cases:
    - when a pagination event occurs => this.paginator.page
    - when a sort event occurs => this.sort.sortChange
    **/
    const paginatorSubscriptions = merge(
      this.sort.sortChange,
      this.paginator.page
    )
      .pipe(tap(() => this.loadShopItemsList()))
      .subscribe();
    this.subscriptions.push(paginatorSubscriptions);

    // Filtration, bind to searchInput
    const searchSubscription = fromEvent(
      this.searchInput.nativeElement,
      'keyup'
    )
      .pipe(
        // tslint:disable-next-line:max-line-length
        debounceTime(50), // The shopItem can type quite quickly in the input box, and that could trigger a lot of server requests. With this operator, we are limiting the amount of server requests emitted to a maximum of one every 150ms
        distinctUntilChanged(), // This operator will eliminate duplicate values
        tap(() => {
          this.paginator.pageIndex = 0;
          this.loadShopItemsList();
        })
      )
      .subscribe();
    this.subscriptions.push(searchSubscription);

    // Init DataSource
    this.dataSource = new ShopItemsDataSource(this.store);
    const entitiesSubscription = this.dataSource.entitySubject
      .pipe(skip(1), distinctUntilChanged())
      .subscribe((res) => {
        this.shopItemsResult = res;
      });
    this.subscriptions.push(entitiesSubscription);
    // First load
    of(undefined)
      .pipe(take(1), delay(1000))
      .subscribe(() => {
        // Remove this line, just loading imitation
        this.loadShopItemsList();
      }); // Remove this line, just loading imitation
  }

  /**
   * On Destroy
   */
  ngOnDestroy() {
    this.subscriptions.forEach((el) => el.unsubscribe());
  }

  /**
   * Load ShopItems List from services through data-source
   */
  loadShopItemsList() {
    this.selection.clear();
    const queryParams = new QueryParamsModel(
      this.filterConfiguration(),
      this.sort.direction,
      this.sort.active,
      this.paginator.pageIndex,
      this.paginator.pageSize
    );
    // Call request from server
    this.store.dispatch(new ShopItemsPageRequested({ page: queryParams }));
    this.selection.clear();
  }

  /**
   * Returns object for filter
   */
  filterConfiguration(): any {
    const filter: any = {};
    const searchText: string = this.searchInput.nativeElement.value;

    if (this.filterStatus && this.filterStatus.length > 0) {
      filter.status = +this.filterStatus;
    }

    if (this.filterType && this.filterType.length > 0) {
      filter.type = +this.filterType;
    }

    filter.shopItemname = searchText;
    if (!searchText) {
      return filter;
    }

    filter.email = searchText;
    filter.trophies = searchText;
    filter.clan = searchText;
    return filter;
  }

  /** ACTIONS */
  /**
   * Delete shopItem
   *
   * @param _item: ShopItemModel
   */
  deleteShopItem(_item: ShopItemModel) {
    const _title: string = this.translate.instant(
      'ECOMMERCE.CUSTOMERS.DELETE_CUSTOMER_SIMPLE.TITLE'
    );
    const _description: string = this.translate.instant(
      'ECOMMERCE.CUSTOMERS.DELETE_CUSTOMER_SIMPLE.DESCRIPTION'
    );
    const _waitDesciption: string = this.translate.instant(
      'ECOMMERCE.CUSTOMERS.DELETE_CUSTOMER_SIMPLE.WAIT_DESCRIPTION'
    );
    const _deleteMessage = this.translate.instant(
      'ECOMMERCE.CUSTOMERS.DELETE_CUSTOMER_SIMPLE.MESSAGE'
    );

    const dialogRef = this.layoutUtilsService.deleteElement(
      _title,
      _description,
      _waitDesciption
    );
    dialogRef.afterClosed().subscribe((res) => {
      if (!res) {
        return;
      }

      this.store.dispatch(new OneShopItemDeleted({ id: _item.id }));
      this.layoutUtilsService.showActionNotification(
        _deleteMessage,
        MessageType.Delete
      );
    });
  }

  /**
   * Delete selected shopItems
   */
  deleteShopItems() {
    const _title: string = this.translate.instant(
      'ECOMMERCE.USERS.DELETE_USER_MULTY.TITLE'
    );
    const _description: string = this.translate.instant(
      'ECOMMERCE.USERS.DELETE_USER_MULTY.DESCRIPTION'
    );
    const _waitDesciption: string = this.translate.instant(
      'ECOMMERCE.USERS.DELETE_USER_MULTY.WAIT_DESCRIPTION'
    );
    const _deleteMessage = this.translate.instant(
      'ECOMMERCE.USERS.DELETE_USER_MULTY.MESSAGE'
    );

    const dialogRef = this.layoutUtilsService.deleteElement(
      _title,
      _description,
      _waitDesciption
    );
    dialogRef.afterClosed().subscribe((res) => {
      if (!res) {
        return;
      }

      const idsForDeletion: number[] = [];
      for (let i = 0; i < this.selection.selected.length; i++) {
        idsForDeletion.push(this.selection.selected[i].id);
      }
      this.store.dispatch(new ManyShopItemsDeleted({ ids: idsForDeletion }));
      this.layoutUtilsService.showActionNotification(
        _deleteMessage,
        MessageType.Delete
      );
      this.selection.clear();
    });
  }

  /**
   * Fetch selected shopItems
   */
  fetchShopItems() {
    const messages = [];
    this.selection.selected.forEach((elem) => {
      messages.push({
        text: `${elem.name}`,
        id: elem.id.toString(),
      });
    });
    this.layoutUtilsService.fetchElements(messages);
  }

  /**
   * Show UpdateStatuDialog for selected shopItems
   */
  updateStatusForShopItems() {
    const _title = this.translate.instant(
      'ECOMMERCE.CUSTOMERS.UPDATE_STATUS.TITLE'
    );
    const _updateMessage = this.translate.instant(
      'ECOMMERCE.CUSTOMERS.UPDATE_STATUS.MESSAGE'
    );
    const _statuses = [
      { value: 0, text: 'Offline' },
      { value: 1, text: 'Online' },
    ];
    const _messages = [];

    this.selection.selected.forEach((elem) => {
      _messages.push({
        text: `${elem.name}`,
        id: elem.id.toString(),
      });
    });

    const dialogRef = this.layoutUtilsService.updateStatusForEntities(
      _title,
      _statuses,
      _messages
    );
    dialogRef.afterClosed().subscribe((res) => {
      if (!res) {
        this.selection.clear();
        return;
      }

      this.store.dispatch(
        new ShopItemsStatusUpdated({
          status: +res,
          shopItems: this.selection.selected,
        })
      );

      this.layoutUtilsService.showActionNotification(
        _updateMessage,
        MessageType.Update,
        10000,
        true,
        true
      );
      this.selection.clear();
    });
  }

  /**
   * Show add shopItem dialog
   */
  addShopItem() {}

  /**
   * Show Edit shopItem dialog and save after success close result
   * @param id: number
   */
  editShopItem(id: number) {
    this.router.navigate([this.router.url + '/edit', id], {
      relativeTo: this.activatedRoute,
    });
  }

  /**
   * Check all rows are selected
   */
  isAllSelected(): boolean {
    const numSelected = this.selection.selected.length;
    const numRows = this.shopItemsResult.length;
    return numSelected === numRows;
  }

  /**
   * Toggle all selections
   */
  masterToggle() {
    if (this.selection.selected.length === this.shopItemsResult.length) {
      this.selection.clear();
    } else {
      this.shopItemsResult.forEach((row) => this.selection.select(row));
    }
  }
  //0 - admin , 1 - active , 2 - verify , 3 - blocked , 4 - delete

  /** UI */
  /**
   * Retursn CSS Class Name by status
   *
   * @param status: number
   */
  getItemCssClassByStatus(status: number = 0): string {
    switch (status) {
      case 0:
        return 'metal';
      case 1:
        return 'success';
    }
    return '';
  }

  /**
   * Returns Item Status in string
   * @param status: number
   */
  getItemStatusString(status: number = 0): string {
    switch (status) {
      case 0:
        return 'Offline';
      case 1:
        return 'Online';
    }
    return '';
  }

  /**
   * Returns CSS Class Name by type
   * @param status: number
   */
  getItemCssClassByType(status: number = 0): string {
    switch (status) {
      case 0:
        return 'accent';
      case 1:
        return 'primary';
      case 2:
        return 'warn';
    }
    return '';
  }

  /**
   * Returns Item Type in string
   * 0 - admin , 1 - active , 2 - verify , 3 - blocked , 4 - delete
   * @param status: number
   */
  getItemTypeString(status: number = 0): string {
    switch (status) {
      case 1:
        return 'Gems';
      case 2:
        return 'Gold';
      case 3:
        return 'Eggs';
      case 4:
        return 'Daily Deals';
    }
  }
}
