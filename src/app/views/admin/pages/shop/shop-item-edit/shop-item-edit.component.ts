import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";


@Component({
  selector: 'kt-shop-item-edit',
  templateUrl: './shop-item-edit.component.html',
  styleUrls: ['./shop-item-edit.component.scss']
})
export class ShopItemEditComponent  {
  hasFormErrors: false;
  imageSrc: any;
  achievementForm: FormGroup;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private  achievementFB: FormBuilder,
  ) { }

  ngOnInit() {
    this.createForm();
  }

  onAlertClose($event: boolean) {

  }

  goBackWithoutId() {
    const url = `admin/shop`;
    this.router.navigateByUrl(url, { relativeTo: this.activatedRoute });
  }

  getComponentTitle() {
    return 'Edit Shop - ';
  }

  reset() {

  }

  onSumbit(b: boolean) {

  }

  onFileChange(event) {
    const reader = new FileReader();

    if(event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      reader.readAsDataURL(file);

      reader.onload = () => {

        this.imageSrc = reader.result as string;

        this.achievementForm.patchValue({
          fileSource: reader.result
        });

      };

    }
  }

  createForm() {
    this.achievementForm = this.achievementFB.group({
      name: new FormControl('', [Validators.required, Validators.minLength(3)]),
      quantityGoods: new FormControl('',Validators.required),
      priority: new FormControl('',Validators.required),
      price: new FormControl('',Validators.required),
      oldPrice: new FormControl('',Validators.required),
      badgeExtra: new FormControl('',Validators.required),
      categoryShop: new FormControl('',Validators.required),
      dailyDeals: new FormControl('',Validators.required),
      typeGoods: new FormControl('',Validators.required),
      typePrice: new FormControl('',Validators.required),
      typeOldPrice: new FormControl('',Validators.required),
      file: new FormControl('', [Validators.required]),
      fileSource: new FormControl('', [Validators.required])
    });


  }
}
