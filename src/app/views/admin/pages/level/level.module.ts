import { NgModule } from '@angular/core';
import { LevelEditComponent } from './level-edit/level-edit.component';
import { LevelListComponent } from './level-list/level-list.component';
import { RouterModule } from '@angular/router';
import { InterceptService } from '../../../../core/_base/crud';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import {
  LevelEffects,
  levelsReducer,
  LevelsService,
} from '../../../../core/api';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatTableModule } from '@angular/material/table';
import { MatIconModule } from '@angular/material/icon';
import { PartialsModule } from '../../../partials/partials.module';
import { MatFormFieldModule } from '@angular/material/form-field';
import { CommonModule } from '@angular/common';
import { MatSortModule } from '@angular/material/sort';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { NgxPermissionsAllowStubModule } from 'ngx-permissions';
import { MatButtonModule } from '@angular/material/button';
import { MatMenuModule } from '@angular/material/menu';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatInputModule } from '@angular/material/input';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatSelectModule } from '@angular/material/select';
import { MatListModule } from '@angular/material/list';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: '',
        component: LevelListComponent,
      },
      {
        path: 'edit/:id',
        component: LevelEditComponent,
      },
    ]),
    StoreModule.forFeature('levels', levelsReducer),
    EffectsModule.forFeature([LevelEffects]),
    MatPaginatorModule,
    MatProgressSpinnerModule,
    MatTableModule,
    MatIconModule,
    PartialsModule,
    MatFormFieldModule,
    CommonModule,
    MatSortModule,
    MatCheckboxModule,
    NgxPermissionsAllowStubModule,
    MatButtonModule,
    MatMenuModule,
    MatTooltipModule,
    MatInputModule,
    ReactiveFormsModule,
    MatSelectModule,
    MatListModule,
    FormsModule,
  ],
  declarations: [LevelListComponent, LevelEditComponent],
  providers: [
    InterceptService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: InterceptService,
      multi: true,
    },
    LevelsService,
  ],
})
export class LevelAdminModule {}
