import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'kt-level-edit',
  templateUrl: './level-edit.component.html',
  styleUrls: ['./level-edit.component.scss']
})
export class LevelEditComponent implements OnInit {
  unlockItems=[new FormControl('1'),new FormControl('2')];
  hasFormErrors: false;
  leverForm: FormGroup;

  constructor(private levelFB: FormBuilder,  private router: Router,
              private activatedRoute: ActivatedRoute,) { }

  ngOnInit() {
    this.createForm();
  }

  addUnlock() {
    this.unlockItems.push(new FormControl('1'));
  }

  onAlertClose($event: boolean) {

  }

  onSumbit(b: boolean) {

  }

  reset() {

  }

  goBackWithoutId() {
    const url = `admin/levels`;
    this.router.navigateByUrl(url, { relativeTo: this.activatedRoute });
  }

  getComponentTitle() {
    return 'Level Edit -'
  }

  createForm() {
    this.leverForm = this.levelFB.group({
      level: new FormControl('', Validators.required),
      xp: new FormControl('', Validators.required),
      gold: new FormControl('', Validators.required),
      gems: new FormControl('', Validators.required),
    })
  }
}
