import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import {
  LevelsDataSource,
  LevelModel,
  LevelsPageRequested,
  ManyLevelsDeleted,
  OneLevelDeleted,
} from '../../../../../core/api';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { SelectionModel } from '@angular/cdk/collections';
import { fromEvent, merge, of, Subscription } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import {
  LayoutUtilsService,
  MessageType,
  QueryParamsModel,
} from '../../../../../core/_base/crud';
import { TranslateService } from '@ngx-translate/core';
import { Store } from '@ngrx/store';
import { IAppState } from '../../../../../core/reducers';
import {
  debounceTime,
  delay,
  distinctUntilChanged,
  skip,
  take,
  tap,
} from 'rxjs/operators';

@Component({
  selector: 'kt-level-list',
  templateUrl: './level-list.component.html',
  styleUrls: ['./level-list.component.scss'],
})
export class LevelListComponent implements OnInit {
  dataSource: LevelsDataSource;
  displayedColumns = [
    'select',
    'id',
    'level',
    'xp',
    'gold',
    'gems',
    'egg',
    'actions',
  ];
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild('sort1', { static: true }) sort: MatSort;
  // Filter fields
  @ViewChild('searchInput', { static: true }) searchInput: ElementRef;

  selection = new SelectionModel<LevelModel>(true, []);
  clanLevelsResult: LevelModel[] = [];
  // Subscriptions
  private subscriptions: Subscription[] = [];

  constructor(
    public dialog: MatDialog,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private layoutUtilsService: LayoutUtilsService,
    private translate: TranslateService,
    private store: Store<IAppState>
  ) {}

  /**
   * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
   */

  /**
   * On init
   */
  ngOnInit() {
    // If the user changes the sort order, reset back to the first page.
    const sortSubscription = this.sort.sortChange.subscribe(
      () => (this.paginator.pageIndex = 0)
    );
    this.subscriptions.push(sortSubscription);

    /* Data load will be triggered in two cases:
    - when a pagination event occurs => this.paginator.page
    - when a sort event occurs => this.sort.sortChange
    **/
    const paginatorSubscriptions = merge(
      this.sort.sortChange,
      this.paginator.page
    )
      .pipe(tap(() => this.loadLevelsList()))
      .subscribe();
    this.subscriptions.push(paginatorSubscriptions);

    // Filtration, bind to searchInput
    const searchSubscription = fromEvent(
      this.searchInput.nativeElement,
      'keyup'
    )
      .pipe(
        // tslint:disable-next-line:max-line-length
        debounceTime(50), // The user can type quite quickly in the input box, and that could trigger a lot of server requests. With this operator, we are limiting the amount of server requests emitted to a maximum of one every 150ms
        distinctUntilChanged(), // This operator will eliminate duplicate values
        tap(() => {
          this.paginator.pageIndex = 0;
          this.loadLevelsList();
        })
      )
      .subscribe();
    this.subscriptions.push(searchSubscription);

    // Init DataSource
    this.dataSource = new LevelsDataSource(this.store);
    const entitiesSubscription = this.dataSource.entitySubject
      .pipe(skip(1), distinctUntilChanged())
      .subscribe((res) => {
        this.clanLevelsResult = res;
      });
    this.subscriptions.push(entitiesSubscription);
    // First load
    of(undefined)
      .pipe(take(1), delay(1000))
      .subscribe(() => {
        // Remove this line, just loading imitation
        this.loadLevelsList();
      }); // Remove this line, just loading imitation
  }

  /**
   * On Destroy
   */
  ngOnDestroy() {
    this.subscriptions.forEach((el) => el.unsubscribe());
  }

  /**
   * Load Customers List from services through data-source
   */
  loadLevelsList() {
    this.selection.clear();
    const queryParams = new QueryParamsModel(
      this.filterConfiguration(),
      this.sort.direction,
      this.sort.active,
      this.paginator.pageIndex,
      this.paginator.pageSize
    );
    // Call request from server
    this.store.dispatch(new LevelsPageRequested({ page: queryParams }));
    this.selection.clear();
  }

  /**
   * Returns object for filter
   */
  filterConfiguration(): any {
    const filter: any = {};
    const searchText: string = this.searchInput.nativeElement.value;

    filter.username = searchText;
    if (!searchText) {
      return filter;
    }

    filter.email = searchText;
    filter.trophies = searchText;
    filter.clanLevel = searchText;
    return filter;
  }

  /** ACTIONS */
  /**
   * Delete customer
   *
   * @param _item: CustomerModel
   */
  deleteLevel(_item: LevelModel) {
    return;
  }

  /**
   * Delete selected customers
   */
  deleteLevels() {
    return;
  }

  /**
   * Fetch selected customers
   */
  fetchLevels() {
    return;
  }

  /**
   * Show UpdateStatuDialog for selected customers
   */

  /**
   * Show add customer dialog
   */
  addLevel() {}

  /**
   * Show Edit customer dialog and save after success close result
   * @param id: number
   */
  editLevel(id: number) {
    this.router.navigate([this.router.url + '/edit', id], {
      relativeTo: this.activatedRoute,
    });
  }

  /**
   * Check all rows are selected
   */
  isAllSelected(): boolean {
    const numSelected = this.selection.selected.length;
    const numRows = this.clanLevelsResult.length;
    return numSelected === numRows;
  }

  /**
   * Toggle all selections
   */
  masterToggle() {
    if (this.selection.selected.length === this.clanLevelsResult.length) {
      this.selection.clear();
    } else {
      this.clanLevelsResult.forEach((row) => this.selection.select(row));
    }
  }
}
