import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CustomizeListComponent } from './customize-list/customize-list.component';
import { CustomizeEditComponent } from './customize-edit/customize-edit.component';
import { InterceptService } from '../../../../core/_base/crud';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import {
  CustomizeEffects,
  customizesReducer,
  CustomizesService,
} from '../../../../core/api';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatTableModule } from '@angular/material/table';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { MatButtonModule } from '@angular/material/button';
import { MatTooltipModule } from '@angular/material/tooltip';
import { NgxPermissionsRestrictStubModule } from 'ngx-permissions';
import { MatSortModule } from '@angular/material/sort';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { CommonModule } from '@angular/common';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { PartialsModule } from '../../../partials/partials.module';
import { MaterialFileInputModule } from 'ngx-material-file-input';
import { MatInputModule } from '@angular/material/input';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DeleteEntityDialogComponent } from '../../../partials/content/crud';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: '',
        component: CustomizeListComponent,
      },
      {
        path: 'edit/:id',
        component: CustomizeEditComponent,
      },
    ]),
    StoreModule.forFeature('customizes', customizesReducer),
    EffectsModule.forFeature([CustomizeEffects]),
    MatPaginatorModule,
    MatProgressSpinnerModule,
    MatTableModule,
    MatIconModule,
    MatMenuModule,
    MatButtonModule,
    MatTooltipModule,
    NgxPermissionsRestrictStubModule,
    MatSortModule,
    MatCheckboxModule,
    CommonModule,
    MatFormFieldModule,
    MatSelectModule,
    PartialsModule,
    MaterialFileInputModule,
    MatInputModule,
    ReactiveFormsModule,
    FormsModule,
  ],
  declarations: [CustomizeListComponent, CustomizeEditComponent],
  providers: [
    InterceptService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: InterceptService,
      multi: true,
    },
    CustomizesService,
  ],
  entryComponents: [DeleteEntityDialogComponent],
})
export class CustomizeAdminModule {}
