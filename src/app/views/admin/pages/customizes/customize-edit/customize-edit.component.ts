import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'kt-customize-edit',
  templateUrl: './customize-edit.component.html',
  styleUrls: ['./customize-edit.component.scss']
})
export class CustomizeEditComponent implements OnInit {
  hasFormErrors: false;
  imageSrc: any;
  achievementForm: FormGroup;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private  achievementFB: FormBuilder,
  ) { }

  ngOnInit() {
    this.createForm();
  }

  onAlertClose($event: boolean) {

  }

  goBackWithoutId() {
    const url = `admin/customizes`;
    this.router.navigateByUrl(url, { relativeTo: this.activatedRoute });
  }

  getComponentTitle() {
    return 'Customize Edit - ';
  }

  reset() {

  }

  onSumbit(b: boolean) {

  }

  onFileChange(event) {
    const reader = new FileReader();

    if(event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      reader.readAsDataURL(file);

      reader.onload = () => {

        this.imageSrc = reader.result as string;

        this.achievementForm.patchValue({
          fileSource: reader.result
        });

      };

    }
  }

  createForm() {
    this.achievementForm = this.achievementFB.group({
      name: new FormControl('', [Validators.required, Validators.minLength(3)]),
      description: new FormControl('', Validators.required),
      type: new FormControl('', Validators.required),
      color: new FormControl('', Validators.required),
      style: new FormControl('', Validators.required),
      unlockLevel: new FormControl('', Validators.required),
      unlockEvent: new FormControl('', Validators.required),
      unlockDescription: new FormControl('', Validators.required),
      cost: new FormControl('', Validators.required),
      costCurrency: new FormControl('', Validators.required),
      file: new FormControl('', [Validators.required]),
      fileSource: new FormControl('', [Validators.required])
    });
  }

  }
