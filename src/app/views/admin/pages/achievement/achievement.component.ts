import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";


@Component({
  selector: 'kt-achievement',
  templateUrl: './achievement.component.html',
  styleUrls: ['./achievement.component.scss']
})
export class AchievementComponent implements OnInit {
  hasFormErrors: false;
  imageSrc: any;
  achievementForm: FormGroup;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private  achievementFB: FormBuilder,
  ) { }

  ngOnInit() {
    this.createForm();
  }

  onAlertClose($event: boolean) {

  }

  goBackWithoutId() {
    const url = `admin/`;
    this.router.navigateByUrl(url, { relativeTo: this.activatedRoute });
  }

  getComponentTitle() {
    return 'Achievement Edit - ';
  }

  reset() {

  }

  onSumbit(b: boolean) {

  }

  onFileChange(event) {
    const reader = new FileReader();

    if(event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      reader.readAsDataURL(file);

      reader.onload = () => {

        this.imageSrc = reader.result as string;

        this.achievementForm.patchValue({
          fileSource: reader.result
        });

      };

    }
  }

  createForm() {
    this.achievementForm = this.achievementFB.group({
      name: new FormControl('', [Validators.required, Validators.minLength(3)]),
      description: new FormControl('',Validators.required),
      priority: new FormControl('',Validators.required),
      previous: new FormControl('',Validators.required),
      maxOrMin: new FormControl('',Validators.required),
      totalBest: new FormControl('',Validators.required),
      start_datePeriod: new FormControl('',Validators.required),
      end_datePeriod: new FormControl('',Validators.required),
      distance: new FormControl('',Validators.required),
      time: new FormControl('',Validators.required),
      pace: new FormControl('',Validators.required),
      totalDistance: new FormControl('',Validators.required),
      elevation: new FormControl('',Validators.required),
      totalElevation: new FormControl('',Validators.required),
      file: new FormControl('', [Validators.required]),
      fileSource: new FormControl('', [Validators.required])
    });


  }
}
