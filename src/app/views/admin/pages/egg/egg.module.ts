import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { EggListComponent } from './egg-list/egg-list.component';
import { EggEditComponent } from './egg-edit/egg-edit.component';
import { PartialsModule } from '../../../partials/partials.module';

import { StoreModule } from '@ngrx/store';
import { EggEffects, eggsReducer, EggsService } from '../../../../core/api';
import { EffectsModule } from '@ngrx/effects';
import { InterceptService } from '../../../../core/_base/crud';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { MatTableModule } from '@angular/material/table';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatPaginatorModule } from '@angular/material/paginator';
import { CommonModule } from '@angular/common';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { NgxPermissionsRestrictStubModule } from 'ngx-permissions';
import { MatButtonModule } from '@angular/material/button';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatSortModule } from '@angular/material/sort';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { DeleteEntityDialogComponent } from '../../../partials/content/crud';
import { MaterialFileInputModule } from 'ngx-material-file-input';
import { ReactiveFormsModule } from '@angular/forms';
import { MatSelectModule } from '@angular/material/select';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: '',
        component: EggListComponent,
      },
      {
        path: 'edit/:id',
        component: EggEditComponent,
      },
    ]),

    PartialsModule,
    StoreModule.forFeature('eggs', eggsReducer),
    EffectsModule.forFeature([EggEffects]),
    MatTableModule,
    MatProgressSpinnerModule,
    MatPaginatorModule,
    CommonModule,
    MatIconModule,
    MatMenuModule,
    NgxPermissionsRestrictStubModule,
    MatButtonModule,
    MatTooltipModule,
    MatSortModule,
    MatCheckboxModule,
    MatFormFieldModule,
    MatInputModule,
    MaterialFileInputModule,
    ReactiveFormsModule,
    MatSelectModule,
  ],
  providers: [
    InterceptService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: InterceptService,
      multi: true,
    },
    EggsService,
  ],
  entryComponents: [DeleteEntityDialogComponent],
  declarations: [EggListComponent, EggEditComponent],
})
export class EggAdminModule {}
