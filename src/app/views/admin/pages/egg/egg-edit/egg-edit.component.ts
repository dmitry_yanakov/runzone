import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'kt-egg-edit',
  templateUrl: './egg-edit.component.html',
  styleUrls: ['./egg-edit.component.scss']
})
export class EggEditComponent implements OnInit {
  hasFormErrors: false;
  imageSrcClosedEgg: any = "assets/media/users/300_25.jpg";
  imageSrcOpenedEgg: any = "assets/media/users/300_25.jpg";
  achievementForm: FormGroup;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private  achievementFB: FormBuilder,
  ) { }

  ngOnInit() {
    this.createForm();
  }

  onAlertClose($event: boolean) {

  }

  goBackWithoutId() {
    const url = `admin/eggs`;
    this.router.navigateByUrl(url, { relativeTo: this.activatedRoute });
  }

  getComponentTitle() {
    return 'Achievement Edit - ';
  }

  reset() {

  }

  onSumbit(b: boolean) {

  }
  onFileChangeCloseEgg(event){
    const reader = new FileReader();
    if(event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      reader.readAsDataURL(file);

      reader.onload = () => {

        this.imageSrcClosedEgg = reader.result as string;

        this.achievementForm.patchValue({
          fileClosedEggSource: reader.result
        });

      };

    }
  }

  onFileChangeOpenEgg(event){
    const reader = new FileReader();
    if(event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      reader.readAsDataURL(file);

      reader.onload = () => {

        this.imageSrcOpenedEgg = reader.result as string;

        this.achievementForm.patchValue({
          fileOpenEggSource: reader.result
        });

      };

    }
  }



  createForm() {
    this.achievementForm = this.achievementFB.group({
      name: new FormControl('', [Validators.required]),
      unlockAtLevel: new FormControl('', [Validators.required]),
      distance: new FormControl('', [Validators.required]),
      gold: new FormControl('', [Validators.required]),
      xp: new FormControl('', [Validators.required]),
      typeEgg: new FormControl('', [Validators.required]),
      fileClosedEgg: new FormControl('', [Validators.required]),
      fileOpenEgg: new FormControl('', [Validators.required]),
      fileClosedEggSource: new FormControl('', [Validators.required]),
      fileOpenEggSource: new FormControl('', [Validators.required])
    });


  }

}
