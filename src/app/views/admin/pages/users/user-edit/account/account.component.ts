// Angular
import {
  Component,
  OnInit,
  ElementRef,
  ViewChild,
  Input,
  ChangeDetectionStrategy,
  OnDestroy,
} from '@angular/core';
import {
  Validators,
  FormBuilder,
  FormGroup,
  FormControl,
} from '@angular/forms';
// Material
import { MatPaginator, MatSort, MatDialog } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';
// RXJS
import { debounceTime, distinctUntilChanged, tap, delay } from 'rxjs/operators';
import {
  fromEvent,
  merge,
  BehaviorSubject,
  Subscription,
  Observable,
  of,
} from 'rxjs';
// NGRX
import { Store, select } from '@ngrx/store';
import { Update } from '@ngrx/entity';
// State
import { IAppState } from '../../../../../../core/reducers';
// CRUD
import {
  TypesUtilsService,
  QueryParamsModel,
  LayoutUtilsService,
  MessageType,
} from '../../../../../../core/_base/crud';
import { UserAccountModel } from '../../../../../../core/api';

@Component({
  selector: 'kt-account',
  templateUrl: './account.component.html',
})
export class AccountComponent implements OnInit {
  @Input() userId$: Observable<number>;
  userId: number;
  hasFormErrors: boolean;
  user: UserAccountModel;
  userForm: FormGroup;
  datePicker;

  constructor(private accountFB: FormBuilder) {}

  ngOnInit(): void {
    this.createForm();
    this.hasFormErrors = false;
    this.user = new UserAccountModel();
    this.user.clear();
  }

  onAlertClose($event: boolean) {}

  createForm() {
    this.userForm = this.accountFB.group({
      name: new FormControl('', [Validators.required, Validators.minLength(3)]),
      description: new FormControl('', Validators.required),
      email: new FormControl('', Validators.required),
      dateBirth: new FormControl('', Validators.required),
      country: new FormControl('', Validators.required),
      city: new FormControl('', Validators.required),
      language: new FormControl('', Validators.required),
      timeZone: new FormControl('', Validators.required),
      gender: new FormControl('', Validators.required),
      units: new FormControl('', Validators.required),
      firstDay: new FormControl('', Validators.required),
      height: new FormControl('', Validators.required),
      weight: new FormControl('', Validators.required),
      facebook: new FormControl('', Validators.required),
      strava: new FormControl('', Validators.required),
    });
  }
}
