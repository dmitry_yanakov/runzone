import {Component, Input, OnInit} from '@angular/core';
import {Observable} from "rxjs";
import {ConfigUserList} from "../../user-list/config-user-list";

@Component({
  selector: 'kt-friends',
  templateUrl: './friends.component.html',
  styleUrls: ['./friends.component.scss']
})
export class FriendsComponent implements OnInit {
  @Input() userId$: Observable<number>;
  userId: number;
  user : ConfigUserList;
  constructor() { }

  ngOnInit() {
    this.user = new class implements ConfigUserList {
      titleDate = 'Date joined';

      getStatus(id: number) {
        switch (id) {
          case 0:
            return 'Member';
          case 1:
            return 'Friend';
          case 2:
            return 'Request';
          case 3:
            return 'Delete';
        }
      }
    }
  }

}
