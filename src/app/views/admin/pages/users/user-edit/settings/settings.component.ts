import { Component, Input, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { UserAccountModel } from '../../../../../../core/api';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'kt-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss'],
})
export class SettingsComponent implements OnInit {
  @Input() userId$: Observable<number>;
  userId: number;
  hasFormErrors: boolean;
  user: UserAccountModel;
  userForm: FormGroup;

  ngOnInit(): void {
    this.hasFormErrors = false;
    this.user = new UserAccountModel();
    this.user.clear();
  }

  onAlertClose($event: boolean) {}
}
