import {Component, Input, OnInit} from '@angular/core';
import {Observable} from "rxjs";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'kt-activities',
  templateUrl: './activities.component.html',
  styleUrls: ['./activities.component.scss']
})
export class ActivitiesComponent implements OnInit {
  @Input() userId$: Observable<number>;
  userId: number;
  userForm: FormGroup;
  constructor(private  accountFB: FormBuilder,) {
  }

  ngOnInit() {

  }

}
