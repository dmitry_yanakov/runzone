import { CommonModule } from '@angular/common';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import {
  CUSTOM_ELEMENTS_SCHEMA,
  NgModule,
  NO_ERRORS_SCHEMA,
} from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatButtonModule } from '@angular/material/button';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatMenuModule } from '@angular/material/menu';
import { MatRadioModule } from '@angular/material/radio';
import { MatSelectModule } from '@angular/material/select';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { MatTabsModule } from '@angular/material/tabs';
import { MatTooltipModule } from '@angular/material/tooltip';
import { RouterModule, Routes } from '@angular/router';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { environment } from '../../../../../../environments/environment';
import { InterceptService } from '../../../../../core/_base/crud';
import { FakeApiService } from '../../../../../core/_base/layout';
import {
  UserEffects,
  usersReducer,
  UsersService,
} from '../../../../../core/api';
import { MaterialPreviewModule } from '../../../../partials/content/general/material-preview/material-preview.module';
import { PortletModule } from '../../../../partials/content/general/portlet/portlet.module';
import { PartialsModule } from '../../../../partials/partials.module';
import { PagesAdminModule } from '../../pages.module';
import { ActivitiesAdminModule } from '../activities/activities.module';
import { UserListComponent } from '../user-list/user-list.component';
import { AccountComponent } from './account/account.component';
import { ActivitiesComponent } from './activities/activities.component';
import { FriendsComponent } from './friends/friends.component';
import { GameProfileComponent } from './game-profile/game-profile.component';
import { LayoutEditComponent } from './layout/layout.component';
import { SettingsComponent } from './settings/settings.component';

// UserEffects
@NgModule({
  imports: [
    MatMenuModule,
    MatIconModule,
    MatTabsModule,
    CommonModule,
    MatButtonModule,
    MatTooltipModule,
    MatFormFieldModule,
    MatInputModule,
    MatTableModule,
    MatSortModule,
    MatSelectModule,
    PartialsModule,
    MatAutocompleteModule,
    MatRadioModule,
    ReactiveFormsModule,
    MatDatepickerModule,
    MaterialPreviewModule,
    FormsModule,
    PagesAdminModule,
    MatListModule,
    ActivitiesAdminModule,
    // HttpClientInMemoryWebApiModule.forRoot(
    //   FakeApiService, { dataEncapsulation: false }
    // ),
  ],
  providers: [
    InterceptService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: InterceptService,
      multi: true,
    },
    UsersService,
  ],
  entryComponents: [],

  declarations: [
    LayoutEditComponent,
    GameProfileComponent,
    FriendsComponent,
    ActivitiesComponent,
    AccountComponent,
    SettingsComponent,
  ],
})
export class UserEditModule {}
