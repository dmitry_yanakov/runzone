import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { BehaviorSubject, Observable, of, Subscription } from 'rxjs';
import { select, Store } from '@ngrx/store';
import {
  LayoutConfigService,
  SubheaderService,
} from '../../../../../../core/_base/layout';

import { Update } from '@ngrx/entity';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import {
  LayoutUtilsService,
  MessageType,
  TypesUtilsService,
} from '../../../../../../core/_base/crud';
import { IAppState } from '../../../../../../core/reducers';
import { delay, map, startWith } from 'rxjs/operators';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UsersService } from '../../../../../../core/api/_services';
import {
  selectLastCreatedUserId,
  selectUserById,
  UserModel,
  UserOnServerCreated,
  UserUpdated,
} from '../../../../../../core/api';

@Component({
  selector: 'kt-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss'],
})
export class LayoutEditComponent implements OnInit {
  @Input() userId$: Observable<number>;

  user: UserModel;
  userId: Observable<number>;
  oldUser: UserModel;
  selectedTab = 0;
  loadingSubject = new BehaviorSubject<boolean>(true);
  loading$: Observable<boolean>;
  userForm: FormGroup;
  hasFormErrors = false;
  availableYears: number[] = [];
  filteredColors: Observable<string[]>;
  filteredManufactures: Observable<string[]>;
  // Private password
  private componentSubscriptions: Subscription;
  // sticky portlet header margin
  private headerMargin: number;

  constructor(
    private store: Store<IAppState>,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private typesUtilsService: TypesUtilsService,
    private userFB: FormBuilder,
    public dialog: MatDialog,
    private subheaderService: SubheaderService,
    private layoutUtilsService: LayoutUtilsService,
    private layoutConfigService: LayoutConfigService,
    private userService: UsersService,
    private cdr: ChangeDetectorRef
  ) {}

  ngOnInit(): void {
    for (let i = 2019; i > 1945; i--) {
      this.availableYears.push(i);
    }
    this.loading$ = this.loadingSubject.asObservable();
    this.loadingSubject.next(true);
    this.activatedRoute.params.subscribe((params) => {
      const id = params.id;
      if (id && id > 0) {
        this.store.pipe(select(selectUserById(id))).subscribe((result) => {
          if (!result) {
            this.loadUserFromService(id);
            return;
          }

          this.loadUser(result);
        });
      } else {
        const newUser = new UserModel();
        newUser.clear();
        newUser.id = 55; // tests
        this.loadUser(newUser);
      }
    });

    // sticky portlet header
    window.onload = () => {
      const style = getComputedStyle(document.getElementById('kt_header'));
      this.headerMargin = parseInt(style.height, 0);
    };
  }

  loadUser(_user, fromService: boolean = false) {
    if (!_user) {
      this.goBack('');
    }
    this.user = _user;
    this.userId$ = of(_user.id);
    this.oldUser = Object.assign({}, _user);
    this.initUser();
    if (fromService) {
      this.cdr.detectChanges();
    }
  }

  // If product didn't find in store
  loadUserFromService(userId) {
    this.userService.getCustomerById(userId).subscribe((res) => {
      this.loadUser(res, true);
    });
  }

  goBack(id) {
    this.loadingSubject.next(false);
    const url = `admin/users/edit?id=${id}`;
    this.router.navigateByUrl(url, { relativeTo: this.activatedRoute });
  }

  initUser() {}

  onSumbit(b: boolean) {}

  getComponentTitle() {
    let result = 'Create user';
    if (!this.user || !this.user.id) {
      return result;
    }

    result = `Edit user - ${this.user.username} ${this.user.email}`;
    return result;
  }

  goBackWithoutId() {
    this.router.navigateByUrl('admin/users', {
      relativeTo: this.activatedRoute,
    });
  }

  reset() {}
}
