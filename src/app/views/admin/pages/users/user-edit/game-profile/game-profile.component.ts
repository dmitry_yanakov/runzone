import { Component, Input, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { UserAccountModel } from '../../../../../../core/api';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';

@Component({
  selector: 'kt-game-profile',
  templateUrl: './game-profile.component.html',
  styleUrls: ['./game-profile.component.scss'],
})
export class GameProfileComponent implements OnInit {
  @Input() userId$: Observable<number>;
  userId: number;
  hasFormErrors: boolean;
  user: UserAccountModel;
  userForm: FormGroup;
  datePicker;

  constructor(private accountFB: FormBuilder) {}

  ngOnInit(): void {
    this.createForm();
    this.hasFormErrors = false;
    this.user = new UserAccountModel();
    this.user.clear();
  }

  onAlertClose($event: boolean) {}
  createForm() {
    this.userForm = this.accountFB.group({
      level: new FormControl('', [Validators.required]),
      clan: new FormControl('', Validators.required),
      date: new FormControl('', Validators.required),
      auth: new FormControl('', Validators.required),
      coins: new FormControl('', Validators.required),
      gems: new FormControl('', Validators.required),
    });
  }
}
