// Angular
import {
  Component,
  OnInit,
  ElementRef,
  ViewChild,
  ChangeDetectionStrategy,
  OnDestroy,
  Input,
} from '@angular/core';
// Material
import { SelectionModel } from '@angular/cdk/collections';
import {
  MatPaginator,
  MatSort,
  MatSnackBar,
  MatDialog,
} from '@angular/material';
// RXJS
import {
  debounceTime,
  distinctUntilChanged,
  tap,
  skip,
  delay,
  take,
} from 'rxjs/operators';
import { fromEvent, merge, Subscription, of } from 'rxjs';
// Translate Module
import { TranslateService } from '@ngx-translate/core';
// NGRX
import { Store, ActionsSubject } from '@ngrx/store';
import { IAppState } from '../../../../../core/reducers';
// CRUD
import {
  LayoutUtilsService,
  MessageType,
  QueryParamsModel,
} from '../../../../../core/_base/crud';
// Services and Models
import {
  UserModel,
  UsersDataSource,
  UsersPageRequested,
  OneUserDeleted,
  ManyUsersDeleted,
  UsersStatusUpdated,
} from '../../../../../core/api';
import { ActivatedRoute, Router } from '@angular/router';
import { ConfigUserList } from './config-user-list';

@Component({
  selector: 'kt-user-list',
  templateUrl: './user-list.component.html',
  //styleUrls: ['./user-list.component.scss']
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UserListComponent implements OnInit, OnDestroy {
  @Input() configUser: ConfigUserList;

  // Table fields
  dataSource: UsersDataSource;
  displayedColumns = [
    'select',
    'id',
    'country',
    'username',
    'email',
    'clan',
    'trophies',
    'level',
    'xp',
    'date_registration',
    'type',
    'status',
    'actions',
  ];
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild('sort1', { static: true }) sort: MatSort;
  // Filter fields
  @ViewChild('searchInput', { static: true }) searchInput: ElementRef;
  filterStatus = '';
  filterType = '';
  // Selection
  selection = new SelectionModel<UserModel>(true, []);
  customersResult: UserModel[] = [];
  // Subscriptions
  private subscriptions: Subscription[] = [];

  /**
   * Component constructor
   *
   * @param dialog: MatDialog
   * @param snackBar: MatSnackBar
   * @param layoutUtilsService: LayoutUtilsService
   * @param translate: TranslateService
   * @param store: Store<AppState>
   */
  constructor(
    public dialog: MatDialog,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    public snackBar: MatSnackBar,
    private layoutUtilsService: LayoutUtilsService,
    private translate: TranslateService,
    private store: Store<IAppState>
  ) {}

  /**
   * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
   */

  /**
   * On init
   */
  ngOnInit() {
    console.log(this.configUser);
    this.initConfigUser();
    console.log(this.configUser);
    // If the user changes the sort order, reset back to the first page.
    const sortSubscription = this.sort.sortChange.subscribe(
      () => (this.paginator.pageIndex = 0)
    );
    this.subscriptions.push(sortSubscription);

    /* Data load will be triggered in two cases:
    - when a pagination event occurs => this.paginator.page
    - when a sort event occurs => this.sort.sortChange
    **/
    const paginatorSubscriptions = merge(
      this.sort.sortChange,
      this.paginator.page
    )
      .pipe(tap(() => this.loadCustomersList()))
      .subscribe();
    this.subscriptions.push(paginatorSubscriptions);

    // Filtration, bind to searchInput
    const searchSubscription = fromEvent(
      this.searchInput.nativeElement,
      'keyup'
    )
      .pipe(
        // tslint:disable-next-line:max-line-length
        debounceTime(50), // The user can type quite quickly in the input box, and that could trigger a lot of server requests. With this operator, we are limiting the amount of server requests emitted to a maximum of one every 150ms
        distinctUntilChanged(), // This operator will eliminate duplicate values
        tap(() => {
          this.paginator.pageIndex = 0;
          this.loadCustomersList();
        })
      )
      .subscribe();
    this.subscriptions.push(searchSubscription);

    // Init DataSource
    this.dataSource = new UsersDataSource(this.store);
    const entitiesSubscription = this.dataSource.entitySubject
      .pipe(skip(1), distinctUntilChanged())
      .subscribe((res) => {
        this.customersResult = res;
      });
    this.subscriptions.push(entitiesSubscription);
    // First load
    of(undefined)
      .pipe(take(1), delay(1000))
      .subscribe(() => {
        // Remove this line, just loading imitation
        this.loadCustomersList();
      }); // Remove this line, just loading imitation
  }

  /**
   * On Destroy
   */
  ngOnDestroy() {
    this.subscriptions.forEach((el) => el.unsubscribe());
  }

  /**
   * Load Customers List from services through data-source
   */
  loadCustomersList() {
    this.selection.clear();
    const queryParams = new QueryParamsModel(
      this.filterConfiguration(),
      this.sort.direction,
      this.sort.active,
      this.paginator.pageIndex,
      this.paginator.pageSize
    );
    // Call request from server
    this.store.dispatch(new UsersPageRequested({ page: queryParams }));
    this.selection.clear();
  }

  /**
   * Returns object for filter
   */
  filterConfiguration(): any {
    const filter: any = {};
    const searchText: string = this.searchInput.nativeElement.value;

    if (this.filterStatus && this.filterStatus.length > 0) {
      filter.status = +this.filterStatus;
    }

    if (this.filterType && this.filterType.length > 0) {
      filter.type = +this.filterType;
    }

    filter.username = searchText;
    if (!searchText) {
      return filter;
    }

    filter.email = searchText;
    filter.trophies = searchText;
    filter.clan = searchText;
    return filter;
  }

  /** ACTIONS */
  /**
   * Delete customer
   *
   * @param _item: CustomerModel
   */
  deleteCustomer(_item: UserModel) {
    return;
  }

  /**
   * Delete selected customers
   */
  deleteCustomers() {
    return;
  }

  /**
   * Fetch selected customers
   */
  fetchCustomers() {
    return;
  }

  /**
   * Show UpdateStatuDialog for selected customers
   */
  updateStatusForCustomers() {
    return;
  }

  /**
   * Show add customer dialog
   */
  addCustomer() {}

  /**
   * Show Edit customer dialog and save after success close result
   * @param id: number
   */
  editCustomer(id: number) {
    this.router.navigate([this.router.url + '/edit', id], {
      relativeTo: this.activatedRoute,
    });
  }

  /**
   * Check all rows are selected
   */
  isAllSelected(): boolean {
    const numSelected = this.selection.selected.length;
    const numRows = this.customersResult.length;
    return numSelected === numRows;
  }

  /**
   * Toggle all selections
   */
  masterToggle() {
    if (this.selection.selected.length === this.customersResult.length) {
      this.selection.clear();
    } else {
      this.customersResult.forEach((row) => this.selection.select(row));
    }
  }
  //0 - admin , 1 - active , 2 - verify , 3 - blocked , 4 - delete
  initConfigUser() {
    if (this.configUser) {
    } else {
      this.configUser = new (class implements ConfigUserList {
        titleDate = 'Date registration';

        getStatus(id: number) {
          switch (id) {
            case 1:
              return 'Admin';
            case 2:
              return 'Active';
            case 3:
              return 'Verify';
            case 4:
              return 'Blocked';
            case 5:
              return 'Delete';
          }
        }
      })();
    }
  }

  /** UI */
  /**
   * Retursn CSS Class Name by status
   *
   * @param status: number
   */
  getItemCssClassByStatus(status: number = 0): string {
    switch (status) {
      case 0:
        return 'metal';
      case 1:
        return 'success';
    }
    return '';
  }

  /**
   * Returns Item Status in string
   * @param status: number
   */
  getItemStatusString(status: number = 0): string {
    switch (status) {
      case 0:
        return 'Offline';
      case 1:
        return 'Online';
    }
    return '';
  }

  /**
   * Returns CSS Class Name by type
   * @param status: number
   */
  getItemCssClassByType(status: number = 0): string {
    switch (status) {
      case 0:
        return 'accent';
      case 1:
        return 'primary';
      case 2:
        return '';
    }
    return '';
  }

  /**
   * Returns Item Type in string
   * 0 - admin , 1 - active , 2 - verify , 3 - blocked , 4 - delete
   * @param status: number
   */
  getItemTypeString(status: number = 0): string {
    return this.configUser.getStatus(status);
  }
}
