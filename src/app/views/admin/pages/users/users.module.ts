import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { environment } from '../../../../../environments/environment';
import { InterceptService } from '../../../../core/_base/crud';
import { FakeApiService } from '../../../../core/_base/layout';
import {
  activitiesReducer,
  ActivitiesService,
  ActivityEffects,
  UserEffects,
  usersReducer,
  UsersService,
} from '../../../../core/api';
import { DashboardComponent } from '../dashboard/dashboard.component';
import { ActivitiesEditComponent } from './activities/activities-edit/activities-edit.component';
import { ActivitiesListComponent } from './activities/activities-list/activities-list.component';
import { AccountComponent } from './user-edit/account/account.component';
import { LayoutEditComponent } from './user-edit/layout/layout.component';
import { UserListComponent } from './user-list/user-list.component';
import { UserPageComponent } from './user.component';

// UserEffects
@NgModule({
  imports: [
    // environment.isMockEnabled
    //   ? HttpClientInMemoryWebApiModule.forFeature(FakeApiService, {
    //       passThruUnknownUrl: true,
    //       dataEncapsulation: false,
    //     })
    //   : [],
    // HttpClientInMemoryWebApiModule.forRoot(
    //   FakeApiService, { dataEncapsulation: false }
    // ),
    RouterModule.forChild([
      {
        path: 'edit/:userId',
        component: LayoutEditComponent,
      },
      {
        path: 'activity',
        component: ActivitiesListComponent,
      },
      {
        path: 'activity/edit/:Id',
        component: ActivitiesEditComponent,
      },
      {
        path: '',
        component: UserListComponent,
      },
    ]),
    StoreModule.forFeature('users', usersReducer),
    EffectsModule.forFeature([UserEffects]),
    StoreModule.forFeature('activities', activitiesReducer),
    EffectsModule.forFeature([ActivityEffects]),
  ],
  providers: [
    InterceptService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: InterceptService,
      multi: true,
    },
    UsersService,
    ActivitiesService,
  ],
  entryComponents: [],
  declarations: [UserPageComponent],
})
export class UserAdminModule {}
