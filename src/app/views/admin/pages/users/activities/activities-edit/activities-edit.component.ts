import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'kt-activities-edit',
  templateUrl: './activities-edit.component.html',
  styleUrls: ['./activities-edit.component.scss']
})
export class ActivitiesEditComponent implements OnInit {
  hasFormErrors: false;
  userForm: FormGroup;
  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private  accountFB: FormBuilder
  ) { }

  ngOnInit() {
    this.createForm();
  }

  onAlertClose($event: boolean) {

  }

  goBackWithoutId() {
      const url = `admin/users/activity`;
      this.router.navigateByUrl(url, { relativeTo: this.activatedRoute });
  }

  getComponentTitle() {
    return 'Activity Edit - ';
  }

  reset() {

  }

  onSumbit(b: boolean) {

  }
  createForm() {
    this.userForm = this.accountFB.group({
      name: new FormControl('', Validators.required),
      type: new FormControl('', Validators.required),
      dateCreate: new FormControl('', Validators.required),
      dateDone: new FormControl('', Validators.required),
      distance: new FormControl('', Validators.required),
      time: new FormControl('', Validators.required),
      elevation: new FormControl('', Validators.required),
      status: new FormControl('', Validators.required),
      heartRate: new FormControl('', Validators.required),
    });
  }
}
