import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CommonModule } from '@angular/common';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatMenuModule } from '@angular/material/menu';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSelectModule } from '@angular/material/select';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { MatTooltipModule } from '@angular/material/tooltip';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { NgxPermissionsModule } from 'ngx-permissions';
import { environment } from '../../../../../../environments/environment';
import { InterceptService } from '../../../../../core/_base/crud';
import { FakeApiService } from '../../../../../core/_base/layout';
import {
  activitiesReducer,
  ActivitiesService,
  ActivityEffects,
} from '../../../../../core/api';
import { PartialsModule } from '../../../../partials/partials.module';
import { ActivitiesEditComponent } from './activities-edit/activities-edit.component';
import { ActivitiesListComponent } from './activities-list/activities-list.component';

const routes: Routes = [
  {
    children: [
      {
        path: '',
        component: ActivitiesListComponent,
      },
      {
        path: 'edit/:id',
        component: ActivitiesEditComponent,
      },
    ],
  },
];

@NgModule({
  imports: [
    // environment.isMockEnabled
    //   ? HttpClientInMemoryWebApiModule.forFeature(FakeApiService, {
    //       passThruUnknownUrl: true,
    //       dataEncapsulation: false,
    //     })
    //   : [],
    // HttpClientInMemoryWebApiModule.forRoot(
    //   FakeApiService, { dataEncapsulation: false }
    // ),
    RouterModule.forChild(routes),
    PartialsModule,
    MatFormFieldModule,
    MatSelectModule,
    CommonModule,
    MatIconModule,
    MatTableModule,
    MatCheckboxModule,
    MatMenuModule,
    MatProgressSpinnerModule,
    MatPaginatorModule,
    MatSortModule,
    MatButtonModule,
    MatTooltipModule,
    NgxPermissionsModule,
    MatInputModule,
    MatDatepickerModule,
    ReactiveFormsModule,
    FormsModule,
  ],
  exports: [RouterModule, ActivitiesListComponent],
  providers: [
    InterceptService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: InterceptService,
      multi: true,
    },
    ActivitiesService,
  ],

  declarations: [ActivitiesListComponent, ActivitiesEditComponent],
})
export class ActivitiesAdminModule {}
