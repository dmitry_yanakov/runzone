import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import {
  ManyActivitiesDeleted,
  OneActivityDeleted,
  ActivitiesModel,
  ActivitiesDataSource,
  ActivitiesPageRequested,
  ActivitiesStatusUpdated,
} from '../../../../../../core/api';
import { SelectionModel } from '@angular/cdk/collections';
import { fromEvent, merge, of, Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import {
  LayoutUtilsService,
  MessageType,
  QueryParamsModel,
} from '../../../../../../core/_base/crud';
import { TranslateService } from '@ngx-translate/core';
import { Store } from '@ngrx/store';
import { IAppState } from '../../../../../../core/reducers';
import {
  debounceTime,
  delay,
  distinctUntilChanged,
  skip,
  take,
  tap,
} from 'rxjs/operators';

import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'kt-activities-list',
  templateUrl: './activities-list.component.html',
  styles: [],
})
export class ActivitiesListComponent implements OnInit {
  dataSource: ActivitiesDataSource;
  displayedColumns = [
    'select',
    'id',
    'name',
    'time',
    'username',
    'distance',
    'elevation',
    'pace',
    'date_create',
    'date_done',
    'status',
    'erun',
    'actions',
  ];
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild('sort1', { static: true }) sort: MatSort;
  // Filter fields
  @ViewChild('searchInput', { static: true }) searchInput: ElementRef;
  filterStatus = '';
  filterType = '';
  // Selection
  selection = new SelectionModel<ActivitiesModel>(true, []);
  customersResult: ActivitiesModel[] = [];
  // Subscriptions
  private subscriptions: Subscription[] = [];

  /**
   * Component constructor
   *
   * @param dialog: MatDialog
   * @param snackBar: MatSnackBar
   * @param layoutUtilsService: LayoutUtilsService
   * @param translate: TranslateService
   * @param store: Store<AppState>
   */
  constructor(
    public dialog: MatDialog,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    public snackBar: MatSnackBar,
    private layoutUtilsService: LayoutUtilsService,
    private translate: TranslateService,
    private store: Store<IAppState>
  ) {}

  /**
   * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
   */

  /**
   * On init
   */
  ngOnInit() {
    // If the user changes the sort order, reset back to the first page.
    const sortSubscription = this.sort.sortChange.subscribe(
      () => (this.paginator.pageIndex = 0)
    );
    this.subscriptions.push(sortSubscription);

    /* Data load will be triggered in two cases:
    - when a pagination event occurs => this.paginator.page
    - when a sort event occurs => this.sort.sortChange
    **/
    const paginatorSubscriptions = merge(
      this.sort.sortChange,
      this.paginator.page
    )
      .pipe(tap(() => this.loadActivitiesList()))
      .subscribe();
    this.subscriptions.push(paginatorSubscriptions);
    // new ActivityListModel()
    //   ._createdDate    // Filtration, bind to searchInput
    const searchSubscription = fromEvent(
      this.searchInput.nativeElement,
      'keyup'
    )
      .pipe(
        // tslint:disable-next-line:max-line-length
        debounceTime(50), // The user can type quite quickly in the input box, and that could trigger a lot of server requests. With this operator, we are limiting the amount of server requests emitted to a maximum of one every 150ms
        distinctUntilChanged(), // This operator will eliminate duplicate values
        tap(() => {
          this.paginator.pageIndex = 0;
          this.loadActivitiesList();
        })
      )
      .subscribe();
    this.subscriptions.push(searchSubscription);

    // Init DataSource
    this.dataSource = new ActivitiesDataSource(this.store);
    const entitiesSubscription = this.dataSource.entitySubject
      .pipe(skip(1), distinctUntilChanged())
      .subscribe((res) => {
        this.customersResult = res;
      });
    this.subscriptions.push(entitiesSubscription);
    // First load
    of(undefined)
      .pipe(take(1), delay(1000))
      .subscribe(() => {
        // Remove this line, just loading imitation
        this.loadActivitiesList();
      }); // Remove this line, just loading imitation
  }

  /**
   * On Destroy
   */
  ngOnDestroy() {
    this.subscriptions.forEach((el) => el.unsubscribe());
  }

  /**
   * Load Customers List from services through data-source
   */
  loadActivitiesList() {
    this.selection.clear();
    const queryParams = new QueryParamsModel(
      this.filterConfiguration(),
      this.sort.direction,
      this.sort.active,
      this.paginator.pageIndex,
      this.paginator.pageSize
    );
    // Call request from server
    this.store.dispatch(new ActivitiesPageRequested({ page: queryParams }));
    this.selection.clear();
  }

  /**
   * Returns object for filter
   */
  filterConfiguration(): any {
    const filter: any = {};
    const searchText: string = this.searchInput.nativeElement.value;

    if (this.filterStatus && this.filterStatus.length > 0) {
      filter.status = +this.filterStatus;
    }

    if (this.filterType && this.filterType.length > 0) {
      filter.type = +this.filterType;
    }

    filter.username = searchText;
    if (!searchText) {
      return filter;
    }

    filter.email = searchText;
    filter.trophies = searchText;
    filter.clan = searchText;
    return filter;
  }

  /** ACTIONS */
  /**
   * Delete customer
   *
   * @param _item: CustomerModel
   */
  deleteActivity(_item: ActivitiesModel) {
    return;
  }

  /**
   * Delete selected customers
   */
  deleteActivities() {
    return;
  }

  /**
   * Fetch selected customers
   */
  fetchCustomers() {
    const messages = [];
    this.selection.selected.forEach((elem) => {
      messages.push({
        text: `${elem.username}`,
        id: elem.id.toString(),
        status: elem.status,
      });
    });
    this.layoutUtilsService.fetchElements(messages);
  }

  /**
   * Show add customer dialog
   */
  addActivity() {}

  /**
   * Show Edit customer dialog and save after success close result
   * @param id: number
   */
  editActivity(id: number) {
    this.router.navigate([this.router.url + '/edit', id], {
      relativeTo: this.activatedRoute,
    });
  }

  /**
   * Check all rows are selected
   */
  isAllSelected(): boolean {
    const numSelected = this.selection.selected.length;
    const numRows = this.customersResult.length;
    return numSelected === numRows;
  }

  /**
   * Toggle all selections
   */
  masterToggle() {
    if (this.selection.selected.length === this.customersResult.length) {
      this.selection.clear();
    } else {
      this.customersResult.forEach((row) => this.selection.select(row));
    }
  }

  /**
   * Returns CSS Class Name by type
   * @param status: number
   */
  getItemCssClassByType(status: number = 0): string {
    switch (status) {
      case 0:
        return 'accent';
      case 1:
        return 'primary';
      case 2:
        return '';
      case 3:
        return '';
      case 4:
        return 'danger';
    }
    return '';
  }

  /**
   * Returns Item Type in string
   * 0 - admin , 1 - active , 2 - verify , 3 - blocked , 4 - delete
   * @param status: number
   */
  getItemTypeString(status: number = 0): string {
    // 0 - revise, 1 - incorrect, 2 - duplicated, 3 - active , 4 - disabled
    switch (status) {
      case 0:
        return 'Revise';
      case 1:
        return 'Incorrect';
      case 2:
        return 'Duplicated';
      case 3:
        return 'Active';
      case 4:
        return 'Disabled';
    }
    return '';
  }
}
