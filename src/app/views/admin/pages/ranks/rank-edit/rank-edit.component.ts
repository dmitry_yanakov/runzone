import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'kt-rank-edit',
  templateUrl: './rank-edit.component.html',
  styleUrls: ['./rank-edit.component.scss']
})
export class RankEditComponent implements OnInit {
  hasFormErrors: false;
  imageSrc: any;
  achievementForm: FormGroup;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private  achievementFB: FormBuilder,
  ) { }

  ngOnInit() {
    this.createForm();
  }

  onAlertClose($event: boolean) {

  }

  goBackWithoutId() {
    const url = `admin/ranks`;
    this.router.navigateByUrl(url, { relativeTo: this.activatedRoute });
  }

  getComponentTitle() {
    return 'Runner Edit - ';
  }

  reset() {

  }

  onSumbit(b: boolean) {

  }

  onFileChange(event) {
    const reader = new FileReader();

    if(event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      reader.readAsDataURL(file);

      reader.onload = () => {

        this.imageSrc = reader.result as string;

        this.achievementForm.patchValue({
          fileSource: reader.result
        });

      };

    }
  }

  createForm() {

    this.achievementForm = this.achievementFB.group({
      rank: new FormControl('', [Validators.required]),
      rarity: new FormControl('',Validators.required),
      number: new FormControl('',Validators.required),
      gold: new FormControl('',Validators.required),
      xp: new FormControl('',Validators.required),
    });

  }
}
