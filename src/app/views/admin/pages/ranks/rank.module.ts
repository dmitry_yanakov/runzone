import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatMenuModule } from '@angular/material/menu';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSelectModule } from '@angular/material/select';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { MatTooltipModule } from '@angular/material/tooltip';
import { RouterModule } from '@angular/router';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { MaterialFileInputModule } from 'ngx-material-file-input';
import { NgxPermissionsModule } from 'ngx-permissions';
import { RankEffects, ranksReducer, RanksService } from '../../../../core/api';
import { PartialsModule } from '../../../partials/partials.module';
import { RankEditComponent } from './rank-edit/rank-edit.component';
import { RankListComponent } from './rank-list/rank-list.component';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: '',
        component: RankListComponent,
      },
      {
        path: 'edit/:id',
        component: RankEditComponent,
      },
    ]),
    StoreModule.forFeature('ranks', ranksReducer),
    EffectsModule.forFeature([RankEffects]),
    PartialsModule,
    MatFormFieldModule,
    MatInputModule,
    CommonModule,
    NgxPermissionsModule,
    MatButtonModule,
    MatTooltipModule,
    MatIconModule,
    MatTableModule,
    MatCheckboxModule,
    MatSortModule,
    MatMenuModule,
    MatProgressSpinnerModule,
    MatPaginatorModule,
    MaterialFileInputModule,
    ReactiveFormsModule,
    MatSelectModule,
  ],
  declarations: [RankListComponent, RankEditComponent],
  providers: [RanksService],
})
export class RankAdminModule {}
