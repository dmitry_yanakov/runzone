import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'kt-runner-edit',
  templateUrl: './runner-edit.component.html',
  styleUrls: ['./runner-edit.component.scss']
})
export class RunnerEditComponent implements OnInit {
  hasFormErrors: false;
  imageSrc: any;
  achievementForm: FormGroup;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private  achievementFB: FormBuilder,
  ) { }

  ngOnInit() {
    this.createForm();
  }

  onAlertClose($event: boolean) {

  }

  goBackWithoutId() {
    const url = `admin/runners`;
    this.router.navigateByUrl(url, { relativeTo: this.activatedRoute });
  }

  getComponentTitle() {
    return 'Runner Edit - ';
  }

  reset() {

  }

  onSumbit(b: boolean) {

  }

  onFileChange(event) {
    const reader = new FileReader();

    if(event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      reader.readAsDataURL(file);

      reader.onload = () => {

        this.imageSrc = reader.result as string;

        this.achievementForm.patchValue({
          fileSource: reader.result
        });

      };

    }
  }

  createForm() {
    this.achievementForm = this.achievementFB.group({
      name: new FormControl('', [Validators.required, Validators.minLength(3)]),
      description: new FormControl('',Validators.required),
      rarity: new FormControl('',Validators.required),
      type: new FormControl('',Validators.required),
      unlock: new FormControl('',Validators.required),
      location: new FormControl('',Validators.required),
      speed: new FormControl('',Validators.required),
      endurance: new FormControl('',Validators.required),
      climb: new FormControl('',Validators.required),
      recovery: new FormControl('',Validators.required),
      file: new FormControl('', [Validators.required]),
      fileSource: new FormControl('', [Validators.required])
    });


  }
}
