import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import {
  ManyRunnersDeleted,
  OneRunnerDeleted,
  RunnerModel,
  RunnersDataSource,
  RunnersPageRequested,
  RunnersStatusUpdated,
} from '../../../../../core/api';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { SelectionModel } from '@angular/cdk/collections';
import { fromEvent, merge, of, Subscription } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import {
  LayoutUtilsService,
  MessageType,
  QueryParamsModel,
} from '../../../../../core/_base/crud';
import { TranslateService } from '@ngx-translate/core';
import { Store } from '@ngrx/store';
import { IAppState } from '../../../../../core/reducers';
import {
  debounceTime,
  delay,
  distinctUntilChanged,
  skip,
  take,
  tap,
} from 'rxjs/operators';

@Component({
  selector: 'kt-runner-list',
  templateUrl: './runner-list.component.html',
  styleUrls: ['./runner-list.component.scss'],
})
export class RunnerListComponent implements OnInit {
  dataSource: RunnersDataSource;
  displayedColumns = [
    'select',
    'id',
    'name',
    'rarity',
    'unlock',
    'type',
    'location',
    'speed',
    'endurance',
    'climb',
    'actions',
  ];
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild('sort1', { static: true }) sort: MatSort;
  // Filter fields
  @ViewChild('searchInput', { static: true }) searchInput: ElementRef;
  filterStatus = '';
  filterType = '';
  // Selection
  selection = new SelectionModel<RunnerModel>(true, []);
  runnersResult: RunnerModel[] = [];
  // Subscriptions
  private subscriptions: Subscription[] = [];

  /**
   * Component constructor
   *
   * @param dialog: MatDialog
   * @param snackBar: MatSnackBar
   * @param layoutUtilsService: LayoutUtilsService
   * @param translate: TranslateService
   * @param store: Store<AppState>
   */
  constructor(
    public dialog: MatDialog,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    public snackBar: MatSnackBar,
    private layoutUtilsService: LayoutUtilsService,
    private translate: TranslateService,
    private store: Store<IAppState>
  ) {}

  /**
   * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
   */

  /**
   * On init
   */
  ngOnInit() {
    // If the runner changes the sort order, reset back to the first page.
    const sortSubscription = this.sort.sortChange.subscribe(
      () => (this.paginator.pageIndex = 0)
    );
    this.subscriptions.push(sortSubscription);

    /* Data load will be triggered in two cases:
    - when a pagination event occurs => this.paginator.page
    - when a sort event occurs => this.sort.sortChange
    **/
    const paginatorSubscriptions = merge(
      this.sort.sortChange,
      this.paginator.page
    )
      .pipe(tap(() => this.loadRunnersList()))
      .subscribe();
    this.subscriptions.push(paginatorSubscriptions);

    // Filtration, bind to searchInput
    const searchSubscription = fromEvent(
      this.searchInput.nativeElement,
      'keyup'
    )
      .pipe(
        // tslint:disable-next-line:max-line-length
        debounceTime(50), // The runner can type quite quickly in the input box, and that could trigger a lot of server requests. With this operator, we are limiting the amount of server requests emitted to a maximum of one every 150ms
        distinctUntilChanged(), // This operator will eliminate duplicate values
        tap(() => {
          this.paginator.pageIndex = 0;
          this.loadRunnersList();
        })
      )
      .subscribe();
    this.subscriptions.push(searchSubscription);

    // Init DataSource
    this.dataSource = new RunnersDataSource(this.store);
    const entitiesSubscription = this.dataSource.entitySubject
      .pipe(skip(1), distinctUntilChanged())
      .subscribe((res) => {
        this.runnersResult = res;
      });
    this.subscriptions.push(entitiesSubscription);
    // First load
    of(undefined)
      .pipe(take(1), delay(1000))
      .subscribe(() => {
        // Remove this line, just loading imitation
        this.loadRunnersList();
      }); // Remove this line, just loading imitation
  }

  /**
   * On Destroy
   */
  ngOnDestroy() {
    this.subscriptions.forEach((el) => el.unsubscribe());
  }

  /**
   * Load Runners List from services through data-source
   */
  loadRunnersList() {
    this.selection.clear();
    const queryParams = new QueryParamsModel(
      this.filterConfiguration(),
      this.sort.direction,
      this.sort.active,
      this.paginator.pageIndex,
      this.paginator.pageSize
    );
    // Call request from server
    this.store.dispatch(new RunnersPageRequested({ page: queryParams }));
    this.selection.clear();
  }

  /**
   * Returns object for filter
   */
  filterConfiguration(): any {
    const filter: any = {};
    const searchText: string = this.searchInput.nativeElement.value;

    if (this.filterStatus && this.filterStatus.length > 0) {
      filter.status = +this.filterStatus;
    }

    if (this.filterType && this.filterType.length > 0) {
      filter.type = +this.filterType;
    }

    filter.runnername = searchText;
    if (!searchText) {
      return filter;
    }

    filter.email = searchText;
    filter.trophies = searchText;
    filter.clan = searchText;
    return filter;
  }

  /** ACTIONS */
  /**
   * Delete runner
   *
   * @param _item: RunnerModel
   */
  deleteRunner(_item: RunnerModel) {
    return;
  }

  /**
   * Delete selected runners
   */
  deleteRunners() {
    return;
  }

  /**
   * Fetch selected runners
   */
  fetchRunners() {
    const messages = [];
    this.selection.selected.forEach((elem) => {
      messages.push({
        text: `${elem.name}`,
        id: elem.id.toString(),
      });
    });
    this.layoutUtilsService.fetchElements(messages);
  }

  /**
   * Show UpdateStatuDialog for selected runners
   */
  updateStatusForRunners() {
    return;
  }

  /**
   * Show add runner dialog
   */
  addRunner() {}

  /**
   * Show Edit runner dialog and save after success close result
   * @param id: number
   */
  editRunner(id: number) {
    this.router.navigate([this.router.url + '/edit', id], {
      relativeTo: this.activatedRoute,
    });
  }

  /**
   * Check all rows are selected
   */
  isAllSelected(): boolean {
    const numSelected = this.selection.selected.length;
    const numRows = this.runnersResult.length;
    return numSelected === numRows;
  }

  /**
   * Toggle all selections
   */
  masterToggle() {
    if (this.selection.selected.length === this.runnersResult.length) {
      this.selection.clear();
    } else {
      this.runnersResult.forEach((row) => this.selection.select(row));
    }
  }
  //0 - admin , 1 - active , 2 - verify , 3 - blocked , 4 - delete

  /** UI */
  /**
   * Retursn CSS Class Name by status
   *
   * @param status: number
   */
  getItemCssClassByStatus(status: string = 'Common'): string {
    switch (status) {
      case 'Elite':
        return 'elite';
      case 'Legend':
        return 'legend';
      case 'Pro':
        return 'pro';
      case 'Common':
        return 'common';
    }
    return '';
  }

  /**
   * Returns Item Status in string
   * @param status: number
   */
  getItemStatusString(status: number = 0): string {
    switch (status) {
      case 0:
        return 'Offline';
      case 1:
        return 'Online';
    }
    return '';
  }

  /**
   * Returns CSS Class Name by type
   * @param status: number
   */
  getItemCssClassByType(status: number = 0): string {
    switch (status) {
      case 0:
        return 'accent';
      case 1:
        return 'primary';
      case 2:
        return 'warn';
    }
    return '';
  }

  /**
   * Returns Item Type in string
   * 0 - admin , 1 - active , 2 - verify , 3 - blocked , 4 - delete
   * @param status: number
   */
  getItemTypeString(status: number = 0): string {
    switch (status) {
      case 1:
        return 'Gems';
      case 2:
        return 'Gold';
      case 3:
        return 'Eggs';
      case 4:
        return 'Daily Deals';
    }
  }
}
