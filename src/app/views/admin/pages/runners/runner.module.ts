import { NgModule } from '@angular/core';
import { RunnerListComponent } from './runner-list/runner-list.component';
import { RunnerEditComponent } from './runner-edit/runner-edit.component';
import { RouterModule } from '@angular/router';
import { StoreModule } from '@ngrx/store';
import {
  RunnerEffects,
  runnersReducer,
  RunnersService,
} from '../../../../core/api';
import { EffectsModule } from '@ngrx/effects';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatTableModule } from '@angular/material/table';
import { MatMenuModule } from '@angular/material/menu';
import { PartialsModule } from '../../../partials/partials.module';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { CommonModule } from '@angular/common';
import { MatIconModule } from '@angular/material/icon';
import { MatSortModule } from '@angular/material/sort';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { NgxPermissionsModule } from 'ngx-permissions';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatButtonModule } from '@angular/material/button';
import { MaterialFileInputModule } from 'ngx-material-file-input';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatSelectModule } from '@angular/material/select';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: '',
        component: RunnerListComponent,
      },
      {
        path: 'edit/:id',
        component: RunnerEditComponent,
      },
    ]),
    StoreModule.forFeature('runners', runnersReducer),
    EffectsModule.forFeature([RunnerEffects]),
    MatPaginatorModule,
    MatProgressSpinnerModule,
    MatTableModule,
    MatMenuModule,
    PartialsModule,
    MatFormFieldModule,
    MatInputModule,
    CommonModule,
    MatIconModule,
    MatSortModule,
    MatCheckboxModule,
    NgxPermissionsModule,
    MatTooltipModule,
    MatButtonModule,
    MaterialFileInputModule,
    ReactiveFormsModule,
    FormsModule,
    MatSelectModule,
  ],
  providers: [RunnersService],
  declarations: [RunnerListComponent, RunnerEditComponent],
})
export class RunnerAdminModule {}
