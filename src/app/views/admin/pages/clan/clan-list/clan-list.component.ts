import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import {
  ManyClansDeleted,
  OneClanDeleted,
  ClansModel,
  ClansDataSource,
  ClansPageRequested,
  ClansStatusUpdated,
} from '../../../../../core/api';
import { SelectionModel } from '@angular/cdk/collections';
import { fromEvent, merge, of, Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import {
  LayoutUtilsService,
  MessageType,
  QueryParamsModel,
} from '../../../../../core/_base/crud';
import { TranslateService } from '@ngx-translate/core';
import { Store } from '@ngrx/store';
import { IAppState } from '../../../../../core/reducers';
import {
  debounceTime,
  delay,
  distinctUntilChanged,
  skip,
  take,
  tap,
} from 'rxjs/operators';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'kt-clan-list',
  templateUrl: './clan-list.component.html',
  styles: [],
})
export class ClanListComponent implements OnInit {
  dataSource: ClansDataSource;
  displayedColumns = [
    'select',
    'id',
    'country',
    'members',
    'lider',
    'trophies',
    'level',
    'xp',
    'date_registration',
    'type',
    'status',
    'actions',
  ];
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild('sort1', { static: true }) sort: MatSort;
  // Filter fields
  @ViewChild('searchInput', { static: true }) searchInput: ElementRef;
  filterStatus = '';
  filterType = '';
  // Selection
  selection = new SelectionModel<ClansModel>(true, []);
  clansResult: ClansModel[] = [];
  // Subscriptions
  private subscriptions: Subscription[] = [];

  /**
   * Component constructor
   *
   * @param dialog: MatDialog
   * @param snackBar: MatSnackBar
   * @param layoutUtilsService: LayoutUtilsService
   * @param translate: TranslateService
   * @param store: Store<AppState>
   */
  constructor(
    public dialog: MatDialog,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    public snackBar: MatSnackBar,
    private layoutUtilsService: LayoutUtilsService,
    private translate: TranslateService,
    private store: Store<IAppState>
  ) {}

  /**
   * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
   */

  /**
   * On init
   */
  ngOnInit() {
    // If the user changes the sort order, reset back to the first page.
    const sortSubscription = this.sort.sortChange.subscribe(
      () => (this.paginator.pageIndex = 0)
    );
    this.subscriptions.push(sortSubscription);

    /* Data load will be triggered in two cases:
    - when a pagination event occurs => this.paginator.page
    - when a sort event occurs => this.sort.sortChange
    **/
    const paginatorSubscriptions = merge(
      this.sort.sortChange,
      this.paginator.page
    )
      .pipe(tap(() => this.loadClansList()))
      .subscribe();
    this.subscriptions.push(paginatorSubscriptions);

    // Filtration, bind to searchInput
    const searchSubscription = fromEvent(
      this.searchInput.nativeElement,
      'keyup'
    )
      .pipe(
        // tslint:disable-next-line:max-line-length
        debounceTime(50), // The user can type quite quickly in the input box, and that could trigger a lot of server requests. With this operator, we are limiting the amount of server requests emitted to a maximum of one every 150ms
        distinctUntilChanged(), // This operator will eliminate duplicate values
        tap(() => {
          this.paginator.pageIndex = 0;
          this.loadClansList();
        })
      )
      .subscribe();
    this.subscriptions.push(searchSubscription);

    // Init DataSource
    this.dataSource = new ClansDataSource(this.store);
    const entitiesSubscription = this.dataSource.entitySubject
      .pipe(skip(1), distinctUntilChanged())
      .subscribe((res) => {
        this.clansResult = res;
      });
    this.subscriptions.push(entitiesSubscription);
    // First load
    of(undefined)
      .pipe(take(1), delay(1000))
      .subscribe(() => {
        // Remove this line, just loading imitation
        this.loadClansList();
      }); // Remove this line, just loading imitation
  }

  /**
   * On Destroy
   */
  ngOnDestroy() {
    this.subscriptions.forEach((el) => el.unsubscribe());
  }

  /**
   * Load Customers List from services through data-source
   */
  loadClansList() {
    this.selection.clear();
    const queryParams = new QueryParamsModel(
      this.filterConfiguration(),
      this.sort.direction,
      this.sort.active,
      this.paginator.pageIndex,
      this.paginator.pageSize
    );
    // Call request from server
    this.store.dispatch(new ClansPageRequested({ page: queryParams }));
    this.selection.clear();
  }

  /**
   * Returns object for filter
   */
  filterConfiguration(): any {
    const filter: any = {};
    const searchText: string = this.searchInput.nativeElement.value;

    if (this.filterStatus && this.filterStatus.length > 0) {
      filter.status = +this.filterStatus;
    }

    if (this.filterType && this.filterType.length > 0) {
      filter.type = +this.filterType;
    }

    filter.username = searchText;
    if (!searchText) {
      return filter;
    }

    filter.email = searchText;
    filter.trophies = searchText;
    filter.clan = searchText;
    return filter;
  }

  /** ACTIONS */
  /**
   * Delete customer
   *
   * @param _item: CustomerModel
   */
  deleteClan(_item: ClansModel) {
    return;
  }

  /**
   * Delete selected customers
   */
  deleteClans() {
    return;
  }

  /**
   * Fetch selected customers
   */
  fetchClans() {
    return;
  }

  /**
   * Show UpdateStatuDialog for selected customers
   */
  updateStatusForClans() {
    const _title = 'Clan Update';
    const _updateMessage = 'Clan Update';
    const _statuses = [
      { value: 0, text: 'Offline' },
      { value: 1, text: 'Online' },
    ];
    const _messages = [];

    this.selection.selected.forEach((elem) => {
      _messages.push({
        text: `${elem.name}`,
        id: elem.id.toString(),
        status: elem.status,
        statusTitle: this.getItemStatusString(elem.status),
        statusCssClass: this.getItemCssClassByStatus(elem.status),
      });
    });

    const dialogRef = this.layoutUtilsService.updateStatusForEntities(
      _title,
      _statuses,
      _messages
    );
    dialogRef.afterClosed().subscribe((res) => {
      if (!res) {
        this.selection.clear();
        return;
      }

      this.store.dispatch(
        new ClansStatusUpdated({
          status: +res,
          clans: this.selection.selected,
        })
      );

      this.layoutUtilsService.showActionNotification(
        _updateMessage,
        MessageType.Update,
        10000,
        true,
        true
      );
      this.selection.clear();
    });
  }

  /**
   * Show add customer dialog
   */
  addClan() {}

  /**
   * Show Edit customer dialog and save after success close result
   * @param id: number
   */
  editClan(id: number) {
    this.router.navigate([this.router.url + '/edit', id], {
      relativeTo: this.activatedRoute,
    });
  }

  /**
   * Check all rows are selected
   */
  isAllSelected(): boolean {
    const numSelected = this.selection.selected.length;
    const numRows = this.clansResult.length;
    return numSelected === numRows;
  }

  /**
   * Toggle all selections
   */
  masterToggle() {
    if (this.selection.selected.length === this.clansResult.length) {
      this.selection.clear();
    } else {
      this.clansResult.forEach((row) => this.selection.select(row));
    }
  }

  /** UI */
  /**
   * Retursn CSS Class Name by status
   *
   * @param status: number
   */
  getItemCssClassByStatus(status: number = 0): string {
    switch (status) {
      case 0:
        return 'metal';
      case 1:
        return 'success';
    }
    return '';
  }

  /**
   * Returns Item Status in string
   * @param status: number
   */
  getItemStatusString(status: number = 0): string {
    switch (status) {
      case 0:
        return 'Offline';
      case 1:
        return 'Online';
    }
    return '';
  }

  /**
   * Returns CSS Class Name by type
   * @param status: number
   */
  getItemCssClassByType(status: number = 0): string {
    switch (status) {
      case 0:
        return 'accent';
      case 1:
        return 'primary';
      case 2:
        return '';
    }
    return '';
  }

  /**
   * Returns Item Type in string
   * 0 - admin , 1 - active , 2 - verify , 3 - blocked , 4 - delete
   * @param status: number
   */
  getItemTypeString(status: number = 0): string {
    switch (status) {
      case 0:
        return 'Admin';
      case 1:
        return 'Active';
      case 2:
        return 'Verify';
      case 3:
        return 'Blocked';
      case 4:
        return 'Delete';
    }
    return '';
  }
}
