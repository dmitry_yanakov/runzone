import { CommonModule } from '@angular/common';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatMenuModule } from '@angular/material/menu';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSelectModule } from '@angular/material/select';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { MatTooltipModule } from '@angular/material/tooltip';
import { RouterModule } from '@angular/router';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { NgxPermissionsAllowStubModule } from 'ngx-permissions';
import { InterceptService } from '../../../../core/_base/crud';
import {
  activitiesReducer,
  ActivityEffects,
  ClanEffects,
  ClanLevelEffects,
  clanLevelsReducer,
  ClanLevelsService,
  clansReducer,
  ClansService,
  UserEffects,
  usersReducer,
  UsersService,
} from '../../../../core/api';
import { PartialsModule } from '../../../partials/partials.module';
import { LayoutEditComponent } from '../users/user-edit/layout/layout.component';
import { ClanAdminEditModule } from './clan-edit/clan-edit.module';
import { ClanComponent } from './clan-edit/clan/clan.component';
import { LayoutClanEditComponent } from './clan-edit/layout/layout.component';
import { MembersComponent } from './clan-edit/members/members.component';
import { ClanLevelsComponent } from './clan-levels/clan-levels.component';
import { ClanListComponent } from './clan-list/clan-list.component';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: '',
        component: ClanListComponent,
      },
      {
        path: 'edit/:id',
        component: LayoutClanEditComponent,
      },
      {
        path: 'levels',
        component: ClanLevelsComponent,
      },
    ]),
    ClanAdminEditModule,
    PartialsModule,
    MatButtonModule,
    MatTooltipModule,
    MatFormFieldModule,
    MatSelectModule,
    CommonModule,
    MatIconModule,
    MatTableModule,
    MatSortModule,
    MatCheckboxModule,
    NgxPermissionsAllowStubModule,
    MatMenuModule,
    MatProgressSpinnerModule,
    MatPaginatorModule,
    MatInputModule,
    StoreModule.forFeature('users', usersReducer),
    EffectsModule.forFeature([UserEffects]),
    StoreModule.forFeature('clans', clansReducer),
    EffectsModule.forFeature([ClanEffects]),
    StoreModule.forFeature('clanLevels', clanLevelsReducer),
    EffectsModule.forFeature([ClanLevelEffects]),
  ],
  providers: [
    InterceptService,
    {
      multi: true,
      provide: HTTP_INTERCEPTORS,
      useClass: InterceptService,
    },
    ClansService,
    UsersService,
    ClanLevelsService,
  ],
  declarations: [ClanListComponent, ClanLevelsComponent],
})
export class ClanAdminModule {}
