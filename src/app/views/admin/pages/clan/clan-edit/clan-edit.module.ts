// ,ClanLevelsComponent,ClanComponent,MembersComponent
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatMenuModule } from '@angular/material/menu';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSelectModule } from '@angular/material/select';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { MatTabsModule } from '@angular/material/tabs';
import { MatTooltipModule } from '@angular/material/tooltip';
import { NgxPermissionsModule } from 'ngx-permissions';
import { PartialsModule } from '../../../../partials/partials.module';
import { PagesAdminModule } from '../../pages.module';
import { ClanLevelsComponent } from '../clan-levels/clan-levels.component';
import { ClanComponent } from './clan/clan.component';
import { LayoutClanEditComponent } from './layout/layout.component';
import { MembersComponent } from './members/members.component';

@NgModule({
  imports: [
    PartialsModule,
    MatButtonModule,
    MatTooltipModule,
    MatMenuModule,
    MatIconModule,
    MatTabsModule,
    CommonModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatDatepickerModule,
    PagesAdminModule,
    MatTableModule,
    NgxPermissionsModule,
    MatSortModule,
    MatCheckboxModule,
    MatProgressSpinnerModule,
    MatPaginatorModule,
    ReactiveFormsModule,
    FormsModule,
  ],
  declarations: [ClanComponent, MembersComponent, LayoutClanEditComponent],
})
export class ClanAdminEditModule {}
