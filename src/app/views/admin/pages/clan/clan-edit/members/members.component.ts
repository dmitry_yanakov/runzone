import { Component, Input, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ClansModel } from '../../../../../../core/api';
import { FormGroup } from '@angular/forms';
import { ConfigUserList } from '../../../users/user-list/config-user-list';

@Component({
  selector: 'kt-clan-members',
  templateUrl: './members.component.html',
  styleUrls: ['./members.component.scss'],
})
export class MembersComponent implements OnInit {
  @Input() clanId$: Observable<number>;
  clanId: number;
  clan: ClansModel;
  user: ConfigUserList;
  constructor() {}

  ngOnInit(): void {
    this.user = new (class implements ConfigUserList {
      titleDate = 'Date joined';

      getStatus(id: number) {
        switch (id) {
          case 0:
            return 'Member';
          case 1:
            return 'Lider';
          case 2:
            return 'Co-lider';
          case 3:
            return 'request';
        }
      }
    })();

    this.clan = new ClansModel();
    this.clan.clear();
  }
}
