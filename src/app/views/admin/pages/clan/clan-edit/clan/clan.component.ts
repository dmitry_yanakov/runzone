import { Component, Input, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ClansModel } from '../../../../../../core/api';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';

@Component({
  selector: 'kt-clan',
  templateUrl: './clan.component.html',
  styleUrls: ['./clan.component.scss'],
})
export class ClanComponent implements OnInit {
  @Input() clanId$: Observable<number>;
  clanId: number;
  hasFormErrors: boolean;
  clan: ClansModel;
  clanForm: FormGroup;
  datePicker;

  constructor(private clanFB: FormBuilder) {}

  ngOnInit(): void {
    this.createForm();
    this.hasFormErrors = false;
    this.clan = new ClansModel();
    this.clan.clear();
  }

  onAlertClose($event: boolean) {}

  createForm() {
    this.clanForm = this.clanFB.group({
      name: new FormControl('', [Validators.required, Validators.minLength(3)]),
      description: new FormControl('', Validators.required),
      type: new FormControl('', Validators.required),
      dateCreate: new FormControl('', Validators.required),
      country: new FormControl('', Validators.required),
      city: new FormControl('', Validators.required),
      language: new FormControl('', Validators.required),
      trophiesReq: new FormControl('', Validators.required),
      levelReq: new FormControl('', Validators.required),
      level: new FormControl('', Validators.required),
      trophies: new FormControl('', [Validators.required]),
      lider: new FormControl('', [Validators.required]),
    });
  }
}
