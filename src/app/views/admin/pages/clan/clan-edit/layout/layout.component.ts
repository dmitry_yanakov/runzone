import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { BehaviorSubject, Observable, of, Subscription } from 'rxjs';
import {
  selectClanById,
  ClansModel,
  ClansService,
} from '../../../../../../core/api';
import { FormBuilder, FormGroup } from '@angular/forms';
import { select, Store } from '@ngrx/store';
import { IAppState } from '../../../../../../core/reducers';
import { ActivatedRoute, Router } from '@angular/router';
import {
  LayoutUtilsService,
  TypesUtilsService,
} from '../../../../../../core/_base/crud';
import { MatDialog } from '@angular/material/dialog';
import {
  LayoutConfigService,
  SubheaderService,
} from '../../../../../../core/_base/layout';

@Component({
  selector: 'kt-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss'],
})
export class LayoutClanEditComponent implements OnInit {
  @Input() clanId$: Observable<number>;

  clan: ClansModel;
  clanId: Observable<number>;
  oldUser: ClansModel;
  selectedTab = 0;
  loadingSubject = new BehaviorSubject<boolean>(true);
  loading$: Observable<boolean>;
  clanForm: FormGroup;
  hasFormErrors = false;
  availableYears: number[] = [];
  filteredColors: Observable<string[]>;
  filteredManufactures: Observable<string[]>;
  // Private password
  private componentSubscriptions: Subscription;
  // sticky portlet header margin
  private headerMargin: number;

  constructor(
    private store: Store<IAppState>,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private typesUtilsService: TypesUtilsService,
    private clanFB: FormBuilder,
    public dialog: MatDialog,
    private subheaderService: SubheaderService,
    private layoutUtilsService: LayoutUtilsService,
    private layoutConfigService: LayoutConfigService,
    private clanService: ClansService,
    private cdr: ChangeDetectorRef
  ) {}

  ngOnInit(): void {
    for (let i = 2019; i > 1945; i--) {
      this.availableYears.push(i);
    }
    this.loading$ = this.loadingSubject.asObservable();
    this.loadingSubject.next(true);
    this.activatedRoute.params.subscribe((params) => {
      const id = params.id;
      if (id && id > 0) {
        this.store.pipe(select(selectClanById(id))).subscribe((result) => {
          if (!result) {
            this.loadUserFromService(id);
            return;
          }

          this.loadUser(result);
        });
      } else {
        const newUser = new ClansModel();
        newUser.clear();
        newUser.id = 55; // tests
        this.loadUser(newUser);
      }
    });

    // sticky portlet header
    window.onload = () => {
      const style = getComputedStyle(document.getElementById('kt_header'));
      this.headerMargin = parseInt(style.height, 0);
    };
  }

  loadUser(_clan, fromService: boolean = false) {
    if (!_clan) {
      this.goBack('');
    }
    this.clan = _clan;
    this.clanId$ = of(_clan.id);
    this.oldUser = Object.assign({}, _clan);
    this.initUser();
    if (fromService) {
      this.cdr.detectChanges();
    }
  }

  // If product didn't find in store
  loadUserFromService(clanId) {
    this.clanService.getClanById(clanId).subscribe((res) => {
      this.loadUser(res, true);
    });
  }

  goBack(id) {
    this.loadingSubject.next(false);
    const url = `admin/clans/edit?id=${id}`;
    this.router.navigateByUrl(url, { relativeTo: this.activatedRoute });
  }

  initUser() {}

  onSumbit(b: boolean) {}

  getComponentTitle() {
    let result = 'Create clan';
    if (!this.clan || !this.clan.id) {
      return result;
    }

    result = `Edit clan - ${this.clan.name} ${this.clan.name}`;
    return result;
  }

  goBackWithoutId() {
    this.router.navigateByUrl('admin/clans', {
      relativeTo: this.activatedRoute,
    });
  }

  reset() {}
}
