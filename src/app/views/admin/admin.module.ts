// Angular
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
// Components
import { BaseComponent } from '../theme/base/base.component';
import { ErrorPageComponent } from '../theme/content/error-page/error-page.component';
// Auth
import { AuthGuard } from '../../core/api/auth';
import { ThemeModule } from '../theme/theme.module';
import { AdminComponent } from './admin.component';
import { PartialsModule } from '../partials/partials.module';
import { CommonModule } from '@angular/common';
import { AchievementComponent } from './pages/achievement/achievement.component';
import { InterceptService } from '../../core/_base/crud';
import { HTTP_INTERCEPTORS } from '@angular/common/http';

const routes: Routes = [
  {
    path: '',
    component: BaseComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: 'users',
        loadChildren: () =>
          import('app/views/admin/pages/users/users.module').then(
            (m) => m.UserAdminModule
          ),
      },
      {
        path: 'clans',
        loadChildren: () =>
          import('app/views/admin/pages/clan/clan.module').then(
            (m) => m.ClanAdminModule
          ),
      },
      {
        path: 'dashboard',
        loadChildren: () =>
          import('app/views/admin/pages/dashboard/dashboard.module').then(
            (m) => m.DashboardModule
          ),
      },
      {
        path: 'achievement',
        component: AchievementComponent,
      },
      {
        path: 'shop',
        loadChildren: () =>
          import('app/views/admin/pages/shop/shop-item.module').then(
            (m) => m.ShopItemAdminModule
          ),
      },
      {
        path: 'levels',
        loadChildren: () =>
          import('app/views/admin/pages/level/level.module').then(
            (m) => m.LevelAdminModule
          ),
      },
      {
        path: 'eggs',
        loadChildren: () =>
          import('app/views/admin/pages/egg/egg.module').then(
            (m) => m.EggAdminModule
          ),
      },
      {
        path: 'runners',
        loadChildren: () =>
          import('app/views/admin/pages/runners/runner.module').then(
            (m) => m.RunnerAdminModule
          ),
      },
      {
        path: 'ranks',
        loadChildren: () =>
          import('app/views/admin/pages/ranks/rank.module').then(
            (m) => m.RankAdminModule
          ),
      },
      {
        path: 'customizes',
        loadChildren: () =>
          import('app/views/admin/pages/customizes/customize.module').then(
            (m) => m.CustomizeAdminModule
          ),
      },
      { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
      {
        path: 'error/403',
        component: ErrorPageComponent,
        data: {
          type: 'error-v6',
          code: 403,
          title: '403... Access forbidden',
          desc:
            "Looks like you don't have permission to access for requested page.<br> Please, contact administrator",
        },
      },
      { path: '404', redirectTo: 'dashboard' },
      { path: 'error/:type', component: ErrorPageComponent },
    ],
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    ThemeModule,
    PartialsModule,
    CommonModule,
  ],
  providers: [
    InterceptService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: InterceptService,
      multi: true,
    },
  ],
  exports: [RouterModule],
  declarations: [AdminComponent],
  bootstrap: [AdminComponent],
  entryComponents: [],
})
export class AdminRoutingModule {}
