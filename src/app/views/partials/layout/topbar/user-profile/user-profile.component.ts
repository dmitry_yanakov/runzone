// Angular
import { Component, Input, OnInit } from '@angular/core';
// RxJS
import { Observable } from 'rxjs';
// NGRX
import { select, Store } from '@ngrx/store';
// State
import { IAppState } from '../../../../../core/reducers';
import { AuthSelectors } from '../../../../../core/api/auth/store/selectors';
import { Logout } from '../../../../../core/api/auth/store/actions';
import { CurrentUser } from '../../../../../core/api/auth/types';

@Component({
  selector: 'kt-user-profile',
  templateUrl: './user-profile.component.html',
})
export class UserProfileComponent implements OnInit {
  // Public properties
  user$: Observable<CurrentUser>;

  @Input() avatar = true;
  @Input() greeting = true;
  @Input() badge: boolean;
  @Input() icon: boolean;

  /**
   * Component constructor
   *
   * @param store: Store<AppState>
   */
  constructor(private store: Store<IAppState>) {}

  /**
   * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
   */

  /**
   * On init
   */
  ngOnInit(): void {
    this.user$ = this.store.pipe(select(AuthSelectors.currentUser));
    
  }

  /**
   * Log out
   */
  logout() {
    localStorage.removeItem('token');
    this.store.dispatch(new Logout());
  }
}
