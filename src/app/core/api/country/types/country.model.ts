import { BaseModel } from '../../../_base/crud';

export class CountryModel extends BaseModel {
  name: string;
  id: number;
  ISO: string;

  clear() {
    this.id = 0;
    this.name = '';
    this.ISO = '';
  }
}
