// NGRX
import { createFeatureSelector } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter, Update } from '@ngrx/entity';
// Actions
import { ClanLevelActions,ClanLevelActionTypes } from '../_actions/clan-level.actions';
// Models
import { ClanLevelsModel } from '../_models/clan-levels.model';
import { QueryParamsModel } from '../../_base/crud';

export interface ClanLevelsState extends EntityState<ClanLevelsModel> {
  listLoading: boolean;
  actionsloading: boolean;
  totalCount: number;
  lastCreatedClanLevelId: number;
  lastQuery: QueryParamsModel;
  showInitWaitingMessage: boolean;
}

export const adapter: EntityAdapter<ClanLevelsModel> = createEntityAdapter<ClanLevelsModel>();

export const initialClanLevelsState: ClanLevelsState = adapter.getInitialState({
  clanLevelForEdit: null,
  listLoading: false,
  actionsloading: false,
  totalCount: 0,
  lastCreatedClanLevelId: undefined,
  lastQuery: new QueryParamsModel({}),
  showInitWaitingMessage: true
});

export function clanLevelsReducer(state = initialClanLevelsState, action: ClanLevelActions): ClanLevelsState {
  switch  (action.type) {
    case ClanLevelActionTypes.ClanLevelsPageToggleLoading: {
      return {
        ...state, listLoading: action.payload.isLoading, lastCreatedClanLevelId: undefined
      };
    }
    case ClanLevelActionTypes.ClanLevelActionToggleLoading: {
      return {
        ...state, actionsloading: action.payload.isLoading
      };
    }
    case ClanLevelActionTypes.ClanLevelOnServerCreated: return {
      ...state
    };
    case ClanLevelActionTypes.ClanLevelCreated: return adapter.addOne(action.payload.clanLevel, {
      ...state, lastCreatedClanLevelId: action.payload.clanLevel.id
    });
    case ClanLevelActionTypes.ClanLevelUpdated: return adapter.updateOne(action.payload.partialClanLevel, state);
    case ClanLevelActionTypes.OneClanLevelDeleted: return adapter.removeOne(action.payload.id, state);
    case ClanLevelActionTypes.ManyClanLevelsDeleted: return adapter.removeMany(action.payload.ids, state);
    case ClanLevelActionTypes.ClanLevelsPageCancelled: {
      return {
        ...state, listLoading: false, lastQuery: new QueryParamsModel({})
      };
    }
    case ClanLevelActionTypes.ClanLevelsPageLoaded: {
      return adapter.addMany(action.payload.clanLevels, {
        ...initialClanLevelsState,
        totalCount: action.payload.totalCount,
        listLoading: false,
        lastQuery: action.payload.page,
        showInitWaitingMessage: false
      });
    }
    default: return state;
  }
}

export const getClanLevelState = createFeatureSelector<ClanLevelsModel>('clanLevels');

export const {
  selectAll,
  selectEntities,
  selectIds,
  selectTotal
} = adapter.getSelectors();
