// NGRX
import { createFeatureSelector } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter, Update } from '@ngrx/entity';
// Actions
import { EggActions, EggActionTypes } from '../_actions/egg.actions';
// Models
import { EggModel } from '../_models/egg.models';
import { QueryParamsModel } from '../../_base/crud';

export interface EggsState extends EntityState<EggModel> {
  listLoading: boolean;
  actionsloading: boolean;
  totalCount: number;
  lastCreatedEggId: number;
  lastQuery: QueryParamsModel;
  showInitWaitingMessage: boolean;
}

export const adapter: EntityAdapter<EggModel> = createEntityAdapter<EggModel>();

export const initialEggsState: EggsState = adapter.getInitialState({
  eggForEdit: null,
  listLoading: false,
  actionsloading: false,
  totalCount: 0,
  lastCreatedEggId: undefined,
  lastQuery: new QueryParamsModel({}),
  showInitWaitingMessage: true
});

export function eggsReducer(state = initialEggsState, action: EggActions): EggsState {
  switch  (action.type) {
    case EggActionTypes.EggsPageToggleLoading: {
      return {
        ...state, listLoading: action.payload.isLoading, lastCreatedEggId: undefined
      };
    }
    case EggActionTypes.EggActionToggleLoading: {
      return {
        ...state, actionsloading: action.payload.isLoading
      };
    }
    case EggActionTypes.EggOnServerCreated: return {
      ...state
    };
    case EggActionTypes.EggCreated: return adapter.addOne(action.payload.egg, {
      ...state, lastCreatedEggId: action.payload.egg.id
    });
    case EggActionTypes.EggUpdated: return adapter.updateOne(action.payload.partialEgg, state);
    case EggActionTypes.OneEggDeleted: return adapter.removeOne(action.payload.id, state);
    case EggActionTypes.ManyEggsDeleted: return adapter.removeMany(action.payload.ids, state);
    case EggActionTypes.EggsPageCancelled: {
      return {
        ...state, listLoading: false, lastQuery: new QueryParamsModel({})
      };
    }
    case EggActionTypes.EggsPageLoaded: {
      return adapter.addMany(action.payload.eggs, {
        ...initialEggsState,
        totalCount: action.payload.totalCount,
        listLoading: false,
        lastQuery: action.payload.page,
        showInitWaitingMessage: false
      });
    }
    default: return state;
  }
}

export const getEggState = createFeatureSelector<EggModel>('eggs');

export const {
  selectAll,
  selectEntities,
  selectIds,
  selectTotal
} = adapter.getSelectors();
