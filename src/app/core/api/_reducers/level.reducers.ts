// NGRX
import { createFeatureSelector } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter, Update } from '@ngrx/entity';
// Actions
import { LevelActions, LevelActionTypes } from '../_actions/level.actions';
// Models
import { LevelModel } from '../_models/level.model';
import { QueryParamsModel } from '../../_base/crud';

export interface LevelsState extends EntityState<LevelModel> {
  listLoading: boolean;
  actionsloading: boolean;
  totalCount: number;
  lastCreatedLevelId: number;
  lastQuery: QueryParamsModel;
  showInitWaitingMessage: boolean;
}

export const adapter: EntityAdapter<LevelModel> = createEntityAdapter<LevelModel>();

export const initialLevelsState: LevelsState = adapter.getInitialState({
  levelForEdit: null,
  listLoading: false,
  actionsloading: false,
  totalCount: 0,
  lastCreatedLevelId: undefined,
  lastQuery: new QueryParamsModel({}),
  showInitWaitingMessage: true
});

export function levelsReducer(state = initialLevelsState, action: LevelActions): LevelsState {
  switch  (action.type) {
    case LevelActionTypes.LevelsPageToggleLoading: {
      return {
        ...state, listLoading: action.payload.isLoading, lastCreatedLevelId: undefined
      };
    }
    case LevelActionTypes.LevelActionToggleLoading: {
      return {
        ...state, actionsloading: action.payload.isLoading
      };
    }
    case LevelActionTypes.LevelOnServerCreated: return {
      ...state
    };
    case LevelActionTypes.LevelCreated: return adapter.addOne(action.payload.level, {
      ...state, lastCreatedLevelId: action.payload.level.id
    });
    case LevelActionTypes.LevelUpdated: return adapter.updateOne(action.payload.partialLevel, state);
    case LevelActionTypes.OneLevelDeleted: return adapter.removeOne(action.payload.id, state);
    case LevelActionTypes.ManyLevelsDeleted: return adapter.removeMany(action.payload.ids, state);
    case LevelActionTypes.LevelsPageCancelled: {
      return {
        ...state, listLoading: false, lastQuery: new QueryParamsModel({})
      };
    }
    case LevelActionTypes.LevelsPageLoaded: {
      return adapter.addMany(action.payload.levels, {
        ...initialLevelsState,
        totalCount: action.payload.totalCount,
        listLoading: false,
        lastQuery: action.payload.page,
        showInitWaitingMessage: false
      });
    }
    default: return state;
  }
}

export const getLevelState = createFeatureSelector<LevelModel>('levels');

export const {
  selectAll,
  selectEntities,
  selectIds,
  selectTotal
} = adapter.getSelectors();
