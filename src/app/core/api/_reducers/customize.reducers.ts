// NGRX
import { createFeatureSelector } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter, Update } from '@ngrx/entity';
// Actions
import { CustomizeActions, CustomizeActionTypes } from '../_actions/customize.actions';
// Models
import { CustomizeModel } from '../_models/customize.model';
import { QueryParamsModel } from '../../_base/crud';

export interface CustomizesState extends EntityState<CustomizeModel> {
  listLoading: boolean;
  actionsloading: boolean;
  totalCount: number;
  lastCreatedCustomizeId: number;
  lastQuery: QueryParamsModel;
  showInitWaitingMessage: boolean;
}

export const adapter: EntityAdapter<CustomizeModel> = createEntityAdapter<CustomizeModel>();

export const initialCustomizesState: CustomizesState = adapter.getInitialState({
  customizeForEdit: null,
  listLoading: false,
  actionsloading: false,
  totalCount: 0,
  lastCreatedCustomizeId: undefined,
  lastQuery: new QueryParamsModel({}),
  showInitWaitingMessage: true
});

export function customizesReducer(state = initialCustomizesState, action: CustomizeActions): CustomizesState {
  switch  (action.type) {
    case CustomizeActionTypes.CustomizesPageToggleLoading: {
      return {
        ...state, listLoading: action.payload.isLoading, lastCreatedCustomizeId: undefined
      };
    }
    case CustomizeActionTypes.CustomizeActionToggleLoading: {
      return {
        ...state, actionsloading: action.payload.isLoading
      };
    }
    case CustomizeActionTypes.CustomizeOnServerCreated: return {
      ...state
    };
    case CustomizeActionTypes.CustomizeCreated: return adapter.addOne(action.payload.customize, {
      ...state, lastCreatedCustomizeId: action.payload.customize.id
    });
    case CustomizeActionTypes.CustomizeUpdated: return adapter.updateOne(action.payload.partialCustomize, state);
    case CustomizeActionTypes.OneCustomizeDeleted: return adapter.removeOne(action.payload.id, state);
    case CustomizeActionTypes.ManyCustomizesDeleted: return adapter.removeMany(action.payload.ids, state);
    case CustomizeActionTypes.CustomizesPageCancelled: {
      return {
        ...state, listLoading: false, lastQuery: new QueryParamsModel({})
      };
    }
    case CustomizeActionTypes.CustomizesPageLoaded: {
      return adapter.addMany(action.payload.customizes, {
        ...initialCustomizesState,
        totalCount: action.payload.totalCount,
        listLoading: false,
        lastQuery: action.payload.page,
        showInitWaitingMessage: false
      });
    }
    default: return state;
  }
}

export const getCustomizeState = createFeatureSelector<CustomizeModel>('customizes');

export const {
  selectAll,
  selectEntities,
  selectIds,
  selectTotal
} = adapter.getSelectors();
