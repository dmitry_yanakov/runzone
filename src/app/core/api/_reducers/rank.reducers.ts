// NGRX
import { createFeatureSelector } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter, Update } from '@ngrx/entity';
// Actions
import { RankActions, RankActionTypes } from '../_actions/rank.actions';
// Models
import { RankModel } from '../_models/rank.model';
import { QueryParamsModel } from '../../_base/crud';

export interface RanksState extends EntityState<RankModel> {
  listLoading: boolean;
  actionsloading: boolean;
  totalCount: number;
  lastCreatedRankId: number;
  lastQuery: QueryParamsModel;
  showInitWaitingMessage: boolean;
}

export const adapter: EntityAdapter<RankModel> = createEntityAdapter<RankModel>();

export const initialRanksState: RanksState = adapter.getInitialState({
  rankForEdit: null,
  listLoading: false,
  actionsloading: false,
  totalCount: 0,
  lastCreatedRankId: undefined,
  lastQuery: new QueryParamsModel({}),
  showInitWaitingMessage: true
});

export function ranksReducer(state = initialRanksState, action: RankActions): RanksState {
  switch  (action.type) {
    case RankActionTypes.RanksPageToggleLoading: {
      return {
        ...state, listLoading: action.payload.isLoading, lastCreatedRankId: undefined
      };
    }
    case RankActionTypes.RankActionToggleLoading: {
      return {
        ...state, actionsloading: action.payload.isLoading
      };
    }
    case RankActionTypes.RankOnServerCreated: return {
      ...state
    };
    case RankActionTypes.RankCreated: return adapter.addOne(action.payload.rank, {
      ...state, lastCreatedRankId: action.payload.rank.id
    });
    case RankActionTypes.RankUpdated: return adapter.updateOne(action.payload.partialRank, state);
    case RankActionTypes.OneRankDeleted: return adapter.removeOne(action.payload.id, state);
    case RankActionTypes.ManyRanksDeleted: return adapter.removeMany(action.payload.ids, state);
    case RankActionTypes.RanksPageCancelled: {
      return {
        ...state, listLoading: false, lastQuery: new QueryParamsModel({})
      };
    }
    case RankActionTypes.RanksPageLoaded: {
      return adapter.addMany(action.payload.ranks, {
        ...initialRanksState,
        totalCount: action.payload.totalCount,
        listLoading: false,
        lastQuery: action.payload.page,
        showInitWaitingMessage: false
      });
    }
    default: return state;
  }
}

export const getRankState = createFeatureSelector<RankModel>('ranks');

export const {
  selectAll,
  selectEntities,
  selectIds,
  selectTotal
} = adapter.getSelectors();
