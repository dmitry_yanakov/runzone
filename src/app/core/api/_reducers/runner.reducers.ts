// NGRX
import { createFeatureSelector } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter, Update } from '@ngrx/entity';
// Actions
import { RunnerActions, RunnerActionTypes } from '../_actions/runner.actions';
// Models
import { RunnerModel } from '../_models/runner.models';
import { QueryParamsModel } from '../../_base/crud';

export interface RunnersState extends EntityState<RunnerModel> {
  listLoading: boolean;
  actionsloading: boolean;
  totalCount: number;
  lastCreatedRunnerId: number;
  lastQuery: QueryParamsModel;
  showInitWaitingMessage: boolean;
}

export const adapter: EntityAdapter<RunnerModel> = createEntityAdapter<RunnerModel>();

export const initialRunnersState: RunnersState = adapter.getInitialState({
  runnerForEdit: null,
  listLoading: false,
  actionsloading: false,
  totalCount: 0,
  lastCreatedRunnerId: undefined,
  lastQuery: new QueryParamsModel({}),
  showInitWaitingMessage: true
});

export function runnersReducer(state = initialRunnersState, action: RunnerActions): RunnersState {
  switch  (action.type) {
    case RunnerActionTypes.RunnersPageToggleLoading: {
      return {
        ...state, listLoading: action.payload.isLoading, lastCreatedRunnerId: undefined
      };
    }
    case RunnerActionTypes.RunnerActionToggleLoading: {
      return {
        ...state, actionsloading: action.payload.isLoading
      };
    }
    case RunnerActionTypes.RunnerOnServerCreated: return {
      ...state
    };
    case RunnerActionTypes.RunnerCreated: return adapter.addOne(action.payload.runner, {
      ...state, lastCreatedRunnerId: action.payload.runner.id
    });
    case RunnerActionTypes.RunnerUpdated: return adapter.updateOne(action.payload.partialRunner, state);
    case RunnerActionTypes.OneRunnerDeleted: return adapter.removeOne(action.payload.id, state);
    case RunnerActionTypes.ManyRunnersDeleted: return adapter.removeMany(action.payload.ids, state);
    case RunnerActionTypes.RunnersPageCancelled: {
      return {
        ...state, listLoading: false, lastQuery: new QueryParamsModel({})
      };
    }
    case RunnerActionTypes.RunnersPageLoaded: {
      return adapter.addMany(action.payload.runners, {
        ...initialRunnersState,
        totalCount: action.payload.totalCount,
        listLoading: false,
        lastQuery: action.payload.page,
        showInitWaitingMessage: false
      });
    }
    default: return state;
  }
}

export const getRunnerState = createFeatureSelector<RunnerModel>('runners');

export const {
  selectAll,
  selectEntities,
  selectIds,
  selectTotal
} = adapter.getSelectors();
