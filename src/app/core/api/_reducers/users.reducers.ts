import { createFeatureSelector } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter, Update } from '@ngrx/entity';
// Actions
import { UserActions, UserActionTypes } from '../_actions/users.actions';
// Models
import { UserModel } from '../_models/user.model';
import { QueryParamsModel } from '../../_base/crud';



export interface UsersState extends EntityState<UserModel> {
  listLoading: boolean;
  actionsloading: boolean;
  totalCount: number;
  lastCreatedCustomerId: number;
  lastQuery: QueryParamsModel;
  showInitWaitingMessage: boolean;
}

export const adapter: EntityAdapter<UserModel> = createEntityAdapter<UserModel>();

export const initialUsersState: UsersState = adapter.getInitialState({
  customerForEdit: null,
  listLoading: false,
  actionsloading: false,
  totalCount: 0,
  lastCreatedCustomerId: undefined,
  lastQuery: new QueryParamsModel({}),
  showInitWaitingMessage: true
});

export function usersReducer(state = initialUsersState, action: UserActions): UsersState {
  switch  (action.type) {
    case UserActionTypes.UsersPageToggleLoading: {
      return {
        ...state, listLoading: action.payload.isLoading, lastCreatedCustomerId: undefined
      };
    }
    case UserActionTypes.UserActionToggleLoading: {
      return {
        ...state, actionsloading: action.payload.isLoading
      };
    }
    case UserActionTypes.UserOnServerCreated: return {
      ...state
    };
    case UserActionTypes.UserCreated: return adapter.addOne(action.payload.customer, {
      ...state, lastCreatedCustomerId: action.payload.customer.id
    });
    case UserActionTypes.UserUpdated: return adapter.updateOne(action.payload.partialCustomer, state);
    case UserActionTypes.UsersStatusUpdated: {
      const _partialCustomers: Update<UserModel>[] = [];
      // tslint:disable-next-line:prefer-const
      for (let i = 0; i < action.payload.customers.length; i++) {
        _partialCustomers.push({
          id: action.payload.customers[i].id,
          changes: {
            status: action.payload.status
          }
        });
      }
      return adapter.updateMany(_partialCustomers, state);
    }
    case UserActionTypes.OneUserDeleted: return adapter.removeOne(action.payload.id, state);
    case UserActionTypes.ManyUsersDeleted: return adapter.removeMany(action.payload.ids, state);
    case UserActionTypes.UsersPageCancelled: {
      return {
        ...state, listLoading: false, lastQuery: new QueryParamsModel({})
      };
    }
    case UserActionTypes.UsersPageLoaded: {
      return adapter.addMany(action.payload.customers, {
        ...initialUsersState,
        totalCount: action.payload.totalCount,
        listLoading: false,
        lastQuery: action.payload.page,
        showInitWaitingMessage: false
      });
    }
    default: return state;
  }
}

export const getCustomerState = createFeatureSelector<UserModel>('users');
