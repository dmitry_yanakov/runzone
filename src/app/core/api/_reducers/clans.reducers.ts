// NGRX
import { createEntityAdapter, EntityAdapter, EntityState, Update } from '@ngrx/entity';
import { createFeatureSelector } from '@ngrx/store';
import { QueryParamsModel } from '../../_base/crud';
// Actions
import { ClanActions, ClanActionTypes } from '../_actions/clans.actions';
// Models
import { ClansModel } from '../_models/clans.module';

export interface ClansState extends EntityState<ClansModel> {
  listLoading: boolean;
  actionsloading: boolean;
  totalCount: number;
  lastCreatedClanId: number;
  lastQuery: QueryParamsModel;
  showInitWaitingMessage: boolean;
}

export const adapter: EntityAdapter<ClansModel> = createEntityAdapter<ClansModel>();

export const initialClansState: ClansState = adapter.getInitialState({
  clanForEdit: null,
  listLoading: false,
  actionsloading: false,
  totalCount: 0,
  lastCreatedClanId: undefined,
  lastQuery: new QueryParamsModel({}),
  showInitWaitingMessage: true,
});

export function clansReducer(state = initialClansState, action: ClanActions): ClansState {
  switch  (action.type) {
    case ClanActionTypes.ClansPageToggleLoading: {
      return {
        ...state, listLoading: action.payload.isLoading, lastCreatedClanId: undefined,
      };
    }
    case ClanActionTypes.ClanActionToggleLoading: {
      return {
        ...state, actionsloading: action.payload.isLoading,
      };
    }
    case ClanActionTypes.ClanOnServerCreated: return {
      ...state,
    };
    case ClanActionTypes.ClanCreated: return adapter.addOne(action.payload.clan, {
      ...state, lastCreatedClanId: action.payload.clan.id,
    });
    case ClanActionTypes.ClanUpdated: return adapter.updateOne(action.payload.partialclan, state);
    case ClanActionTypes.ClansStatusUpdated: {
      const _partialClans: Update<ClansModel>[] = [];
      // tslint:disable-next-line:prefer-const
      for (let i = 0; i < action.payload.clans.length; i++) {
        _partialClans.push({
          id: action.payload.clans[i].id,
          changes: {
            status: action.payload.status,
          },
        });
      }
      return adapter.updateMany(_partialClans, state);
    }
    case ClanActionTypes.OneClanDeleted: return adapter.removeOne(action.payload.id, state);
    case ClanActionTypes.ManyClansDeleted: return adapter.removeMany(action.payload.ids, state);
    case ClanActionTypes.ClansPageCancelled: {
      return {
        ...state, listLoading: false, lastQuery: new QueryParamsModel({}),
      };
    }
    case ClanActionTypes.ClansPageLoaded: {
      return adapter.addMany(action.payload.clans, {
        ...initialClansState,
        totalCount: action.payload.totalCount,
        listLoading: false,
        lastQuery: action.payload.page,
        showInitWaitingMessage: false,
      });
    }
    default: return state;
  }
}

export const getClansState = createFeatureSelector<ClansModel>('clans');

export const {
  selectAll,
  selectEntities,
  selectIds,
  selectTotal,
} = adapter.getSelectors();
