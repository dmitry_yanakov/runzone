// NGRX
import {
  createEntityAdapter,
  EntityAdapter,
  EntityState,
  Update,
} from '@ngrx/entity';
import { createFeatureSelector } from '@ngrx/store';
// Actions
import { ActivityActions, ActivityActionTypes } from '..';
// Models
import { ActivitiesModel } from '..';
import { QueryParamsModel } from '../../_base/crud';

export interface ActivitiesState extends EntityState<ActivitiesModel> {
  listLoading: boolean;
  actionsloading: boolean;
  totalCount: number;
  lastCreatedActivityId: number;
  lastQuery: QueryParamsModel;
  showInitWaitingMessage: boolean;
}

export const adapter: EntityAdapter<ActivitiesModel> = createEntityAdapter<
  ActivitiesModel
>();

export const initialActivitiesState: ActivitiesState = adapter.getInitialState({
  activityForEdit: null,
  listLoading: false,
  actionsloading: false,
  totalCount: 0,
  lastCreatedActivityId: undefined,
  lastQuery: new QueryParamsModel({}),
  showInitWaitingMessage: true,
});

export function activitiesReducer(
  state = initialActivitiesState,
  action: ActivityActions
): ActivitiesState {
  switch (action.type) {
    case ActivityActionTypes.ActivitiesPageToggleLoading: {
      return {
        ...state,
        listLoading: action.payload.isLoading,
        lastCreatedActivityId: undefined,
      };
    }
    case ActivityActionTypes.ActivityActionToggleLoading: {
      return {
        ...state,
        actionsloading: action.payload.isLoading,
      };
    }
    case ActivityActionTypes.ActivityOnServerCreated:
      return {
        ...state,
      };
    case ActivityActionTypes.ActivityCreated:
      return adapter.addOne(action.payload.activity, {
        ...state,
        lastCreatedActivityId: action.payload.activity.id,
      });
    case ActivityActionTypes.ActivityUpdated:
      return adapter.updateOne(action.payload.partialActivity, state);
    case ActivityActionTypes.ActivitiesStatusUpdated: {
      const _partialActivities: Update<ActivitiesModel>[] = [];

      for (let i = 0; i < action.payload.activities.length; i++) {
        _partialActivities.push({
          id: action.payload.activities[i].id,
          changes: {
            status: action.payload.status,
          },
        });
      }
      return adapter.updateMany(_partialActivities, state);
    }
    case ActivityActionTypes.OneActivityDeleted:
      return adapter.removeOne(action.payload.id, state);
    case ActivityActionTypes.ManyActivitiesDeleted:
      return adapter.removeMany(action.payload.ids, state);
    case ActivityActionTypes.ActivitiesPageCancelled: {
      return {
        ...state,
        listLoading: false,
        lastQuery: new QueryParamsModel({}),
      };
    }
    case ActivityActionTypes.ActivitiesPageLoaded: {
      return adapter.addMany(action.payload.activities, {
        ...initialActivitiesState,
        totalCount: action.payload.totalCount,
        listLoading: false,
        lastQuery: action.payload.page,
        showInitWaitingMessage: false,
      });
    }
    default:
      return state;
  }
}

export const getActivityState = createFeatureSelector<ActivitiesModel>(
  'activities'
);
