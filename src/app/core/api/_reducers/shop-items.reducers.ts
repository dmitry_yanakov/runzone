// NGRX
import { createFeatureSelector } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter, Update } from '@ngrx/entity';
// Actions
import { ShopItemActions, ShopItemActionTypes } from '../_actions/shop-item.actions';
// Models
import { ShopItemModel } from '../_models/shop-item.model';
import { QueryParamsModel } from '../../_base/crud';

export interface ShopItemsState extends EntityState<ShopItemModel> {
  listLoading: boolean;
  actionsloading: boolean;
  totalCount: number;
  lastCreatedShopItemId: number;
  lastQuery: QueryParamsModel;
  showInitWaitingMessage: boolean;
}

export const adapter: EntityAdapter<ShopItemModel> = createEntityAdapter<ShopItemModel>();

export const initialShopItemsState: ShopItemsState = adapter.getInitialState({
  shopItemForEdit: null,
  listLoading: false,
  actionsloading: false,
  totalCount: 0,
  lastCreatedShopItemId: undefined,
  lastQuery: new QueryParamsModel({}),
  showInitWaitingMessage: true
});

export function shopItemsReducer(state = initialShopItemsState, action: ShopItemActions): ShopItemsState {
  switch  (action.type) {
    case ShopItemActionTypes.ShopItemsPageToggleLoading: {
      return {
        ...state, listLoading: action.payload.isLoading, lastCreatedShopItemId: undefined
      };
    }
    case ShopItemActionTypes.ShopItemActionToggleLoading: {
      return {
        ...state, actionsloading: action.payload.isLoading
      };
    }
    case ShopItemActionTypes.ShopItemOnServerCreated: return {
      ...state
    };
    case ShopItemActionTypes.ShopItemCreated: return adapter.addOne(action.payload.shopItem, {
      ...state, lastCreatedShopItemId: action.payload.shopItem.id
    });
    case ShopItemActionTypes.ShopItemUpdated: return adapter.updateOne(action.payload.partialShopItem, state);
    case ShopItemActionTypes.OneShopItemDeleted: return adapter.removeOne(action.payload.id, state);
    case ShopItemActionTypes.ManyShopItemsDeleted: return adapter.removeMany(action.payload.ids, state);
    case ShopItemActionTypes.ShopItemsPageCancelled: {
      return {
        ...state, listLoading: false, lastQuery: new QueryParamsModel({})
      };
    }
    case ShopItemActionTypes.ShopItemsPageLoaded: {
      return adapter.addMany(action.payload.shopItems, {
        ...initialShopItemsState,
        totalCount: action.payload.totalCount,
        listLoading: false,
        lastQuery: action.payload.page,
        showInitWaitingMessage: false
      });
    }
    default: return state;
  }
}

export const getShopItemState = createFeatureSelector<ShopItemModel>('shopItems');

export const {
  selectAll,
  selectEntities,
  selectIds,
  selectTotal
} = adapter.getSelectors();
