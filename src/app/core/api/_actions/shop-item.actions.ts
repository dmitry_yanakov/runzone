// NGRX
import { Action } from '@ngrx/store';
import { Update } from '@ngrx/entity';
// CRUD
import { QueryParamsModel } from '../../_base/crud';
// Models
import { ShopItemModel } from '../_models/shop-item.model';

export enum ShopItemActionTypes {
  ShopItemOnServerCreated = '[Edit ShopItem Dialog] ShopItem On Server Created',
  ShopItemCreated = '[Edit ShopItem Dialog] ShopItem Created',
  ShopItemUpdated = '[Edit ShopItem Dialog] ShopItem Updated',
  ShopItemsStatusUpdated = '[ShopItem List Page] ShopItems Status Updated',
  OneShopItemDeleted = '[ShopItems List Page] One ShopItem Deleted',
  ManyShopItemsDeleted = '[ShopItems List Page] Many ShopItem Deleted',
  ShopItemsPageRequested = '[ShopItems List Page] ShopItems Page Requested',
  ShopItemsPageLoaded = '[ShopItems API] ShopItems Page Loaded',
  ShopItemsPageCancelled = '[ShopItems API] ShopItems Page Cancelled',
  ShopItemsPageToggleLoading = '[ShopItems] ShopItems Page Toggle Loading',
  ShopItemActionToggleLoading = '[ShopItems] ShopItems Action Toggle Loading',
}

export class ShopItemOnServerCreated implements Action {
  readonly type = ShopItemActionTypes.ShopItemOnServerCreated;
  constructor(public payload: { shopItem: ShopItemModel }) {}
}

export class ShopItemCreated implements Action {
  readonly type = ShopItemActionTypes.ShopItemCreated;
  constructor(public payload: { shopItem: ShopItemModel }) {}
}

export class ShopItemUpdated implements Action {
  readonly type = ShopItemActionTypes.ShopItemUpdated;
  constructor(
    public payload: {
      partialShopItem: Update<ShopItemModel>; // For State update
      shopItem: ShopItemModel; // For Server update (through services)
    }
  ) {}
}

export class ShopItemsStatusUpdated implements Action {
  readonly type = ShopItemActionTypes.ShopItemsStatusUpdated;
  constructor(
    public payload: {
      shopItems: ShopItemModel[];
      status: number;
    }
  ) {}
}

export class OneShopItemDeleted implements Action {
  readonly type = ShopItemActionTypes.OneShopItemDeleted;
  constructor(public payload: { id: number }) {}
}

export class ManyShopItemsDeleted implements Action {
  readonly type = ShopItemActionTypes.ManyShopItemsDeleted;
  constructor(public payload: { ids: number[] }) {}
}

export class ShopItemsPageRequested implements Action {
  readonly type = ShopItemActionTypes.ShopItemsPageRequested;
  constructor(public payload: { page: QueryParamsModel }) {}
}

export class ShopItemsPageLoaded implements Action {
  readonly type = ShopItemActionTypes.ShopItemsPageLoaded;
  constructor(
    public payload: {
      shopItems: ShopItemModel[];
      totalCount: number;
      page: QueryParamsModel;
    }
  ) {}
}

export class ShopItemsPageCancelled implements Action {
  readonly type = ShopItemActionTypes.ShopItemsPageCancelled;
}

export class ShopItemsPageToggleLoading implements Action {
  readonly type = ShopItemActionTypes.ShopItemsPageToggleLoading;
  constructor(public payload: { isLoading: boolean }) {}
}

export class ShopItemActionToggleLoading implements Action {
  readonly type = ShopItemActionTypes.ShopItemActionToggleLoading;
  constructor(public payload: { isLoading: boolean }) {}
}

export type ShopItemActions =
  | ShopItemOnServerCreated
  | ShopItemCreated
  | ShopItemUpdated
  | ShopItemsStatusUpdated
  | OneShopItemDeleted
  | ManyShopItemsDeleted
  | ShopItemsPageRequested
  | ShopItemsPageLoaded
  | ShopItemsPageCancelled
  | ShopItemsPageToggleLoading
  | ShopItemActionToggleLoading;
