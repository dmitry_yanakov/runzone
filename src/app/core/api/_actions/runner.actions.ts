// NGRX
import { Action } from '@ngrx/store';
import { Update } from '@ngrx/entity';
// CRUD
import { QueryParamsModel } from '../../_base/crud';
// Models
import { RunnerModel } from '../_models/runner.models';

export enum RunnerActionTypes {
  RunnerOnServerCreated = '[Edit Runner Dialog] Runner On Server Created',
  RunnerCreated = '[Edit Runner Dialog] Runner Created',
  RunnerUpdated = '[Edit Runner Dialog] Runner Updated',
  RunnersStatusUpdated = '[Runner List Page] Runners Status Updated',
  OneRunnerDeleted = '[Runners List Page] One Runner Deleted',
  ManyRunnersDeleted = '[Runners List Page] Many Runner Deleted',
  RunnersPageRequested = '[Runners List Page] Runners Page Requested',
  RunnersPageLoaded = '[Runners API] Runners Page Loaded',
  RunnersPageCancelled = '[Runners API] Runners Page Cancelled',
  RunnersPageToggleLoading = '[Runners] Runners Page Toggle Loading',
  RunnerActionToggleLoading = '[Runners] Runners Action Toggle Loading',
}

export class RunnerOnServerCreated implements Action {
  readonly type = RunnerActionTypes.RunnerOnServerCreated;
  constructor(public payload: { runner: RunnerModel }) {}
}

export class RunnerCreated implements Action {
  readonly type = RunnerActionTypes.RunnerCreated;
  constructor(public payload: { runner: RunnerModel }) {}
}

export class RunnerUpdated implements Action {
  readonly type = RunnerActionTypes.RunnerUpdated;
  constructor(
    public payload: {
      partialRunner: Update<RunnerModel>; // For State update
      runner: RunnerModel; // For Server update (through services)
    }
  ) {}
}

export class RunnersStatusUpdated implements Action {
  readonly type = RunnerActionTypes.RunnersStatusUpdated;
  constructor(
    public payload: {
      runners: RunnerModel[];
      status: number;
    }
  ) {}
}

export class OneRunnerDeleted implements Action {
  readonly type = RunnerActionTypes.OneRunnerDeleted;
  constructor(public payload: { id: number }) {}
}

export class ManyRunnersDeleted implements Action {
  readonly type = RunnerActionTypes.ManyRunnersDeleted;
  constructor(public payload: { ids: number[] }) {}
}

export class RunnersPageRequested implements Action {
  readonly type = RunnerActionTypes.RunnersPageRequested;
  constructor(public payload: { page: QueryParamsModel }) {}
}

export class RunnersPageLoaded implements Action {
  readonly type = RunnerActionTypes.RunnersPageLoaded;
  constructor(
    public payload: {
      runners: RunnerModel[];
      totalCount: number;
      page: QueryParamsModel;
    }
  ) {}
}

export class RunnersPageCancelled implements Action {
  readonly type = RunnerActionTypes.RunnersPageCancelled;
}

export class RunnersPageToggleLoading implements Action {
  readonly type = RunnerActionTypes.RunnersPageToggleLoading;
  constructor(public payload: { isLoading: boolean }) {}
}

export class RunnerActionToggleLoading implements Action {
  readonly type = RunnerActionTypes.RunnerActionToggleLoading;
  constructor(public payload: { isLoading: boolean }) {}
}

export type RunnerActions =
  | RunnerOnServerCreated
  | RunnerCreated
  | RunnerUpdated
  | RunnersStatusUpdated
  | OneRunnerDeleted
  | ManyRunnersDeleted
  | RunnersPageRequested
  | RunnersPageLoaded
  | RunnersPageCancelled
  | RunnersPageToggleLoading
  | RunnerActionToggleLoading;
