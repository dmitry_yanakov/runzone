// NGRX
import { Action } from '@ngrx/store';
import { Update } from '@ngrx/entity';
// CRUD
import { QueryParamsModel } from '../../_base/crud';
// Models
import { LevelModel } from '../_models/level.model';

export enum LevelActionTypes {
  LevelOnServerCreated = '[Edit Level Dialog] Level On Server Created',
  LevelCreated = '[Edit Level Dialog] Level Created',
  LevelUpdated = '[Edit Level Dialog] Level Updated',
  LevelsStatusUpdated = '[Level List Page] Levels Status Updated',
  OneLevelDeleted = '[Levels List Page] One Level Deleted',
  ManyLevelsDeleted = '[Levels List Page] Many Level Deleted',
  LevelsPageRequested = '[Levels List Page] Levels Page Requested',
  LevelsPageLoaded = '[Levels API] Levels Page Loaded',
  LevelsPageCancelled = '[Levels API] Levels Page Cancelled',
  LevelsPageToggleLoading = '[Levels] Levels Page Toggle Loading',
  LevelActionToggleLoading = '[Levels] Levels Action Toggle Loading',
}

export class LevelOnServerCreated implements Action {
  readonly type = LevelActionTypes.LevelOnServerCreated;
  constructor(public payload: { level: LevelModel }) {}
}

export class LevelCreated implements Action {
  readonly type = LevelActionTypes.LevelCreated;
  constructor(public payload: { level: LevelModel }) {}
}

export class LevelUpdated implements Action {
  readonly type = LevelActionTypes.LevelUpdated;
  constructor(
    public payload: {
      partialLevel: Update<LevelModel>; // For State update
      level: LevelModel; // For Server update (through services)
    }
  ) {}
}

export class LevelsStatusUpdated implements Action {
  readonly type = LevelActionTypes.LevelsStatusUpdated;
  constructor(
    public payload: {
      levels: LevelModel[];
      status: number;
    }
  ) {}
}

export class OneLevelDeleted implements Action {
  readonly type = LevelActionTypes.OneLevelDeleted;
  constructor(public payload: { id: number }) {}
}

export class ManyLevelsDeleted implements Action {
  readonly type = LevelActionTypes.ManyLevelsDeleted;
  constructor(public payload: { ids: number[] }) {}
}

export class LevelsPageRequested implements Action {
  readonly type = LevelActionTypes.LevelsPageRequested;
  constructor(public payload: { page: QueryParamsModel }) {}
}

export class LevelsPageLoaded implements Action {
  readonly type = LevelActionTypes.LevelsPageLoaded;
  constructor(
    public payload: {
      levels: LevelModel[];
      totalCount: number;
      page: QueryParamsModel;
    }
  ) {}
}

export class LevelsPageCancelled implements Action {
  readonly type = LevelActionTypes.LevelsPageCancelled;
}

export class LevelsPageToggleLoading implements Action {
  readonly type = LevelActionTypes.LevelsPageToggleLoading;
  constructor(public payload: { isLoading: boolean }) {}
}

export class LevelActionToggleLoading implements Action {
  readonly type = LevelActionTypes.LevelActionToggleLoading;
  constructor(public payload: { isLoading: boolean }) {}
}

export type LevelActions =
  | LevelOnServerCreated
  | LevelCreated
  | LevelUpdated
  | LevelsStatusUpdated
  | OneLevelDeleted
  | ManyLevelsDeleted
  | LevelsPageRequested
  | LevelsPageLoaded
  | LevelsPageCancelled
  | LevelsPageToggleLoading
  | LevelActionToggleLoading;
