// NGRX
import { Action } from '@ngrx/store';
import { Update } from '@ngrx/entity';
// CRUD
import { QueryParamsModel } from '../../_base/crud';
// Models
import { ActivitiesModel } from '../_models/activities.model';

export enum ActivityActionTypes {
  ActivityOnServerCreated = '[Edit Activity Dialog] Activity On Server Created',
  ActivityCreated = '[Edit Activity Dialog] Activity Created',
  ActivityUpdated = '[Edit Activity Dialog] Activity Updated',
  ActivitiesStatusUpdated = '[Activity List Page] Activities Status Updated',
  OneActivityDeleted = '[Activities List Page] One Activity Deleted',
  ManyActivitiesDeleted = '[Activities List Page] Many Activity Deleted',
  ActivitiesPageRequested = '[Activities List Page] Activities Page Requested',
  ActivitiesPageLoaded = '[Activities API] Activities Page Loaded',
  ActivitiesPageCancelled = '[Activities API] Activities Page Cancelled',
  ActivitiesPageToggleLoading = '[Activities] Activities Page Toggle Loading',
  ActivityActionToggleLoading = '[Activities] Activities Action Toggle Loading',
}

export class ActivityOnServerCreated implements Action {
  readonly type = ActivityActionTypes.ActivityOnServerCreated;
  constructor(public payload: { activity: ActivitiesModel }) {}
}

export class ActivityCreated implements Action {
  readonly type = ActivityActionTypes.ActivityCreated;
  constructor(public payload: { activity: ActivitiesModel }) {}
}

export class ActivityUpdated implements Action {
  readonly type = ActivityActionTypes.ActivityUpdated;
  constructor(
    public payload: {
      partialActivity: Update<ActivitiesModel>; // For State update
      activity: ActivitiesModel; // For Server update (through services)
    }
  ) {}
}

export class ActivitiesStatusUpdated implements Action {
  readonly type = ActivityActionTypes.ActivitiesStatusUpdated;
  constructor(
    public payload: {
      activities: ActivitiesModel[];
      status: number;
    }
  ) {}
}

export class OneActivityDeleted implements Action {
  readonly type = ActivityActionTypes.OneActivityDeleted;
  constructor(public payload: { id: number }) {}
}

export class ManyActivitiesDeleted implements Action {
  readonly type = ActivityActionTypes.ManyActivitiesDeleted;
  constructor(public payload: { ids: number[] }) {}
}

export class ActivitiesPageRequested implements Action {
  readonly type = ActivityActionTypes.ActivitiesPageRequested;
  constructor(public payload: { page: QueryParamsModel }) {}
}

export class ActivitiesPageLoaded implements Action {
  readonly type = ActivityActionTypes.ActivitiesPageLoaded;
  constructor(
    public payload: {
      activities: ActivitiesModel[];
      totalCount: number;
      page: QueryParamsModel;
    }
  ) {}
}

export class ActivitiesPageCancelled implements Action {
  readonly type = ActivityActionTypes.ActivitiesPageCancelled;
}

export class ActivitiesPageToggleLoading implements Action {
  readonly type = ActivityActionTypes.ActivitiesPageToggleLoading;
  constructor(public payload: { isLoading: boolean }) {}
}

export class ActivityActionToggleLoading implements Action {
  readonly type = ActivityActionTypes.ActivityActionToggleLoading;
  constructor(public payload: { isLoading: boolean }) {}
}

export type ActivityActions =
  | ActivityOnServerCreated
  | ActivityCreated
  | ActivityUpdated
  | ActivitiesStatusUpdated
  | OneActivityDeleted
  | ManyActivitiesDeleted
  | ActivitiesPageRequested
  | ActivitiesPageLoaded
  | ActivitiesPageCancelled
  | ActivitiesPageToggleLoading
  | ActivityActionToggleLoading;
