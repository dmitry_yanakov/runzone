// NGRX
import { Action } from '@ngrx/store';
import { Update } from '@ngrx/entity';
// CRUD
import { QueryParamsModel } from '../../_base/crud';
// Models
import { RankModel } from '../_models/rank.model';

export enum RankActionTypes {
  RankOnServerCreated = '[Edit Rank Dialog] Rank On Server Created',
  RankCreated = '[Edit Rank Dialog] Rank Created',
  RankUpdated = '[Edit Rank Dialog] Rank Updated',
  RanksStatusUpdated = '[Rank List Page] Ranks Status Updated',
  OneRankDeleted = '[Ranks List Page] One Rank Deleted',
  ManyRanksDeleted = '[Ranks List Page] Many Rank Deleted',
  RanksPageRequested = '[Ranks List Page] Ranks Page Requested',
  RanksPageLoaded = '[Ranks API] Ranks Page Loaded',
  RanksPageCancelled = '[Ranks API] Ranks Page Cancelled',
  RanksPageToggleLoading = '[Ranks] Ranks Page Toggle Loading',
  RankActionToggleLoading = '[Ranks] Ranks Action Toggle Loading',
}

export class RankOnServerCreated implements Action {
  readonly type = RankActionTypes.RankOnServerCreated;
  constructor(public payload: { rank: RankModel }) {}
}

export class RankCreated implements Action {
  readonly type = RankActionTypes.RankCreated;
  constructor(public payload: { rank: RankModel }) {}
}

export class RankUpdated implements Action {
  readonly type = RankActionTypes.RankUpdated;
  constructor(
    public payload: {
      partialRank: Update<RankModel>; // For State update
      rank: RankModel; // For Server update (through services)
    }
  ) {}
}

export class RanksStatusUpdated implements Action {
  readonly type = RankActionTypes.RanksStatusUpdated;
  constructor(
    public payload: {
      ranks: RankModel[];
      status: number;
    }
  ) {}
}

export class OneRankDeleted implements Action {
  readonly type = RankActionTypes.OneRankDeleted;
  constructor(public payload: { id: number }) {}
}

export class ManyRanksDeleted implements Action {
  readonly type = RankActionTypes.ManyRanksDeleted;
  constructor(public payload: { ids: number[] }) {}
}

export class RanksPageRequested implements Action {
  readonly type = RankActionTypes.RanksPageRequested;
  constructor(public payload: { page: QueryParamsModel }) {}
}

export class RanksPageLoaded implements Action {
  readonly type = RankActionTypes.RanksPageLoaded;
  constructor(
    public payload: {
      ranks: RankModel[];
      totalCount: number;
      page: QueryParamsModel;
    }
  ) {}
}

export class RanksPageCancelled implements Action {
  readonly type = RankActionTypes.RanksPageCancelled;
}

export class RanksPageToggleLoading implements Action {
  readonly type = RankActionTypes.RanksPageToggleLoading;
  constructor(public payload: { isLoading: boolean }) {}
}

export class RankActionToggleLoading implements Action {
  readonly type = RankActionTypes.RankActionToggleLoading;
  constructor(public payload: { isLoading: boolean }) {}
}

export type RankActions =
  | RankOnServerCreated
  | RankCreated
  | RankUpdated
  | RanksStatusUpdated
  | OneRankDeleted
  | ManyRanksDeleted
  | RanksPageRequested
  | RanksPageLoaded
  | RanksPageCancelled
  | RanksPageToggleLoading
  | RankActionToggleLoading;
