// NGRX
import { Action } from '@ngrx/store';
import { Update } from '@ngrx/entity';
// CRUD
import { QueryParamsModel } from '../../_base/crud';
// Models
import { ClanLevelsModel } from '../_models/clan-levels.model';

export enum ClanLevelActionTypes {
  ClanLevelOnServerCreated = '[Edit ClanLevel Dialog] ClanLevel On Server Created',
  ClanLevelCreated = '[Edit ClanLevel Dialog] ClanLevel Created',
  ClanLevelUpdated = '[Edit ClanLevel Dialog] ClanLevel Updated',
  ClanLevelsStatusUpdated = '[ClanLevel List Page] ClanLevels Status Updated',
  OneClanLevelDeleted = '[ClanLevels List Page] One ClanLevel Deleted',
  ManyClanLevelsDeleted = '[ClanLevels List Page] Many ClanLevel Deleted',
  ClanLevelsPageRequested = '[ClanLevels List Page] ClanLevels Page Requested',
  ClanLevelsPageLoaded = '[ClanLevels API] ClanLevels Page Loaded',
  ClanLevelsPageCancelled = '[ClanLevels API] ClanLevels Page Cancelled',
  ClanLevelsPageToggleLoading = '[ClanLevels] ClanLevels Page Toggle Loading',
  ClanLevelActionToggleLoading = '[ClanLevels] ClanLevels Action Toggle Loading',
}

export class ClanLevelOnServerCreated implements Action {
  readonly type = ClanLevelActionTypes.ClanLevelOnServerCreated;
  constructor(public payload: { clanLevel: ClanLevelsModel }) {}
}

export class ClanLevelCreated implements Action {
  readonly type = ClanLevelActionTypes.ClanLevelCreated;
  constructor(public payload: { clanLevel: ClanLevelsModel }) {}
}

export class ClanLevelUpdated implements Action {
  readonly type = ClanLevelActionTypes.ClanLevelUpdated;
  constructor(
    public payload: {
      partialClanLevel: Update<ClanLevelsModel>; // For State update
      clanLevel: ClanLevelsModel; // For Server update (through services)
    }
  ) {}
}

export class ClanLevelsStatusUpdated implements Action {
  readonly type = ClanLevelActionTypes.ClanLevelsStatusUpdated;
  constructor(
    public payload: {
      clanLevels: ClanLevelsModel[];
      status: number;
    }
  ) {}
}

export class OneClanLevelDeleted implements Action {
  readonly type = ClanLevelActionTypes.OneClanLevelDeleted;
  constructor(public payload: { id: number }) {}
}

export class ManyClanLevelsDeleted implements Action {
  readonly type = ClanLevelActionTypes.ManyClanLevelsDeleted;
  constructor(public payload: { ids: number[] }) {}
}

export class ClanLevelsPageRequested implements Action {
  readonly type = ClanLevelActionTypes.ClanLevelsPageRequested;
  constructor(public payload: { page: QueryParamsModel }) {}
}

export class ClanLevelsPageLoaded implements Action {
  readonly type = ClanLevelActionTypes.ClanLevelsPageLoaded;
  constructor(
    public payload: {
      clanLevels: ClanLevelsModel[];
      totalCount: number;
      page: QueryParamsModel;
    }
  ) {}
}

export class ClanLevelsPageCancelled implements Action {
  readonly type = ClanLevelActionTypes.ClanLevelsPageCancelled;
}

export class ClanLevelsPageToggleLoading implements Action {
  readonly type = ClanLevelActionTypes.ClanLevelsPageToggleLoading;
  constructor(public payload: { isLoading: boolean }) {}
}

export class ClanLevelActionToggleLoading implements Action {
  readonly type = ClanLevelActionTypes.ClanLevelActionToggleLoading;
  constructor(public payload: { isLoading: boolean }) {}
}

export type ClanLevelActions =
  | ClanLevelOnServerCreated
  | ClanLevelCreated
  | ClanLevelUpdated
  | ClanLevelsStatusUpdated
  | OneClanLevelDeleted
  | ManyClanLevelsDeleted
  | ClanLevelsPageRequested
  | ClanLevelsPageLoaded
  | ClanLevelsPageCancelled
  | ClanLevelsPageToggleLoading
  | ClanLevelActionToggleLoading;
