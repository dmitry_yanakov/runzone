// NGRX
import { Action } from '@ngrx/store';
import { Update } from '@ngrx/entity';
// CRUD
import { QueryParamsModel } from '../../_base/crud';
// Models
import { CustomizeModel } from '../_models/customize.model';

export enum CustomizeActionTypes {
  CustomizeOnServerCreated = '[Edit Customize Dialog] Customize On Server Created',
  CustomizeCreated = '[Edit Customize Dialog] Customize Created',
  CustomizeUpdated = '[Edit Customize Dialog] Customize Updated',
  CustomizesStatusUpdated = '[Customize List Page] Customizes Status Updated',
  OneCustomizeDeleted = '[Customizes List Page] One Customize Deleted',
  ManyCustomizesDeleted = '[Customizes List Page] Many Customize Deleted',
  CustomizesPageRequested = '[Customizes List Page] Customizes Page Requested',
  CustomizesPageLoaded = '[Customizes API] Customizes Page Loaded',
  CustomizesPageCancelled = '[Customizes API] Customizes Page Cancelled',
  CustomizesPageToggleLoading = '[Customizes] Customizes Page Toggle Loading',
  CustomizeActionToggleLoading = '[Customizes] Customizes Action Toggle Loading',
}

export class CustomizeOnServerCreated implements Action {
  readonly type = CustomizeActionTypes.CustomizeOnServerCreated;
  constructor(public payload: { customize: CustomizeModel }) {}
}

export class CustomizeCreated implements Action {
  readonly type = CustomizeActionTypes.CustomizeCreated;
  constructor(public payload: { customize: CustomizeModel }) {}
}

export class CustomizeUpdated implements Action {
  readonly type = CustomizeActionTypes.CustomizeUpdated;
  constructor(
    public payload: {
      partialCustomize: Update<CustomizeModel>; // For State update
      customize: CustomizeModel; // For Server update (through services)
    }
  ) {}
}

export class CustomizesStatusUpdated implements Action {
  readonly type = CustomizeActionTypes.CustomizesStatusUpdated;
  constructor(
    public payload: {
      customizes: CustomizeModel[];
      status: number;
    }
  ) {}
}

export class OneCustomizeDeleted implements Action {
  readonly type = CustomizeActionTypes.OneCustomizeDeleted;
  constructor(public payload: { id: number }) {}
}

export class ManyCustomizesDeleted implements Action {
  readonly type = CustomizeActionTypes.ManyCustomizesDeleted;
  constructor(public payload: { ids: number[] }) {}
}

export class CustomizesPageRequested implements Action {
  readonly type = CustomizeActionTypes.CustomizesPageRequested;
  constructor(public payload: { page: QueryParamsModel }) {}
}

export class CustomizesPageLoaded implements Action {
  readonly type = CustomizeActionTypes.CustomizesPageLoaded;
  constructor(
    public payload: {
      customizes: CustomizeModel[];
      totalCount: number;
      page: QueryParamsModel;
    }
  ) {}
}

export class CustomizesPageCancelled implements Action {
  readonly type = CustomizeActionTypes.CustomizesPageCancelled;
}

export class CustomizesPageToggleLoading implements Action {
  readonly type = CustomizeActionTypes.CustomizesPageToggleLoading;
  constructor(public payload: { isLoading: boolean }) {}
}

export class CustomizeActionToggleLoading implements Action {
  readonly type = CustomizeActionTypes.CustomizeActionToggleLoading;
  constructor(public payload: { isLoading: boolean }) {}
}

export type CustomizeActions =
  | CustomizeOnServerCreated
  | CustomizeCreated
  | CustomizeUpdated
  | CustomizesStatusUpdated
  | OneCustomizeDeleted
  | ManyCustomizesDeleted
  | CustomizesPageRequested
  | CustomizesPageLoaded
  | CustomizesPageCancelled
  | CustomizesPageToggleLoading
  | CustomizeActionToggleLoading;
