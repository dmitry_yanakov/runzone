// NGRX
import { Action } from '@ngrx/store';
import { Update } from '@ngrx/entity';
// CRUD
import { QueryParamsModel } from '../../_base/crud';
// Models
import { ClansModel } from '../_models/clans.module';

export enum ClanActionTypes {
  ClanOnServerCreated = '[Edit Clan Dialog] Clan On Server Created',
  ClanCreated = '[Edit Clan Dialog] Clan Created',
  ClanUpdated = '[Edit Clan Dialog] Clan Updated',
  ClansStatusUpdated = '[Clan List Page] Clans Status Updated',
  OneClanDeleted = '[Clans List Page] One clan Deleted',
  ManyClansDeleted = '[Clans List Page] Many Clan Deleted',
  ClansPageRequested = '[Clans List Page] Clans Page Requested',
  ClansPageLoaded = '[Clans API] Clans Page Loaded',
  ClansPageCancelled = '[Clans API] Clans Page Cancelled',
  ClansPageToggleLoading = '[Clans] Clans Page Toggle Loading',
  ClanActionToggleLoading = '[Clans] Clans Action Toggle Loading',
}

export class ClanOnServerCreated implements Action {
  readonly type = ClanActionTypes.ClanOnServerCreated;
  constructor(public payload: { clan: ClansModel }) {}
}

export class ClanCreated implements Action {
  readonly type = ClanActionTypes.ClanCreated;
  constructor(public payload: { clan: ClansModel }) {}
}

export class ClanUpdated implements Action {
  readonly type = ClanActionTypes.ClanUpdated;
  constructor(
    public payload: {
      partialclan: Update<ClansModel>; // For State update
      clan: ClansModel; // For Server update (through services)
    }
  ) {}
}

export class ClansStatusUpdated implements Action {
  readonly type = ClanActionTypes.ClansStatusUpdated;
  constructor(
    public payload: {
      clans: ClansModel[];
      status: number;
    }
  ) {}
}

export class OneClanDeleted implements Action {
  readonly type = ClanActionTypes.OneClanDeleted;
  constructor(public payload: { id: number }) {}
}

export class ManyClansDeleted implements Action {
  readonly type = ClanActionTypes.ManyClansDeleted;
  constructor(public payload: { ids: number[] }) {}
}

export class ClansPageRequested implements Action {
  readonly type = ClanActionTypes.ClansPageRequested;
  constructor(public payload: { page: QueryParamsModel }) {}
}

export class ClansPageLoaded implements Action {
  readonly type = ClanActionTypes.ClansPageLoaded;
  constructor(
    public payload: {
      clans: ClansModel[];
      totalCount: number;
      page: QueryParamsModel;
    }
  ) {}
}

export class ClansPageCancelled implements Action {
  readonly type = ClanActionTypes.ClansPageCancelled;
}

export class ClansPageToggleLoading implements Action {
  readonly type = ClanActionTypes.ClansPageToggleLoading;
  constructor(public payload: { isLoading: boolean }) {}
}

export class ClanActionToggleLoading implements Action {
  readonly type = ClanActionTypes.ClanActionToggleLoading;
  constructor(public payload: { isLoading: boolean }) {}
}

export type ClanActions =
  | ClanOnServerCreated
  | ClanCreated
  | ClanUpdated
  | ClansStatusUpdated
  | OneClanDeleted
  | ManyClansDeleted
  | ClansPageRequested
  | ClansPageLoaded
  | ClansPageCancelled
  | ClansPageToggleLoading
  | ClanActionToggleLoading;
