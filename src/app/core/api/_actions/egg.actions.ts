// NGRX
import { Action } from '@ngrx/store';
import { Update } from '@ngrx/entity';
// CRUD
import { QueryParamsModel } from '../../_base/crud';
// Models
import { EggModel } from '../_models/egg.models';

export enum EggActionTypes {
  EggOnServerCreated = '[Edit Egg Dialog] Egg On Server Created',
  EggCreated = '[Edit Egg Dialog] Egg Created',
  EggUpdated = '[Edit Egg Dialog] Egg Updated',
  EggsStatusUpdated = '[Egg List Page] Eggs Status Updated',
  OneEggDeleted = '[Eggs List Page] One Egg Deleted',
  ManyEggsDeleted = '[Eggs List Page] Many Egg Deleted',
  EggsPageRequested = '[Eggs List Page] Eggs Page Requested',
  EggsPageLoaded = '[Eggs API] Eggs Page Loaded',
  EggsPageCancelled = '[Eggs API] Eggs Page Cancelled',
  EggsPageToggleLoading = '[Eggs] Eggs Page Toggle Loading',
  EggActionToggleLoading = '[Eggs] Eggs Action Toggle Loading',
}

export class EggOnServerCreated implements Action {
  readonly type = EggActionTypes.EggOnServerCreated;
  constructor(public payload: { egg: EggModel }) {}
}

export class EggCreated implements Action {
  readonly type = EggActionTypes.EggCreated;
  constructor(public payload: { egg: EggModel }) {}
}

export class EggUpdated implements Action {
  readonly type = EggActionTypes.EggUpdated;
  constructor(
    public payload: {
      partialEgg: Update<EggModel>; // For State update
      egg: EggModel; // For Server update (through services)
    }
  ) {}
}

export class EggsStatusUpdated implements Action {
  readonly type = EggActionTypes.EggsStatusUpdated;
  constructor(
    public payload: {
      eggs: EggModel[];
      status: number;
    }
  ) {}
}

export class OneEggDeleted implements Action {
  readonly type = EggActionTypes.OneEggDeleted;
  constructor(public payload: { id: number }) {}
}

export class ManyEggsDeleted implements Action {
  readonly type = EggActionTypes.ManyEggsDeleted;
  constructor(public payload: { ids: number[] }) {}
}

export class EggsPageRequested implements Action {
  readonly type = EggActionTypes.EggsPageRequested;
  constructor(public payload: { page: QueryParamsModel }) {}
}

export class EggsPageLoaded implements Action {
  readonly type = EggActionTypes.EggsPageLoaded;
  constructor(
    public payload: {
      eggs: EggModel[];
      totalCount: number;
      page: QueryParamsModel;
    }
  ) {}
}

export class EggsPageCancelled implements Action {
  readonly type = EggActionTypes.EggsPageCancelled;
}

export class EggsPageToggleLoading implements Action {
  readonly type = EggActionTypes.EggsPageToggleLoading;
  constructor(public payload: { isLoading: boolean }) {}
}

export class EggActionToggleLoading implements Action {
  readonly type = EggActionTypes.EggActionToggleLoading;
  constructor(public payload: { isLoading: boolean }) {}
}

export type EggActions =
  | EggOnServerCreated
  | EggCreated
  | EggUpdated
  | EggsStatusUpdated
  | OneEggDeleted
  | ManyEggsDeleted
  | EggsPageRequested
  | EggsPageLoaded
  | EggsPageCancelled
  | EggsPageToggleLoading
  | EggActionToggleLoading;
