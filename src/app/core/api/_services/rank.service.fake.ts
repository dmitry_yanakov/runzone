// Angular
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
// RxJS
import { Observable, forkJoin, of } from 'rxjs';
import {mergeMap, delay, map , filter} from 'rxjs/operators';
// Lodash
import { each } from 'lodash';
// CRUD
import { HttpUtilsService, QueryParamsModel, QueryResultsModel } from '../../_base/crud';
// Models
import { RankModel } from '../_models/rank.model';

const API_USERS_URL = 'assets/ranks-db.json';

@Injectable()
export class RanksService {
  constructor(private http: HttpClient, private httpUtils: HttpUtilsService) { }

  // CREATE =>  POST: add a new rank to the server
  createRank(rank: RankModel): Observable<RankModel> {
    // Note: Add headers if needed (tokens/bearer)
    return ;
  }

  // READ
  getAllRanks() {
    return this.http.get<RankModel[]>(API_USERS_URL);
  }

  getRankById(Id: number): Observable<RankModel> {
    return this.http.get<RankModel[]>(API_USERS_URL).pipe(
      map(data => {
        return data.find(p => p.id === Id);
      })
    )
  }

  // Method from server should return QueryResultsModel(items: any[], totalsCount: number)
  // items => filtered/sorted result
  findRanks(queryParams: QueryParamsModel): Observable<QueryResultsModel> {
    // This code imitates server calls

    return this.http.get<RankModel[]>(API_USERS_URL).pipe(
      map(res => {
        const result = this.httpUtils.baseFilter(res, queryParams, ['status','type']);
        return result;
      })
    );
  }


  // UPDATE => PUT: update the rank on the server
  updateRank(rank: RankModel): Observable<any> {
    return;
  }

  // UPDATE Status
  updateStatusForRank(ranks: RankModel[], status: number): Observable<any> {
    return;
  }

  // DELETE => delete the rank from the server
  deleteRank(rankId: number): Observable<any> {
    return;
  }

  deleteRanks(ids: number[] = []): Observable<any> {
    return;
  }
}








