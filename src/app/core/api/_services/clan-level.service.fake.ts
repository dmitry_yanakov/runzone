// Angular
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
// RxJS
import { Observable, forkJoin, of } from 'rxjs';
import { mergeMap, delay } from 'rxjs/operators';
// Lodash
import { each } from 'lodash';
// CRUD
import { HttpUtilsService, QueryParamsModel, QueryResultsModel } from '../../_base/crud';
// Models
import { ClanLevelsModel } from '../_models/clan-levels.model';

const API_CLAN_LEVELS_URL = 'assets/clan-levels-db.json';

// Fake REST API (Mock)
// This code emulates server calls
@Injectable()
export class ClanLevelsService {
  constructor(private http: HttpClient, private httpUtils: HttpUtilsService) { }

  // CREATE =>  POST: add a new clanLevel to the server
  createClanLevel(clanLevel: ClanLevelsModel): Observable<ClanLevelsModel> {
    // Note: Add headers if needed (tokens/bearer)
    const httpHeaders = this.httpUtils.getHTTPHeaders();
    return this.http.post<ClanLevelsModel>(API_CLAN_LEVELS_URL, clanLevel, { headers: httpHeaders});
  }

  // READ
  getAllClanLevels(): Observable<ClanLevelsModel[]> {
    return this.http.get<ClanLevelsModel[]>(API_CLAN_LEVELS_URL);
  }

  getClanLevelById(clanLevelId: number): Observable<ClanLevelsModel> {
    return this.http.get<ClanLevelsModel>(API_CLAN_LEVELS_URL + `/${clanLevelId}`);
  }

  // Method from server should return QueryResultsModel(items: any[], totalsCount: number)
  // items => filtered/sorted result
  findClanLevels(queryParams: QueryParamsModel): Observable<QueryResultsModel> {
    // This code imitates server calls
    const url = API_CLAN_LEVELS_URL;
    return this.http.get<ClanLevelsModel[]>(API_CLAN_LEVELS_URL).pipe(
      mergeMap(res => {
        const result = this.httpUtils.baseFilter(res, queryParams, ['status', 'type']);
        return of(result);
      })
    );
  }

  // UPDATE => PUT: update the clanLevel on the server
  updateClanLevel(clanLevel: ClanLevelsModel): Observable<any> {
    const httpHeader = this.httpUtils.getHTTPHeaders();
    return this.http.put(API_CLAN_LEVELS_URL, clanLevel, { headers: httpHeader });
  }


  // DELETE => delete the clanLevel from the server
  deleteClanLevel(clanLevelId: number): Observable<any> {
    const url = `${API_CLAN_LEVELS_URL}/${clanLevelId}`;
    return this.http.delete<ClanLevelsModel>(url);
  }

  deleteClanLevels(ids: number[] = []): Observable<any> {
    const tasks$ = [];
    const length = ids.length;
    // tslint:disable-next-line:prefer-const
    for (let i = 0; i < length; i++) {
      tasks$.push(this.deleteClanLevel(ids[i]));
    }
    return forkJoin(tasks$);
  }
}
