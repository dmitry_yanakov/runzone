// Angular
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
// RxJS
import { Observable, forkJoin, of } from 'rxjs';
import {mergeMap, delay, map , filter} from 'rxjs/operators';
// Lodash
import { each } from 'lodash';
// CRUD
import { HttpUtilsService, QueryParamsModel, QueryResultsModel } from '../../_base/crud';
// Models
import { ClansModel } from '../_models/clans.module';

// const API_CLANS_URL = 'assets/clans-db.json';
const API_CLANS_URL = 'http://94.130.129.80:53083/api/clan';

@Injectable()
export class ClansService {
  constructor(private http: HttpClient, private httpUtils: HttpUtilsService) { }

  // CREATE =>  POST:
  createClan(clan: ClansModel): Observable<ClansModel> {

    return ;
  }

  // READ
  getAllClans() {
    return this.http.get<ClansModel[]>(API_CLANS_URL);
  }

  getClanById(Id: number): Observable<ClansModel> {
    return this.http.get<ClansModel[]>(API_CLANS_URL).pipe(
      map(data => {
        return data.find(p => p.id === Id);
      })
    )
  }

  // Method from server should return QueryResultsModel(items: any[], totalsCount: number)
  // items => filtered/sorted result
  findClan(queryParams: QueryParamsModel): Observable<QueryResultsModel> {
    // This code imitates server calls
    return this.http.get<ClansModel[]>(`${API_CLANS_URL}/getlist`).pipe(
      map(res => {
        const result = this.httpUtils.baseFilter(res, queryParams, ['country','type']);
        return result;
      })
    );
  }


  // UPDATE => PUT:
  updateClan(clan: ClansModel): Observable<any> {
    return;
  }

  // UPDATE Status
  updateStatusForClan(clans: ClansModel[], status: number): Observable<any> {
    return;
  }

  // DELETE
  deleteClan(clanId: number): Observable<any> {
    return;
  }

  deleteClans(ids: number[] = []): Observable<any> {
    return;
  }
}








