// Angular
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
// RxJS
import { Observable, forkJoin, of } from 'rxjs';
import {mergeMap, delay, map , filter} from 'rxjs/operators';
// Lodash
import { each } from 'lodash';
// CRUD
import { HttpUtilsService, QueryParamsModel, QueryResultsModel } from '../../_base/crud';
// Models
import { CustomizeModel } from '../_models/customize.model';

const API_CUSTOMIZES_URL = 'assets/customize-db.json';

@Injectable()
export class CustomizesService {
  constructor(private http: HttpClient, private httpUtils: HttpUtilsService) { }

  // CREATE =>  POST: add a new customize to the server
  createCustomize(customize: CustomizeModel): Observable<CustomizeModel> {
    // Note: Add headers if needed (tokens/bearer)
    return ;
  }

  // READ
  getAllCustomizes() {
    return this.http.get<CustomizeModel[]>(API_CUSTOMIZES_URL);
  }

  getCustomizeById(Id: number): Observable<CustomizeModel> {
    return this.http.get<CustomizeModel[]>(API_CUSTOMIZES_URL).pipe(
      map(data => {
        return data.find(p => p.id === Id);
      })
    )
  }

  // Method from server should return QueryResultsModel(items: any[], totalsCount: number)
  // items => filtered/sorted result
  findCustomizes(queryParams: QueryParamsModel): Observable<QueryResultsModel> {
    // This code imitates server calls

    return this.http.get<CustomizeModel[]>(API_CUSTOMIZES_URL).pipe(
      map(res => {
        const result = this.httpUtils.baseFilter(res, queryParams, ['status','type']);
        return result;
      })
    );
  }


  // UPDATE => PUT: update the customize on the server
  updateCustomize(customize: CustomizeModel): Observable<any> {
    return;
  }

  // UPDATE Status
  updateStatusForCustomize(customizes: CustomizeModel[], status: number): Observable<any> {
    return;
  }

  // DELETE => delete the customize from the server
  deleteCustomize(customizeId: number): Observable<any> {
    return;
  }

  deleteCustomizes(ids: number[] = []): Observable<any> {
    return;
  }
}








