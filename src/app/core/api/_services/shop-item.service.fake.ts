// Angular
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
// RxJS
import { Observable, forkJoin, of } from 'rxjs';
import { mergeMap, delay } from 'rxjs/operators';
// Lodash
import { each } from 'lodash';
// CRUD
import { HttpUtilsService, QueryParamsModel, QueryResultsModel } from '../../_base/crud';
// Models
import { ShopItemModel } from '../_models/shop-item.model';

const API_SHOPS_URL = 'assets/shop-items-db.json';

// Fake REST API (Mock)
// This code emulates server calls
@Injectable()
export class ShopItemsService {
  constructor(private http: HttpClient, private httpUtils: HttpUtilsService) { }

  // CREATE =>  POST: add a new shopItem to the server
  createShopItem(shopItem: ShopItemModel): Observable<ShopItemModel> {
    // Note: Add headers if needed (tokens/bearer)
    const httpHeaders = this.httpUtils.getHTTPHeaders();
    return this.http.post<ShopItemModel>(API_SHOPS_URL, shopItem, { headers: httpHeaders});
  }

  // READ
  getAllShopItems(): Observable<ShopItemModel[]> {
    return this.http.get<ShopItemModel[]>(API_SHOPS_URL);
  }

  getShopItemById(shopItemId: number): Observable<ShopItemModel> {
    return this.http.get<ShopItemModel>(API_SHOPS_URL + `/${shopItemId}`);
  }

  // Method from server should return QueryResultsModel(items: any[], totalsCount: number)
  // items => filtered/sorted result
  findShopItems(queryParams: QueryParamsModel): Observable<QueryResultsModel> {
    // This code imitates server calls
    const url = API_SHOPS_URL;
    return this.http.get<ShopItemModel[]>(API_SHOPS_URL).pipe(
      mergeMap(res => {
        const result = this.httpUtils.baseFilter(res, queryParams, ['status', 'type']);
        return of(result);
      })
    );
  }


  // UPDATE => PUT: update the shopItem on the server
  updateShopItem(shopItem: ShopItemModel): Observable<any> {
    const httpHeader = this.httpUtils.getHTTPHeaders();
    return this.http.put(API_SHOPS_URL, shopItem, { headers: httpHeader });
  }

  // UPDATE Status

  // DELETE => delete the shopItem from the server
  deleteShopItem(shopItemId: number): Observable<any> {
    const url = `${API_SHOPS_URL}/${shopItemId}`;
    return this.http.delete<ShopItemModel>(url);
  }

  deleteShopItems(ids: number[] = []): Observable<any> {
    const tasks$ = [];
    const length = ids.length;
    // tslint:disable-next-line:prefer-const
    for (let i = 0; i < length; i++) {
      tasks$.push(this.deleteShopItem(ids[i]));
    }
    return forkJoin(tasks$);
  }
}
