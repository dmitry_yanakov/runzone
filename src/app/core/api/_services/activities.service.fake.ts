// Angular
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
// RxJS
import { Observable, forkJoin, of } from 'rxjs';
import { mergeMap, delay, map, filter } from 'rxjs/operators';
// Lodash
import { each } from 'lodash';
// CRUD
import {
  HttpUtilsService,
  QueryParamsModel,
  QueryResultsModel,
} from '../../_base/crud';
// Models
import { ActivitiesModel } from '../_models/activities.model';
import { environment } from '../../../../environments/environment';

const API_ACTIVITIES_URL = 'http://94.130.129.80:53083/activities';
// const API_ACTIVITIES_URL = `${environment.apiUrl}/activities`;

@Injectable()
export class ActivitiesService {
  constructor(private http: HttpClient, private httpUtils: HttpUtilsService) {}

  // CREATE =>  POST:
  createActivityi(Activity: ActivitiesModel): Observable<ActivitiesModel> {
    // Note: Add headers if needed (tokens/bearer)
    return;
  }

  // READ
  getAllActivities() {
    return this.http.get(API_ACTIVITIES_URL);
  }

  getActivityById(Id: number): Observable<ActivitiesModel> {
    return this.http.get<ActivitiesModel[]>(API_ACTIVITIES_URL).pipe(
      map((data) => {
        console.log(data);
        return data.find((p) => p.id === Id);
      })
    );
  }

  // Method from server should return QueryResultsModel(items: any[], totalsCount: number)
  // items => filtered/sorted result
  findActivities(queryParams: QueryParamsModel): Observable<QueryResultsModel> {
    return this.http.post<ActivitiesModel[]>(API_ACTIVITIES_URL + '/get/list', {
      "PageSize": 1,
      "StartPosition": 2,
      "SortField": "",
      "SortDirection": 0
    }).pipe(
      map((res) => {
        const result = this.httpUtils.baseFilter(res, queryParams, [
          'status',
          'type',
        ]);
        console.log(result);
        return result;
      })
    );
  }

  // UPDATE => PUT:
  updateActivity(Activity: ActivitiesModel): Observable<any> {
    return;
  }

  // UPDATE Status
  updateStatusForActivity(
    activities: ActivitiesModel[],
    status: number
  ): Observable<any> {
    return;
  }

  // DELETE
  deleteActivity(ActivityId: number): Observable<any> {
    return;
  }

  deleteActivities(ids: number[] = []): Observable<any> {
    return;
  }
}
