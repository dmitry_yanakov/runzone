// Angular
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
// RxJS
import {forkJoin, Observable, of} from 'rxjs';
// CRUD
import { HttpUtilsService, QueryParamsModel, QueryResultsModel } from '../../_base/crud';
// Models

import {LevelModel} from "..";
import {map, mergeMap} from "rxjs/operators";
import {each} from "lodash";

const API_LEVELS_URL = 'assets/levels-db.json';

@Injectable()
export class LevelsService {
  constructor(private http: HttpClient, private httpUtils: HttpUtilsService) { }

  // CREATE =>  POST: add a new level to the server
  createLevel(level: LevelModel): Observable<LevelModel> {
    // Note: Add headers if needed (tokens/bearer)
    return ;
  }

  // READ
  getAllLevels() {
    return this.http.get<LevelModel[]>(API_LEVELS_URL);
  }

  getLevelById(Id: number): Observable<LevelModel> {
    return this.http.get<LevelModel[]>(API_LEVELS_URL).pipe(
      map(data => {
        return data.find(p => p.id === Id);
      })
    )
  }

  // Method from server should return QueryResultsModel(items: any[], totalsCount: number)
  // items => filtered/sorted result
  findLevels(queryParams: QueryParamsModel): Observable<QueryResultsModel> {
    // This code imitates server calls
    console.log(queryParams)
    return this.http.get<LevelModel[]>(API_LEVELS_URL).pipe(
      map(res => {
        const result = this.httpUtils.baseFilter(res, queryParams, ['status','type']);
        console.log(result)
        return result;
      })
    );
  }


  // UPDATE => PUT: update the level on the server
  updateLevel(level: LevelModel): Observable<any> {
    return;
  }

  // UPDATE Status
  updateStatusForLevel(levels: LevelModel[], status: number): Observable<any> {
    return;
  }

  // DELETE => delete the level from the server
  deleteLevel(levelId: number): Observable<any> {
    return;
  }

  deleteLevels(ids: number[] = []): Observable<any> {
    return;
  }
}
