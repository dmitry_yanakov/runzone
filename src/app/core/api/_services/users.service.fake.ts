// Angular
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
// RxJS
import { Observable, forkJoin, of } from 'rxjs';
import {mergeMap, delay, map , filter} from 'rxjs/operators';
// Lodash
import { each } from 'lodash';
// CRUD
import { HttpUtilsService, QueryParamsModel, QueryResultsModel } from '../../_base/crud';
// Models
import { UserModel } from '../_models/user.model';

const API_USERS_URL = 'http://94.130.129.80:53083/api/user';

@Injectable()
export class UsersService {
  constructor(private http: HttpClient, private httpUtils: HttpUtilsService) { }

  // CREATE =>  POST: add a new customer to the server
  createCustomer(user: UserModel): Observable<UserModel> {
    // Note: Add headers if needed (tokens/bearer)
   return ;
  }

  // READ
  getAllCustomers() {
    return this.http.get<UserModel[]>(API_USERS_URL);
  }

  getCustomerById(Id: number): Observable<UserModel> {
    return this.http.get<UserModel[]>(API_USERS_URL).pipe(
      map(data => {
        return data.find(p => p.id === Id);
      })
    )
  }

  // Method from server should return QueryResultsModel(items: any[], totalsCount: number)
  // items => filtered/sorted result
  findCustomers(queryParams: QueryParamsModel): Observable<QueryResultsModel> {
    // This code imitates server calls

    return this.http.post<any>(API_USERS_URL+'/getall', queryParams).pipe(
      map(res => {
        const result = this.httpUtils.baseFilter(res, queryParams, ['status','type']);
        return result;
      })
    );
  }


  // UPDATE => PUT: update the customer on the server
  updateCustomer(user: UserModel): Observable<any> {
    return;
  }

  // UPDATE Status
  updateStatusForCustomer(customers: UserModel[], status: number): Observable<any> {
   return;
  }

  // DELETE => delete the customer from the server
  deleteCustomer(customerId: number): Observable<any> {
    return;
  }

  deleteCustomers(ids: number[] = []): Observable<any> {
    return;
  }
}








