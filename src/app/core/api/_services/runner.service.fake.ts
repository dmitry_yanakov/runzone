// Angular
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
// RxJS
import { Observable, forkJoin, of } from 'rxjs';
import { mergeMap, delay } from 'rxjs/operators';
// Lodash
import { each } from 'lodash';
// CRUD
import { HttpUtilsService, QueryParamsModel, QueryResultsModel } from '../../_base/crud';
// Models
import { RunnerModel } from '../_models/runner.models';

const API_RUNNERS_URL = 'assets/runners-db.json';

// Fake REST API (Mock)
// This code emulates server calls
@Injectable()
export class RunnersService {
  constructor(private http: HttpClient, private httpUtils: HttpUtilsService) { }

  // CREATE =>  POST: add a new runner to the server
  createRunner(runner: RunnerModel): Observable<RunnerModel> {
    // Note: Add headers if needed (tokens/bearer)
    const httpHeaders = this.httpUtils.getHTTPHeaders();
    return this.http.post<RunnerModel>(API_RUNNERS_URL, runner, { headers: httpHeaders});
  }

  // READ
  getAllRunners(): Observable<RunnerModel[]> {
    return this.http.get<RunnerModel[]>(API_RUNNERS_URL);
  }

  getRunnerById(runnerId: number): Observable<RunnerModel> {
   // return this.http.get<RunnerModel>(API_RUNNERS_URL + `/${runnerId}`);
    return;
  }

  // Method from server should return QueryResultsModel(items: any[], totalsCount: number)
  // items => filtered/sorted result
  findRunners(queryParams: QueryParamsModel): Observable<QueryResultsModel> {
    // This code imitates server calls
    return this.http.get<RunnerModel[]>(API_RUNNERS_URL).pipe(
      mergeMap(res => {
        const result = this.httpUtils.baseFilter(res, queryParams, ['status', 'type']);
        return of(result);
      })
    );
  }

  // UPDATE => PUT: update the runner on the server
  updateRunner(runner: RunnerModel): Observable<any> {
    const httpHeader = this.httpUtils.getHTTPHeaders();
    return this.http.put(API_RUNNERS_URL, runner, { headers: httpHeader });
  }


  // DELETE => delete the runner from the server
  deleteRunner(runnerId: number): Observable<any> {
    const url = `${API_RUNNERS_URL}/${runnerId}`;
    return this.http.delete<RunnerModel>(url);
  }

  deleteRunners(ids: number[] = []): Observable<any> {
    const tasks$ = [];
    const length = ids.length;
    // tslint:disable-next-line:prefer-const
    for (let i = 0; i < length; i++) {
      tasks$.push(this.deleteRunner(ids[i]));
    }
    return forkJoin(tasks$);
  }
}
