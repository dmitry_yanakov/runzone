// Angular
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
// RxJS
import { Observable, forkJoin, of } from 'rxjs';
import {mergeMap, delay, map , filter} from 'rxjs/operators';
// Lodash
import { each } from 'lodash';
// CRUD
import { HttpUtilsService, QueryParamsModel, QueryResultsModel } from '../../_base/crud';
// Models
import { EggModel } from '../_models/egg.models';

const API_USERS_URL = 'assets/eggs-db.json';

@Injectable()
export class EggsService {
  constructor(private http: HttpClient, private httpUtils: HttpUtilsService) { }

  // CREATE =>  POST: add a new egg to the server
  createEgg(egg: EggModel): Observable<EggModel> {
    // Note: Add headers if needed (tokens/bearer)
    return ;
  }

  // READ
  getAllEggs() {
    return this.http.get<EggModel[]>(API_USERS_URL);
  }

  getEggById(Id: number): Observable<EggModel> {
    return this.http.get<EggModel[]>(API_USERS_URL).pipe(
      map(data => {
        return data.find(p => p.id === Id);
      })
    )
  }

  // Method from server should return QueryResultsModel(items: any[], totalsCount: number)
  // items => filtered/sorted result
  findEggs(queryParams: QueryParamsModel): Observable<QueryResultsModel> {
    // This code imitates server calls

    return this.http.get<EggModel[]>(API_USERS_URL).pipe(
      map(res => {
        const result = this.httpUtils.baseFilter(res, queryParams, ['status','type']);
        return result;
      })
    );
  }


  // UPDATE => PUT: update the egg on the server
  updateEgg(egg: EggModel): Observable<any> {
    return;
  }

  // UPDATE Status
  updateStatusForEgg(eggs: EggModel[], status: number): Observable<any> {
    return;
  }

  // DELETE => delete the egg from the server
  deleteEgg(eggId: number): Observable<any> {
    return;
  }

  deleteEggs(ids: number[] = []): Observable<any> {
    return;
  }
}








