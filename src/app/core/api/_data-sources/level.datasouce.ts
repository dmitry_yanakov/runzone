import { mergeMap, tap } from 'rxjs/operators';
// RxJS
import {
  delay,
  distinctUntilChanged,
  skip,
  filter,
  take,
  map,
} from 'rxjs/operators';
// NGRX
import { Store, select } from '@ngrx/store';
// CRUD
import { BaseDataSource, QueryResultsModel } from '../../_base/crud';
// State
import { IAppState } from '../../reducers';
import {
  selectLevelsInStore,
  selectLevelsPageLoading,
  selectLevelsShowInitWaitingMessage,
} from '../_selectors/level.selectors';

export class LevelsDataSource extends BaseDataSource {
  constructor(private store: Store<IAppState>) {
    super();

    this.loading$ = this.store.pipe(select(selectLevelsPageLoading));

    this.isPreloadTextViewed$ = this.store.pipe(
      select(selectLevelsShowInitWaitingMessage)
    );

    this.store
      .pipe(select(selectLevelsInStore))
      .subscribe((response: QueryResultsModel) => {
        this.paginatorTotalSubject.next(response.totalCount);
        this.entitySubject.next(response.items);
      });
  }
}
