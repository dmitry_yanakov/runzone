import { mergeMap, tap } from 'rxjs/operators';
// RxJS
import {
  delay,
  distinctUntilChanged,
  skip,
  filter,
  take,
  map,
} from 'rxjs/operators';
// NGRX
import { Store, select } from '@ngrx/store';
// CRUD
import { BaseDataSource, QueryResultsModel } from '../../_base/crud';
// State
import { IAppState } from '../../reducers';
import {
  selectClansInStore,
  selectClansPageLoading,
  selectClansShowInitWaitingMessage,
} from '../_selectors/clans.selectors';

export class ClansDataSource extends BaseDataSource {
  constructor(private store: Store<IAppState>) {
    super();

    this.loading$ = this.store.pipe(select(selectClansPageLoading));

    this.isPreloadTextViewed$ = this.store.pipe(
      select(selectClansShowInitWaitingMessage)
    );

    this.store
      .pipe(select(selectClansInStore))
      .subscribe((response: QueryResultsModel) => {
        this.paginatorTotalSubject.next(response.totalCount);
        this.entitySubject.next(response.items);
      });
  }
}
