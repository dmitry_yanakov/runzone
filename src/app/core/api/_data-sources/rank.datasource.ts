import { mergeMap, tap } from 'rxjs/operators';
// RxJS
import {
  delay,
  distinctUntilChanged,
  skip,
  filter,
  take,
  map,
} from 'rxjs/operators';
// NGRX
import { Store, select } from '@ngrx/store';
// CRUD
import { BaseDataSource, QueryResultsModel } from '../../_base/crud';
// State
import { IAppState } from '../../reducers';
import {
  selectRanksInStore,
  selectRanksPageLoading,
  selectRanksShowInitWaitingMessage,
} from '../_selectors/rank.selectors';

export class RanksDataSource extends BaseDataSource {
  constructor(private store: Store<IAppState>) {
    super();

    this.loading$ = this.store.pipe(select(selectRanksPageLoading));

    this.isPreloadTextViewed$ = this.store.pipe(
      select(selectRanksShowInitWaitingMessage)
    );

    this.store
      .pipe(select(selectRanksInStore))
      .subscribe((response: QueryResultsModel) => {
        this.paginatorTotalSubject.next(response.totalCount);
        this.entitySubject.next(response.items);
      });
  }
}
