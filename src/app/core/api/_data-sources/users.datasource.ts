import { mergeMap, tap } from 'rxjs/operators';
// RxJS
import {
  delay,
  distinctUntilChanged,
  skip,
  filter,
  take,
  map,
} from 'rxjs/operators';
// NGRX
import { Store, select } from '@ngrx/store';
// CRUD
import { BaseDataSource, QueryResultsModel } from '../../_base/crud';
// State
import { IAppState } from '../../reducers';
import {
  selectUsersInStore,
  selectUsersPageLoading,
  selectUsersShowInitWaitingMessage,
} from '../_selectors/users.selectors';

export class UsersDataSource extends BaseDataSource {
  constructor(private store: Store<IAppState>) {
    super();

    this.loading$ = this.store.pipe(select(selectUsersPageLoading));

    this.isPreloadTextViewed$ = this.store.pipe(
      select(selectUsersShowInitWaitingMessage)
    );

    this.store
      .pipe(select(selectUsersInStore))
      .subscribe((response: QueryResultsModel) => {
        this.paginatorTotalSubject.next(response.totalCount);
        this.entitySubject.next(response.items);
      });

    console.log(this.store);
  }
}
