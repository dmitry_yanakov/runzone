import { mergeMap, tap } from 'rxjs/operators';
// RxJS
import {
  delay,
  distinctUntilChanged,
  skip,
  filter,
  take,
  map,
} from 'rxjs/operators';
// NGRX
import { Store, select } from '@ngrx/store';
// CRUD
import { BaseDataSource, QueryResultsModel } from '../../_base/crud';
// State
import { IAppState } from '../../reducers';
import {
  selectActivitiesStateInStore,
  selectActivitiesPageLoading,
  selectActivitiesShowInitWaitingMessage,
} from '../_selectors/activities.selectors';

export class ActivitiesDataSource extends BaseDataSource {
  constructor(private store: Store<IAppState>) {
    super();

    this.loading$ = this.store.pipe(select(selectActivitiesPageLoading));

    this.isPreloadTextViewed$ = this.store.pipe(
      select(selectActivitiesShowInitWaitingMessage)
    );

    this.store
      .pipe(select(selectActivitiesStateInStore))
      .subscribe((response: QueryResultsModel) => {
        this.paginatorTotalSubject.next(response.totalCount);
        this.entitySubject.next(response.items);
      });

    console.log(this.store);
  }
}
