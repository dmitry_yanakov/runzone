import { mergeMap, tap } from 'rxjs/operators';
// RxJS
import {
  delay,
  distinctUntilChanged,
  skip,
  filter,
  take,
  map,
} from 'rxjs/operators';
// NGRX
import { Store, select } from '@ngrx/store';
// CRUD
import { BaseDataSource, QueryResultsModel } from '../../_base/crud';
// State
import { IAppState } from '../../reducers';
import {
  selectCustomizesInStore,
  selectCustomizesPageLoading,
  selectCustomizesShowInitWaitingMessage,
} from '../_selectors/customize.selectors';

export class CustomizesDataSource extends BaseDataSource {
  constructor(private store: Store<IAppState>) {
    super();

    this.loading$ = this.store.pipe(select(selectCustomizesPageLoading));

    this.isPreloadTextViewed$ = this.store.pipe(
      select(selectCustomizesShowInitWaitingMessage)
    );

    this.store
      .pipe(select(selectCustomizesInStore))
      .subscribe((response: QueryResultsModel) => {
        this.paginatorTotalSubject.next(response.totalCount);
        this.entitySubject.next(response.items);
      });
  }
}
