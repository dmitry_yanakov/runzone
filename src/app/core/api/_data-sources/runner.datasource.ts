import { mergeMap, tap } from 'rxjs/operators';
// RxJS
import {
  delay,
  distinctUntilChanged,
  skip,
  filter,
  take,
  map,
} from 'rxjs/operators';
// NGRX
import { Store, select } from '@ngrx/store';
// CRUD
import { BaseDataSource, QueryResultsModel } from '../../_base/crud';
// State
import { IAppState } from '../../reducers';
import {
  selectRunnersInStore,
  selectRunnersPageLoading,
  selectRunnersShowInitWaitingMessage,
} from '../_selectors/runner.selectors';

export class RunnersDataSource extends BaseDataSource {
  constructor(private store: Store<IAppState>) {
    super();

    this.loading$ = this.store.pipe(select(selectRunnersPageLoading));

    this.isPreloadTextViewed$ = this.store.pipe(
      select(selectRunnersShowInitWaitingMessage)
    );

    this.store
      .pipe(select(selectRunnersInStore))
      .subscribe((response: QueryResultsModel) => {
        this.paginatorTotalSubject.next(response.totalCount);
        this.entitySubject.next(response.items);
      });
  }
}
