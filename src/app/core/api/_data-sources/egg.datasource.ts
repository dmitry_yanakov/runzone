import { mergeMap, tap } from 'rxjs/operators';
// RxJS
import {
  delay,
  distinctUntilChanged,
  skip,
  filter,
  take,
  map,
} from 'rxjs/operators';
// NGRX
import { Store, select } from '@ngrx/store';
// CRUD
import { BaseDataSource, QueryResultsModel } from '../../_base/crud';
// State
import { IAppState } from '../../reducers';
import {
  selectEggsInStore,
  selectEggsPageLoading,
  selectEggsShowInitWaitingMessage,
} from '../_selectors/egg.selectors';

export class EggsDataSource extends BaseDataSource {
  constructor(private store: Store<IAppState>) {
    super();

    this.loading$ = this.store.pipe(select(selectEggsPageLoading));

    this.isPreloadTextViewed$ = this.store.pipe(
      select(selectEggsShowInitWaitingMessage)
    );

    this.store
      .pipe(select(selectEggsInStore))
      .subscribe((response: QueryResultsModel) => {
        this.paginatorTotalSubject.next(response.totalCount);
        this.entitySubject.next(response.items);
      });
  }
}
