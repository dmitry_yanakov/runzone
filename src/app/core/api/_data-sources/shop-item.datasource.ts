import { mergeMap, tap } from 'rxjs/operators';
// RxJS
import {
  delay,
  distinctUntilChanged,
  skip,
  filter,
  take,
  map,
} from 'rxjs/operators';
// NGRX
import { Store, select } from '@ngrx/store';
// CRUD
import { BaseDataSource, QueryResultsModel } from '../../_base/crud';
// State
import { IAppState } from '../../reducers';
import {
  selectShopItemsInStore,
  selectShopItemsPageLoading,
  selectShopItemsShowInitWaitingMessage,
} from '../_selectors/shop-items.selectors';

export class ShopItemsDataSource extends BaseDataSource {
  constructor(private store: Store<IAppState>) {
    super();

    this.loading$ = this.store.pipe(select(selectShopItemsPageLoading));

    this.isPreloadTextViewed$ = this.store.pipe(
      select(selectShopItemsShowInitWaitingMessage)
    );

    this.store
      .pipe(select(selectShopItemsInStore))
      .subscribe((response: QueryResultsModel) => {
        this.paginatorTotalSubject.next(response.totalCount);
        this.entitySubject.next(response.items);
      });
  }
}
