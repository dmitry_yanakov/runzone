import { mergeMap, tap } from 'rxjs/operators';
// RxJS
import {
  delay,
  distinctUntilChanged,
  skip,
  filter,
  take,
  map,
} from 'rxjs/operators';
// NGRX
import { Store, select } from '@ngrx/store';
// CRUD
import { BaseDataSource, QueryResultsModel } from '../../_base/crud';
// State
import { IAppState } from '../../reducers';
import {
  selectClanLevelsInStore,
  selectClanLevelsPageLoading,
  selectClanLevelsShowInitWaitingMessage,
} from '../_selectors/clan-level.selectors';

export class ClanLevelsDataSource extends BaseDataSource {
  constructor(private store: Store<IAppState>) {
    super();

    this.loading$ = this.store.pipe(select(selectClanLevelsPageLoading));

    this.isPreloadTextViewed$ = this.store.pipe(
      select(selectClanLevelsShowInitWaitingMessage)
    );

    this.store
      .pipe(select(selectClanLevelsInStore))
      .subscribe((response: QueryResultsModel) => {
        this.paginatorTotalSubject.next(response.totalCount);
        this.entitySubject.next(response.items);
      });
  }
}
