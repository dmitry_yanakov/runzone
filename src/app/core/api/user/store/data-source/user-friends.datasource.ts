// NGRX
import { select, Store } from '@ngrx/store';
import { mergeMap, tap } from 'rxjs/operators';
// RxJS
import {
  delay,
  distinctUntilChanged,
  filter,
  map,
  skip,
  take,
} from 'rxjs/operators';

import { BaseDataSource, QueryResultsModel } from '../../../../_base/crud';

import { IAppState } from '../../../../reducers';

import {
  selectUserFriendsInStore,
  selectUserFriendsPageLoading,
} from '../selectors/user-friends.selector';

export class UserFriendsDataSource extends BaseDataSource {
  constructor(private store: Store<IAppState>) {
    super();

    this.loading$ = this.store.pipe(select(selectUserFriendsPageLoading));

    this.store
      .pipe(select(selectUserFriendsInStore))
      .subscribe((response: QueryResultsModel) => {
        this.paginatorTotalSubject.next(response.totalCount);
        this.entitySubject.next(response.items);
      });
  }
}
