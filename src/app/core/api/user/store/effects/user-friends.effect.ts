// Angular
import { Injectable } from '@angular/core';
// NGRX
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { forkJoin, of } from 'rxjs';
// RxJS
import { map, mergeMap, tap } from 'rxjs/operators';
import { QueryParamsModel } from '../../../../_base/crud/models/query-models/query-params.model';

import { PaginationModel, QueryResultsModel } from '../../../../_base/crud';

import { IAppState } from '../../../../reducers';
import { UserService } from '../../services/user.service';
import { UserFriendModel } from '../../types';

import {
  UserFriendActionLoading,
  UserFriendActionTypes,
  UserFriendPageCancelled,
  UserFriendPageLoaded,
  UserFriendPageRequested,
  UserFriendToggleLoading,
} from '../actions/user-friends.actions';

@Injectable()
export class UserActivitiesEffect {
  showPageLoadingDistpatcher = new UserFriendActionLoading({
    isLoading: true,
  });
  showActionLoadingDistpatcher = new UserFriendActionLoading({
    isLoading: true,
  });
  hideActionLoadingDistpatcher = new UserFriendActionLoading({
    isLoading: false,
  });

  @Effect()
  loadUserFriendsList$ = this.actions$.pipe(
    ofType<UserFriendPageRequested>(
      UserFriendActionTypes.UserFriendPageRequested
    ),
    mergeMap(({ payload }) => {
      this.store.dispatch(this.showPageLoadingDistpatcher);
      const requestToServer = this.usersService.getFriendsListByUserId(payload);
      const lastQuery = of(payload.page);
      return forkJoin([requestToServer, lastQuery]);
    }),
    map((response) => {
      const result: UserFriendModel[] = response[0];
      const lastQuery: PaginationModel = response[1];
      return new UserFriendPageLoaded({
        friends: result,
        totalCount: 0,
        page: lastQuery,
      });
    })
  );

  constructor(
    private actions$: Actions,
    private usersService: UserService,
    private store: Store<IAppState>
  ) {}
}
