// Angular
import { Injectable } from '@angular/core';
// NGRX
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { forkJoin, of } from 'rxjs';
// RxJS
import { map, mergeMap, tap } from 'rxjs/operators';
import { QueryParamsModel } from '../../../../_base/crud/models/query-models/query-params.model';

import { PaginationModel, QueryResultsModel } from '../../../../_base/crud';

import { IAppState } from '../../../../reducers';
import { UserService } from '../../services/user.service';
import { CurrentUser } from '../../types';

import {
  ActivitiesByUserPageLoaded,
  DeactivateUser,
  DeleteUser,
  EditUserPageLoaded,
  FindUserByParam,
  FriendsByUserPageLoaded,
  GetUser,
  ManyDeleteUser,
  UpdateUser,
  UserActionToggleLoading,
  UserActionTypes,
  UsersPageCancelled,
  UsersPageLoaded,
  UsersPageRequested,
} from '../actions/user.actions';

@Injectable()
export class UserEffects {
  showPageLoadingDistpatcher = new UserActionToggleLoading({ isLoading: true });
  showActionLoadingDistpatcher = new UserActionToggleLoading({
    isLoading: true,
  });
  hideActionLoadingDistpatcher = new UserActionToggleLoading({
    isLoading: false,
  });

  @Effect()
  loadUsersPage$ = this.actions$.pipe(
    ofType<UsersPageRequested>(UserActionTypes.UsersPageRequested),
    mergeMap(({ payload }) => {
      this.store.dispatch(this.showPageLoadingDistpatcher);
      const requestToServer = this.usersService.getAll(payload.page);
      const lastQuery = of(payload.page);
      return forkJoin([requestToServer, lastQuery]);
    }),
    map((response) => {
      const result: CurrentUser[] = response[0];
      const lastQuery: PaginationModel = response[1];
      return new UsersPageLoaded({
        users: result,
        totalCount: result.length,
        page: lastQuery,
      });
    })
  );

  @Effect()
  deleteUser$ = this.actions$.pipe(
    ofType<DeleteUser>(UserActionTypes.DeleteUser),
    mergeMap(({ payload }) => {
      this.store.dispatch(this.showActionLoadingDistpatcher);
      return this.usersService.deleteUser(payload.userDelete);
    }),
    map(() => {
      return this.hideActionLoadingDistpatcher;
    })
  );

  @Effect()
  updateUser$ = this.actions$.pipe(
    ofType<UpdateUser>(UserActionTypes.UpdateUser),
    mergeMap(({ payload }) => {
      this.store.dispatch(this.showActionLoadingDistpatcher);
      return this.usersService.setUserInfo(payload.user);
    }),
    map(() => {
      return this.hideActionLoadingDistpatcher;
    })
  );

  constructor(
    private actions$: Actions,
    private usersService: UserService,
    private store: Store<IAppState>
  ) {}
}
