// Angular
import { Injectable } from '@angular/core';
// NGRX
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { forkJoin, of } from 'rxjs';
// RxJS
import { map, mergeMap, tap } from 'rxjs/operators';
import { QueryParamsModel } from '../../../../_base/crud/models/query-models/query-params.model';

import { PaginationModel, QueryResultsModel } from '../../../../_base/crud';

import { IAppState } from '../../../../reducers';
import { ActivityListModel } from '../../../activities/types';
import { UserService } from '../../services/user.service';

import {
  UserActivityActionLoading,
  UserActivityActionTypes,
  UserActivityPageCancelled,
  UserActivityPageLoaded,
  UserActivityPageRequested,
  UserActivityToggleLoading,
} from '../actions/user-activities.actions';

@Injectable()
export class UserActivitiesEffect {
  showPageLoadingDistpatcher = new UserActivityToggleLoading({
    isLoading: true,
  });
  showActionLoadingDistpatcher = new UserActivityToggleLoading({
    isLoading: true,
  });
  hideActionLoadingDistpatcher = new UserActivityToggleLoading({
    isLoading: false,
  });

  @Effect()
  loadUserActivitiesList$ = this.actions$.pipe(
    ofType<UserActivityPageRequested>(
      UserActivityActionTypes.UserActivityPageRequested
    ),
    mergeMap(({ payload }) => {
      this.store.dispatch(this.showPageLoadingDistpatcher);
      const requestToServer = this.usersService.getActivityListByUserId(
        payload
      );
      const lastQuery = of(payload.page);
      return forkJoin([requestToServer, lastQuery]);
    }),
    map((response) => {
      const result: ActivityListModel[] = response[0];
      const lastQuery: PaginationModel = response[1];
      return new UserActivityPageLoaded({
        activities: result,
        totalCount: 0,
        page: lastQuery,
      });
    })
  );

  constructor(
    private actions$: Actions,
    private usersService: UserService,
    private store: Store<IAppState>
  ) {}
}
