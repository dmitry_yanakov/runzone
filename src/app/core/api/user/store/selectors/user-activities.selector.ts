// NGRX
import { createFeatureSelector, createSelector } from '@ngrx/store';
// Lodash
import { each } from 'lodash';
import { ActivitiesModel } from '../../..';
// CRUD
import {
  HttpExtenstionsModel,
  QueryResultsModel,
} from '../../../../_base/crud';

import { IUserActivityState } from '../reducers';

export const selectUserActivitiesState = createFeatureSelector<
  IUserActivityState
>('user-activities');

export const selectUserActivitiesPageLoading = createSelector(
  selectUserActivitiesState,
  (usersState) => usersState.loadingList
);

export const selectUserActivitiesActionLoading = createSelector(
  selectUserActivitiesState,
  (usersFriendsState) => usersFriendsState.loading
);

export const selectUserActivitiesInStore = createSelector(
  selectUserActivitiesState,
  (userFriendsState) => {
    const items: ActivitiesModel[] = [];
    each(userFriendsState.entities, (element) => {
      items.push(element);
    });
    const httpExtension = new HttpExtenstionsModel();
    const result: ActivitiesModel[] = httpExtension.sortArray(
      items,
      userFriendsState.lastQuery.SortField,
      userFriendsState.lastQuery.SortOrder
    );
    return new QueryResultsModel(result, userFriendsState.totalCount, '');
  }
);
