// NGRX
import { createFeatureSelector, createSelector } from '@ngrx/store';
// Lodash
import { each } from 'lodash';
// CRUD
import {
  HttpExtenstionsModel,
  QueryResultsModel,
} from '../../../../_base/crud';
import { UserFriendModel } from '../../types';
import { IUserFriendState } from '../reducers';

export const selectUserFriendsState = createFeatureSelector<IUserFriendState>(
  'user-friends'
);

export const selectUserFriendsPageLoading = createSelector(
  selectUserFriendsState,
  (usersState) => usersState.loadingList
);

export const selectUserFriendsActionLoading = createSelector(
  selectUserFriendsState,
  (usersFriendsState) => usersFriendsState.loading
);

export const selectUserFriendsInStore = createSelector(
  selectUserFriendsState,
  (userFriendsState) => {
    const items: UserFriendModel[] = [];
    each(userFriendsState.entities, (element) => {
      items.push(element);
    });
    const httpExtension = new HttpExtenstionsModel();
    const result: UserFriendModel[] = httpExtension.sortArray(
      items,
      userFriendsState.lastQuery.SortField,
      userFriendsState.lastQuery.SortOrder
    );
    return new QueryResultsModel(result, userFriendsState.totalCount, '');
  }
);
