// NGRX
import { createFeatureSelector, createSelector } from '@ngrx/store';
// Lodash
import { each } from 'lodash';
// CRUD
import {
  HttpExtenstionsModel,
  QueryResultsModel,
} from '../../../../_base/crud';

import { CurrentUser } from '../../types';
import { IUserState } from '../reducers';

export const selectUserState = createFeatureSelector<IUserState>('users');

export const selectUserById = (userId: number) =>
  createSelector(selectUserState, (usersState) => usersState.entities[userId]);

export const selectUsersPageLoading = createSelector(
  selectUserState,
  (usersState) => usersState.loadingList
);

export const selectUsersActionLoading = createSelector(
  selectUserState,
  (usersState) => usersState.loading
);

export const selectUsersInStore = createSelector(
  selectUserState,
  (usersState) => {
    const items: CurrentUser[] = [];
    each(usersState.entities, (element) => {
      items.push(element);
    });
    const httpExtension = new HttpExtenstionsModel();
    const result: CurrentUser[] = httpExtension.sortArray(items);
    return new QueryResultsModel(result, usersState.totalCount, '');
  }
);
