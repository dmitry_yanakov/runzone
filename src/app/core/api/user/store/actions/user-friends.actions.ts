// NGRX
import { Update } from '@ngrx/entity';
import { Action } from '@ngrx/store';
// CRUD
import { PaginationModel } from '../../../../_base/crud';

import { UserFriendModel } from '../../types';

export enum UserFriendActionTypes {
  UserFriendPageRequested = '[Edit User Friend List Page] Friends Page Loaded',
  UserFriendPageLoaded = '[User API] User Friends List Loaded',
  UserFriendToggleLoading = '[Edit User] User Friends Page Toggle Loading',
  UserFriendActionLoading = '[Edit User] User Friends Action Toggle Loading',
  UserFriendPageCancelled = '[Edit User] User Friends Page Cancelled',
}

export class UserFriendPageRequested implements Action {
  readonly type = UserFriendActionTypes.UserFriendPageRequested;
  constructor(public payload: { page: PaginationModel; userId: number }) {}
}

export class UserFriendPageLoaded implements Action {
  readonly type = UserFriendActionTypes.UserFriendPageLoaded;
  constructor(
    public payload: {
      friends: UserFriendModel[];
      totalCount: number;
      page: PaginationModel;
    }
  ) {}
}

export class UserFriendToggleLoading implements Action {
  readonly type = UserFriendActionTypes.UserFriendToggleLoading;
  constructor(public payload: { isLoading: boolean }) {}
}

export class UserFriendActionLoading implements Action {
  readonly type = UserFriendActionTypes.UserFriendActionLoading;
  constructor(public payload: { isLoading: boolean }) {}
}

export class UserFriendPageCancelled implements Action {
  readonly type = UserFriendActionTypes.UserFriendPageCancelled;
}

export type UserFriendActions =
  | UserFriendPageRequested
  | UserFriendPageLoaded
  | UserFriendToggleLoading
  | UserFriendActionLoading
  | UserFriendPageCancelled;
