import { Update } from '@ngrx/entity';
import { Action } from '@ngrx/store';
import { PaginationModel } from '../../../../_base/crud';
import {
  CurrentUser,
  UserDeleteModel,
  UserFindModel,
  UserInfoModel,
} from '../../types';

export enum UserActionTypes {
  UsersPageLoaded = '[Get List Users] Action',
  UsersPageRequested = '[Get List Users] Users Page Requested',
  UsersPageCancelled = '[Users API] Users Page Cancelled',
  GetUser = '[Get User] Action',
  EditUserPageLoaded = '[Get User Info] Action',
  DeleteUser = '[Delete User] Action',
  ManyDeleteUser = '[Many Delete User] Action',
  DeactivateUser = '[Deactivate User] Action',
  FriendsByUserPageLoaded = '[Get Friends By User] Action',
  ActivitiesByUserPageLoaded = '[Get Activities By User] Action',
  FindUserByParam = '[Find User By Param] Action',
  UserActionToggleLoading = '[User Action Toggle Loading]',
  UpdateUser = '[Update User] Action',
}

export class UsersPageLoaded implements Action {
  readonly type = UserActionTypes.UsersPageLoaded;
  constructor(
    public payload: {
      users: CurrentUser[];
      totalCount: number;
      page: PaginationModel;
    }
  ) {}
}

export class UsersPageRequested implements Action {
  readonly type = UserActionTypes.UsersPageRequested;
  constructor(public payload: { page: PaginationModel }) {}
}

export class UsersPageCancelled implements Action {
  readonly type = UserActionTypes.UsersPageCancelled;
}

export class GetUser implements Action {
  readonly type = UserActionTypes.GetUser;
  constructor(public payload: { id: number }) {}
}

export class EditUserPageLoaded implements Action {
  readonly type = UserActionTypes.EditUserPageLoaded;
  constructor(public payload: { id: number; isLoading: boolean }) {}
}

export class DeleteUser implements Action {
  readonly type = UserActionTypes.DeleteUser;
  constructor(public payload: { userDelete: UserDeleteModel }) {}
}

export class ManyDeleteUser implements Action {
  readonly type = UserActionTypes.ManyDeleteUser;
  constructor(public payload: { ids: number[] }) {}
}

export class DeactivateUser implements Action {
  readonly type = UserActionTypes.DeactivateUser;
  constructor(public payload: { id: number }) {}
}

export class FriendsByUserPageLoaded implements Action {
  readonly type = UserActionTypes.FriendsByUserPageLoaded;
  constructor(public payload: { id: number }) {}
}

export class ActivitiesByUserPageLoaded implements Action {
  readonly type = UserActionTypes.ActivitiesByUserPageLoaded;
  constructor(public payload: { id: number }) {}
}

export class FindUserByParam implements Action {
  readonly type = UserActionTypes.FindUserByParam;
  constructor(public payload: UserFindModel) {}
}

export class UpdateUser implements Action {
  readonly type = UserActionTypes.UpdateUser;
  constructor(
    public payload: {
      particalUser: Update<CurrentUser>;
      user: UserInfoModel;
    }
  ) {}
}

export class UserActionToggleLoading implements Action {
  readonly type = UserActionTypes.UserActionToggleLoading;
  constructor(public payload: { isLoading: boolean }) {}
}

export type UserActions =
  | UsersPageLoaded
  | GetUser
  | EditUserPageLoaded
  | DeleteUser
  | ManyDeleteUser
  | DeactivateUser
  | FriendsByUserPageLoaded
  | ActivitiesByUserPageLoaded
  | FindUserByParam
  | UserActionToggleLoading
  | UpdateUser
  | UsersPageRequested
  | UsersPageCancelled;
