// NGRX
import { Update } from '@ngrx/entity';
import { Action } from '@ngrx/store';
// CRUD
import { PaginationModel } from '../../../../_base/crud';
import { ActivityListModel } from '../../../activities/types';

export enum UserActivityActionTypes {
  UserActivityPageRequested = '[Edit User Activity List Page] Activities Page Loaded',
  UserActivityPageLoaded = '[User API] User Activities List Loaded',
  UserActivityToggleLoading = '[Edit User] User Activities Page Toggle Loading',
  UserActivityActionLoading = '[Edit User] User Activities Action Toggle Loading',
  UserActivityPageCancelled = '[Edit User] User Activities Page Cancelled',
}

export class UserActivityPageRequested implements Action {
  readonly type = UserActivityActionTypes.UserActivityPageRequested;
  constructor(public payload: { page: PaginationModel; userId: number }) {}
}

export class UserActivityPageLoaded implements Action {
  readonly type = UserActivityActionTypes.UserActivityPageLoaded;
  constructor(
    public payload: {
      activities: ActivityListModel[];
      page: PaginationModel;
      totalCount: number;
    }
  ) {}
}

export class UserActivityToggleLoading implements Action {
  readonly type = UserActivityActionTypes.UserActivityToggleLoading;
  constructor(public payload: { isLoading: boolean }) {}
}

export class UserActivityActionLoading implements Action {
  readonly type = UserActivityActionTypes.UserActivityActionLoading;
  constructor(public payload: { isLoading: boolean }) {}
}

export class UserActivityPageCancelled implements Action {
  readonly type = UserActivityActionTypes.UserActivityPageCancelled;
}

export type UserActivityActions =
  | UserActivityPageRequested
  | UserActivityPageLoaded
  | UserActivityToggleLoading
  | UserActivityActionLoading
  | UserActivityPageCancelled;
