import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { createFeatureSelector } from '@ngrx/store';
import { ActivitiesModel } from '../../..';
import { PaginationModel } from '../../../../_base/crud';
import {
  UserActivityActions,
  UserActivityActionTypes,
} from '../actions/user-activities.actions';

export interface IUserActivityState extends EntityState<ActivitiesModel> {
  loading: boolean;
  loadingList: boolean;
  lastQuery: PaginationModel;
  totalCount: number;
}

export const adapter: EntityAdapter<ActivitiesModel> = createEntityAdapter<
  ActivitiesModel
>();

export const initialUserActivityState: IUserActivityState = adapter.getInitialState(
  {
    loading: false,
    loadingList: false,
    lastQuery: new PaginationModel(),
    totalCount: 0,
  }
);

export function userActivityReducer(
  state = initialUserActivityState,
  action: UserActivityActions
): IUserActivityState {
  switch (action.type) {
    case UserActivityActionTypes.UserActivityActionLoading: {
      return {
        ...state,
      };
    }
    case UserActivityActionTypes.UserActivityPageCancelled: {
      return {
        ...state,
        loadingList: false,
        loading: false,
      };
    }
    case UserActivityActionTypes.UserActivityPageLoaded: {
      return {
        ...state,
        lastQuery: action.payload.page,
        totalCount: action.payload.totalCount,
        loadingList: false,
      };
    }
    case UserActivityActionTypes.UserActivityPageRequested: {
      return {
        ...state,
        lastQuery: action.payload.page,
        loadingList: true,
      };
    }
    case UserActivityActionTypes.UserActivityToggleLoading: {
      return {
        ...state,
        loading: action.payload.isLoading,
      };
    }
  }
}

export const getUserActivitiesState = createFeatureSelector<ActivitiesModel>(
  'user-activities'
);

export const {
  selectAll,
  selectEntities,
  selectIds,
  selectTotal,
} = adapter.getSelectors();
