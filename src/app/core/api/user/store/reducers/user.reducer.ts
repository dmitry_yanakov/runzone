import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { createFeatureSelector } from '@ngrx/store';
import { PaginationModel } from '../../../../_base/crud';
import { CurrentUser } from '../../types';
import { UserActions, UserActionTypes } from '../actions/user.actions';

export interface IUserState extends EntityState<CurrentUser> {
  loading: boolean;
  loadingList: boolean;
  userId: number;
  lastQuery: PaginationModel;
  totalCount: number;
}

export const adapter: EntityAdapter<CurrentUser> = createEntityAdapter<
  CurrentUser
>();

export const initialUserState: IUserState = adapter.getInitialState({
  userId: undefined,
  loadingList: false,
  loading: false,
  lastQuery: new PaginationModel(),
  totalCount: 0,
});

export function userReducer(
  state = initialUserState,
  action: UserActions
): IUserState {
  switch (action.type) {
    case UserActionTypes.UsersPageLoaded: {
      return {
        ...initialUserState,
        loadingList: false,
        lastQuery: action.payload.page,
        totalCount: action.payload.totalCount,
      };
    }
    case UserActionTypes.UsersPageRequested: {
      return {
        ...state,
        lastQuery: action.payload.page,
        loadingList: true,
      };
    }
    case UserActionTypes.UsersPageCancelled: {
      return {
        ...state,
        loadingList: false,
        lastQuery: new PaginationModel(),
      };
    }
    case UserActionTypes.EditUserPageLoaded: {
      return {
        ...state,
        loading: action.payload.isLoading,
        userId: action.payload.id,
      };
    }
    case UserActionTypes.DeleteUser:
      return adapter.removeOne(action.payload.userDelete.id, state);
    case UserActionTypes.ManyDeleteUser:
      return adapter.removeMany(action.payload.ids, state);
    case UserActionTypes.DeactivateUser: {
      return {
        ...state,
      };
    }
    case UserActionTypes.UpdateUser: {
      return adapter.updateOne(action.payload.particalUser, state);
    }
    default:
      return state;
  }
}

export const getUserState = createFeatureSelector<CurrentUser>('users');

export const {
  selectAll,
  selectEntities,
  selectIds,
  selectTotal,
} = adapter.getSelectors();
