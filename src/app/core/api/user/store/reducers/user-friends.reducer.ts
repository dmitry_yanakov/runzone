import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { createFeatureSelector } from '@ngrx/store';
import { PaginationModel } from '../../../../_base/crud';
import { UserFriendModel } from '../../types';
import {
  UserFriendActions,
  UserFriendActionTypes,
} from '../actions/user-friends.actions';

export interface IUserFriendState extends EntityState<UserFriendModel> {
  loading: boolean;
  loadingList: boolean;
  lastQuery: PaginationModel;
  totalCount: number;
}

export const adapter: EntityAdapter<UserFriendModel> = createEntityAdapter<
  UserFriendModel
>();

export const initialUserFriendState: IUserFriendState = adapter.getInitialState(
  {
    loading: false,
    loadingList: false,
    lastQuery: new PaginationModel(),
    totalCount: 0,
  }
);

export function userFriendReducer(
  state = initialUserFriendState,
  action: UserFriendActions
): IUserFriendState {
  switch (action.type) {
    case UserFriendActionTypes.UserFriendActionLoading: {
      return {
        ...state,
      };
    }
    case UserFriendActionTypes.UserFriendPageCancelled: {
      return {
        ...state,
        loadingList: false,
        loading: false,
      };
    }
    case UserFriendActionTypes.UserFriendPageLoaded: {
      return {
        ...state,
        totalCount: action.payload.totalCount,
        lastQuery: action.payload.page,
        loadingList: false,
      };
    }
    case UserFriendActionTypes.UserFriendPageRequested: {
      return {
        ...state,
        lastQuery: action.payload.page,
        loadingList: true,
      };
    }
    case UserFriendActionTypes.UserFriendToggleLoading: {
      return {
        ...state,
        loading: action.payload.isLoading,
      };
    }
  }
}

export const getUserFriendsState = createFeatureSelector<UserFriendModel>(
  'user-friends'
);

export const {
  selectAll,
  selectEntities,
  selectIds,
  selectTotal,
} = adapter.getSelectors();
