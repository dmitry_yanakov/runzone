import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { environment } from '../../../../../environments/environment';
import { QueryParamsModel, QueryResultsModel } from '../../../_base/crud';
import { PaginationModel } from '../../../_shared/types';
import { ActivityListModel } from '../../activities/types';

import {
  CurrentUser,
  UserFindModel,
  UserFriendModel,
  UserInfoModel,
} from '../types';

const API_USERS_URL = `${environment.apiUrl}/user`;

@Injectable()
export class UserService {
  constructor(private http: HttpClient) {}

  getAll(pagination: PaginationModel): Observable<CurrentUser[]> {
    return this.http.post<CurrentUser[]>(
      // `${API_USERS_URL}/getall/${pagination.StartPosition}`,
      `${API_USERS_URL}/getall`,
      pagination
    );
  }

  getUserById(id: number): Observable<CurrentUser> {
    return this.http.get<CurrentUser>(`${API_USERS_URL}/get/${id}`);
  }

  getInfoUserById(id: number): Observable<UserInfoModel> {
    return this.http.get<UserInfoModel>(`${API_USERS_URL}/get/info//${id}`);
  }

  deleteUser({ email, id }): Observable<any> {
    return this.http.delete(`${API_USERS_URL}/delete?id=${id}&email=${email}`);
  }

  deactiveUser(id: number) {
    this.http.post(`${API_USERS_URL}/deactive?id=${id}`, {});
  }

  getFriendsListByUserId({ userId }): Observable<UserFriendModel[]> {
    return this.http.get<UserFriendModel[]>(
      `${API_USERS_URL}/friends/list/${userId}`
    );
  }

  getActivityListByUserId({ userId }): Observable<ActivityListModel[]> {
    return this.http.get<ActivityListModel[]>(
      `${API_USERS_URL}/activity/list/${userId}`
    );
  }

  setUserInfo(user: UserInfoModel): Observable<UserInfoModel> {
    return this.http.post<UserInfoModel>(`${API_USERS_URL}/set/info`, user);
  }

  findUser(
    queryFind: UserFindModel,
    pagination: PaginationModel
  ): Observable<CurrentUser[]> {
    return this.http.post<CurrentUser[]>(
      `${API_USERS_URL}/search?
    word={word}
    &type={type}
    &countryId={countryId}
    &clanId={clanId}`,
      pagination
    );
  }
}
