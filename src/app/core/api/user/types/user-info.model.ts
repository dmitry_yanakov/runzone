import { BaseModel } from '../../../_base/crud';
import { EmailNotificationSetup } from '../../../_shared/types';
import { SocialNet } from '../../auth/types/social-net.enum';
import { ClanModel } from '../../clan/types';
import { CountryModel } from '../../country/types';
import { TimeZoneInfoModel } from './time-zone-info.model';
import { UserAchievementListModel } from './user-achievement-list.model';
import { UserMetaDataModel } from './user-meta-data.model';

export class UserInfoModel extends BaseModel {
  UserId;
  MetricSystem: number;
  Clan: ClanModel;
  ProviderRegistration: SocialNet;
  ProviderUserId: string;
  Avatar: UserMetaDataModel;
  TimeZone: TimeZoneInfoModel;
  Gender: boolean;
  Google: string;
  FacebookId: string;
  StravaId: string;
  Weight: number;
  Height: number;
  Country: CountryModel;
  BirthDay: string;
  Xp: number;
  TotalXp: number;
  Level: number;
  LastAuth: string;
  UserAchievements: UserAchievementListModel;
  EmailNotification: EmailNotificationSetup;
  PushNotification: EmailNotificationSetup;
  BlockMessage: string;

  clear() {
    this.UserId = 0;
    this.MetricSystem = 0;
    this.Clan = null;
    this.ProviderRegistration = null;
    this.ProviderUserId = '';
    this.Avatar = null;
    this.TimeZone = null;
    this.Gender = false;
    this.Google = '';
    this.FacebookId = '';
    this.StravaId = '';
    this.Weight = 0;
    this.Height = 0;
    this.Country = null;
    this.BirthDay = '';
    this.Xp = 0;
    this.TotalXp = 0;
    this.Level = 0;
    this.LastAuth = '';
    this.UserAchievements.clear();
    this.EmailNotification = null;
    this.PushNotification = null;
    this.BlockMessage = '';
  }
}
