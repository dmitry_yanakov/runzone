import { BaseModel } from '../../../_base/crud';
import { FileType } from '../../../_shared/types';

export class UserMetaDataModel extends BaseModel {
  fileType: FileType;
  fileName: string;
  id: string;
  ord: number;
  text: string;
  width: number;
  heigth: number;
}
