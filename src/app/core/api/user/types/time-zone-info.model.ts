import { BaseModel } from '../../../_base/crud';

export class TimeZoneInfoModel extends BaseModel {
  id: string;
  displayName: string;
  standardName: string;
  daylightName: string;
  baseUtcOffset: string;
  supportDaylightSavingTime: boolean;

  clear() {
    this.id = '';
    this.displayName = '';
    this.standardName = '';
    this.daylightName = '';
    this.baseUtcOffset = '';
    this.supportDaylightSavingTime = false;
  }
}
