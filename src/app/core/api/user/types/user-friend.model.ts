import { BaseModel } from '../../../_base/crud';
import { FrendshipType } from '../../../_shared/types';

export class UserFriendModel extends BaseModel {
  id: string;
  country: string;
  username: string;
  clan: string;
  trophies: string;
  level: number;
  xp: number;
  friendshipDate: string;
  type: FrendshipType;
  status: number;
}
