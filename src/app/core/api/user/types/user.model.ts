import { BaseModel } from '../../../_base/crud';

export class CurrentUser extends BaseModel {
  id: string;
  number: string;
  email: string;
  Username: string;
  Clan: string;
  level: number;
  Trophies: string;
  XP: number;
  Status: number;
}
