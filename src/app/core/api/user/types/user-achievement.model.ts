import { BaseModel } from '../../../_base/crud';

export class UserAchievementModel extends BaseModel {
  id: number;
  name: string;
  value: string;
}
