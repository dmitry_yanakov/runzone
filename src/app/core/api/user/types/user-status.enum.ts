export enum UserStatus {
  admin,
  blocked,
  verify,
  active,
}
