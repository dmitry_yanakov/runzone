export class UserListModel {
  id: string;
  number: string;
  email: string;
  Username: string;
  Clan: string;
  level: number;
  Trophies: string;
  XP: number;
  Status: number;
}
