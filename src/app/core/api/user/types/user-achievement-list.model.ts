import { BaseModel } from '../../../_base/crud';
import { UserAchievementModel } from './user-achievement.model';

export class UserAchievementListModel extends BaseModel {
  list: UserAchievementModel[];
  totalCount: number;

  clear() {
    this.list = null;
    this.totalCount = 0;
  }
}
