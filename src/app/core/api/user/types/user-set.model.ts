import { UserStatus } from './user-status.enum';

export class UserSetModel {
  description: string;
  lastName: string;
  secondName: string;
  firstName: string;
  phone: string;
  email: string;
  id: string;
  status: UserStatus;
}
