export class UserFindModel {
  word: string;
  type: number;
  countryId: number;
  clanId: string;
}
