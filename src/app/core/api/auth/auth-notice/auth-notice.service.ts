import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { IAuthNotice } from '../types/auth-notice.intefrace';

@Injectable({
  providedIn: 'root',
})
export class AuthNoticeService {
  onNoticeChanged$: BehaviorSubject<IAuthNotice>;

  constructor() {
    this.onNoticeChanged$ = new BehaviorSubject(null);
  }

  setNotice(message: string, type?: string) {
    const notice: IAuthNotice = {
      message,
      type,
    };
    this.onNoticeChanged$.next(notice);
  }
}
