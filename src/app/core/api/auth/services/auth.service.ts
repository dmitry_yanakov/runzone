import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { environment } from '../../../../../environments/environment';
import { QueryParamsModel, QueryResultsModel } from '../../../_base/crud';
import { CurrentUser } from '../types';
import { Permission } from '../types';
import { Role } from '../types';

const API_USERS_URL = `${environment.apiUrl}/user`;
const API_PERMISSION_URL = 'api/permissions';
const API_ROLES_URL = 'api/roles';

@Injectable()
export class AuthService {
  constructor(private http: HttpClient) {}

  // simple login
  login(email: string, password: string): Observable<CurrentUser> {
    return this.http.post<CurrentUser>(`${API_USERS_URL}/login`, { email, password }).pipe(map(response => {
      if (response['Token'] !== null) {
        localStorage.setItem('token', JSON.stringify(response['Token']));
      }
      return response;
    })
  );
  }

  // simple registration
  register(user): Observable<any> {
    return this.http.post(`${API_USERS_URL}/registration`, user);
  }

  getUserByToken(): Observable<CurrentUser> {
    const userToken = localStorage.getItem('token');
    const httpHeaders = new HttpHeaders().set('session', userToken.replace(/['"]+/g, ''));
    return this.http.post<CurrentUser>(`${API_USERS_URL}/get`, {}, { headers: httpHeaders });
  }


  registerSet(user: CurrentUser): Observable<any> {
    const httpHeaders = new HttpHeaders();
    httpHeaders.set('Content-Type', 'application/json');
    return this.http
      .post<CurrentUser>(`${API_USERS_URL}/set`, user, { headers: httpHeaders })
      .pipe(
        map((res: CurrentUser) => {
          return res;
        }),
        catchError((err) => {
          return null;
        })
      );
  }

  loginSocial(email: string, password: string): Observable<CurrentUser> {
    this.http
      .post<CurrentUser>(`${API_USERS_URL}/loginsocialnet`, { email, password })
      .subscribe((user) => {
        console.log(user);
      });
    return this.http.post<CurrentUser>(`${API_USERS_URL}/loginsocialnet`, {
      email,
      password,
    });
  }

  setStravaCode(code: string) {}

  confirmEmail(token: string): Observable<CurrentUser> {
    return this.http.post<CurrentUser>(`${API_USERS_URL}/login`, token);
  }

  passwordSet(password: string, userId: string): Observable<boolean> {
    return this.http.post<boolean>(
      `${API_USERS_URL}/passwordset?password={${password}}`,
      userId
    );
  }

  /*
   * Handle Http operation that failed.
   * Let the app continue.
   *
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = 'operation', result?: any) {
    return (error: any): Observable<any> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // Let the app keep running by returning an empty result.
      return of(result);
    };
  }
}
