import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { environment } from '../../../../../environments/environment';
import { CurrentUser, InfoStravaApiModel } from '../types';

const API_USERS_URL = `${environment.apiUrl}/user`;

@Injectable()
export class SocialAuthService {
  constructor(private http: HttpClient) {}

  setStravaCode(code: string): Observable<InfoStravaApiModel> {
    return this.http.get<InfoStravaApiModel>(
      `${API_USERS_URL}/strarvakeys/${code}`
    );
  }

  stravaLogin(stravaKeys: InfoStravaApiModel): Observable<string> {
    return this.http
      .post(
        `https://www.strava.com/oauth/token?client_id=${stravaKeys.ClientId}&client_secret=${stravaKeys.ClientSecret}&code=${stravaKeys.Code}&grant_type=authorization_code`,
        {}
      )
      .pipe(
        map(({ access_token }: any) => {
          return access_token;
        })
      );
  }

  getUserStrava(accessToken: string): Observable<CurrentUser> {
    return this.http.post<CurrentUser>(
      `${API_USERS_URL}/strarvacallbackauth?accessToken=${accessToken}`,
      {}
    );
  }
}
