import { SocialAuthService } from './auth-social.service';
import { AuthService } from './auth.service';

export { AuthService, SocialAuthService };
