// Angular
import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
} from '@angular/router';
// NGRX
import { select, Store } from '@ngrx/store';
// RxJS
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
// Auth reducers and selectors
import { IAppState } from '../../../reducers';
import { AuthSelectors } from '../store/selectors';

// @Injectable()
// export class AuthGuard implements CanActivate {
//   constructor(private store: Store<IAppState>, private router: Router) {}

//   canActivate(
//     route: ActivatedRouteSnapshot,
//     state: RouterStateSnapshot
//   ): Observable<any> {
//     return this.store.pipe(
//       select(AuthSelectors.isLoggedIn),
//       tap((loggedIn) => {
//         if (!loggedIn) {
//           this.router.navigateByUrl('/auth/login');
//         }
//       })
//     );
//   }
@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {
    constructor(
        private router: Router
    ) {}

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const currentUser = localStorage.getItem('token');
        if (currentUser && currentUser !== null) {
            // authorised so return true
            return true;
        }

        // not logged in so redirect to login page with the return url
        this.router.navigate(['login']);
        return false;
  }
}
