import { CurrentUser, IAuthState } from '../../types';
import { AuthActions, AuthActionTypes } from '../actions';

export const initialAuthState: IAuthState = {
  loggedIn: false,
  authToken: undefined,
  user: undefined,
  isUserLoaded: false,
};

export function authReducer(
  state = initialAuthState,
  action: AuthActions
): IAuthState {
  switch (action.type) {
    case AuthActionTypes.Login: {
      const _token: string = action.payload.authToken;
      return {
        loggedIn: true,
        authToken: _token,
        user: undefined,
        isUserLoaded: false,
      };
    }

    case AuthActionTypes.Register: {
      const _token: string = action.payload.authToken;
      return {
        loggedIn: true,
        authToken: _token,
        user: undefined,
        isUserLoaded: false,
      };
    }

    case AuthActionTypes.Logout:
      return initialAuthState;

    case AuthActionTypes.UserLoaded: {
      const _user: CurrentUser = action.payload.user;
      return {
        ...state,
        user: _user,
        isUserLoaded: true,
      };
    }

    // Social Logins

    case AuthActionTypes.LoginStrava: {
      return {
        loggedIn: true,
        authToken: undefined,
        user: undefined,
        isUserLoaded: false,
      };
    }

    default:
      return state;
  }
}
