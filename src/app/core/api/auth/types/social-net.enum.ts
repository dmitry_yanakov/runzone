export enum SocialNet {
  Local,
  Google,
  Facebook,
  Strava,
}
