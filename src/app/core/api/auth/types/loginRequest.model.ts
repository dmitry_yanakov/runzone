import { BaseModel } from '../../../_base/crud';

export class LoginRequestModel extends BaseModel {
  email: string;
  password: string;

  clear(): void {
    this.email = '';
    this.password = '';
  }
}
