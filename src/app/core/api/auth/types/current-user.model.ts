import { BaseModel } from '../../../_base/crud';

export class CurrentUser extends BaseModel {
  id: string;
  number: string;
  email: string;
  Username: string;
  Clan: string;
  level: number;
  Trophies: string;
  XP: number;
  Status: number;

  clear(): void {
    this.id = undefined;
    this.number = '';
    this.email = '';
    this.Username = '';
    this.Clan = '';
    this.level = undefined;
    this.Trophies = '';
    this.XP = undefined;
    this.Status = undefined;
  }
}
