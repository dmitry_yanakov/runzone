import { CurrentUser } from '../../user/types/user.model';

export interface IAuthState {
  loggedIn: boolean;
  authToken: string;
  user: CurrentUser;
  isUserLoaded: boolean;
}
