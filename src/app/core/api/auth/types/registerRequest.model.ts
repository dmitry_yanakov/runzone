import { BaseModel } from '../../../_base/crud';

export class RegisterRequestModel extends BaseModel {
  email: string;
  username: string;
  clan: string;
  createdDate: string;
  level: number;

  clear(): void {
    this.email = '';
    this.username = '';
    this.clan = '';
    this.level = 0;
  }
}
