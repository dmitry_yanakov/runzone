export interface IAuthNotice {
  type?: string;
  message: string;
}
