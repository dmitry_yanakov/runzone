// Angular
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
// Lodash
import { each } from 'lodash';
// RxJS
import { forkJoin, Observable, of } from 'rxjs';
import { delay, filter, map, mergeMap } from 'rxjs/operators';
import { environment } from '../../../../../environments/environment';
// CRUD
import {
  HttpUtilsService,
  QueryParamsModel,
  QueryResultsModel,
} from '../../../_base/crud';
// Models
import { ActivitiesEditRequestModel, ActivityListModel } from '../types';

const API_ACTIVITIES_URL = environment.apiUrl + '/activities';

@Injectable()
export class ActivitiesService {
  constructor(private http: HttpClient, private httpUtils: HttpUtilsService) {}

  // READ
  getAllActivities(queryParams: QueryParamsModel): Observable<QueryResultsModel> {
    return this.http.get<ActivityListModel[]>(`${API_ACTIVITIES_URL}/get/list`)
      .pipe(
        map((res) => this.httpUtils.baseFilter(res, queryParams, ['status']))
      );
  }

  // DELETE
  deleteActivity(ActivityId: number): Observable<any> {
    return this.http.delete(`${API_ACTIVITIES_URL}/delete/${ActivityId}`);
  }

  // UPDATE =>  POST:
  updateActivities(Activity: ActivitiesEditRequestModel): Observable<ActivitiesEditRequestModel> {
    return this.http.post<ActivitiesEditRequestModel>(`${API_ACTIVITIES_URL}/get/list`, Activity);
  }
}
