// NGRX
import { Update } from '@ngrx/entity';
import { Action } from '@ngrx/store';
// CRUD
import { QueryParamsModel } from '../../../../_base/crud';
// Models
import { ActivitiesEditRequestModel, ActivityListModel } from '../../types';

export enum ActivityActionTypes {
  ActivityOnServerCreated = '[Edit Activity Dialog] Activity On Server Created',
  ActivityUpdated = '[Edit Activity Dialog] Activity Updated',
  ActivitiesStatusUpdated = '[Activity List Page] Activities Status Updated',
  OneActivityDeleted = '[Activities List Page] One Activity Deleted',
  ManyActivitiesDeleted = '[Activities List Page] Many Activity Deleted',
  ActivitiesPageRequested = '[Activities List Page] Activities Page Requested',
  ActivitiesPageLoaded = '[Activities API] Activities Page Loaded',
  ActivitiesPageCancelled = '[Activities API] Activities Page Cancelled',
  ActivitiesPageToggleLoading = '[Activities] Activities Page Toggle Loading',
  ActivityActionToggleLoading = '[Activities] Activities Action Toggle Loading',
}

export class ActivityUpdated implements Action {
  readonly type = ActivityActionTypes.ActivityUpdated;
  constructor(
    public payload: {
      partialActivity: Update<ActivitiesEditRequestModel>; // For State update
      activity: ActivitiesEditRequestModel; // For Server update (through services)
    }
  ) {}
}

export class OneActivityDeleted implements Action {
  readonly type = ActivityActionTypes.OneActivityDeleted;
  constructor(public payload: { id: number }) {}
}

// export class ManyActivitiesDeleted implements Action {
//   readonly type = ActivityActionTypes.ManyActivitiesDeleted;
//   constructor(public payload: { ids: number[] }) {}
// }

export class ActivitiesPageRequested implements Action {
  readonly type = ActivityActionTypes.ActivitiesPageRequested;
  constructor(public payload: { page: QueryParamsModel }) {}
}

export class ActivitiesPageLoaded implements Action {
  readonly type = ActivityActionTypes.ActivitiesPageLoaded;
  constructor(
    public payload: {
      activities: ActivityListModel[];
      totalCount: number;
      page: QueryParamsModel;
    }
  ) {}
}

export class ActivitiesPageCancelled implements Action {
  readonly type = ActivityActionTypes.ActivitiesPageCancelled;
}

export class ActivitiesPageToggleLoading implements Action {
  readonly type = ActivityActionTypes.ActivitiesPageToggleLoading;
  constructor(public payload: { isLoading: boolean }) {}
}

export class ActivityActionToggleLoading implements Action {
  readonly type = ActivityActionTypes.ActivityActionToggleLoading;
  constructor(public payload: { isLoading: boolean }) {}
}

export type ActivityActions =
  | ActivityUpdated
  | OneActivityDeleted
  | ActivitiesPageRequested
  | ActivitiesPageLoaded
  | ActivitiesPageCancelled
  | ActivitiesPageToggleLoading
  | ActivityActionToggleLoading;
