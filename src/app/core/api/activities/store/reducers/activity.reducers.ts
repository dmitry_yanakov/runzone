import {
  createEntityAdapter,
  EntityAdapter,
  EntityState,
  Update,
} from '@ngrx/entity';
import { createFeatureSelector } from '@ngrx/store';

import { ActivityActions, ActivityActionTypes } from '../actions';

import { QueryParamsModel } from '../../../../_base/crud';
import { ActivityListModel } from '../../types';

export interface IActivitiesState extends EntityState<ActivityListModel> {
  listLoading: boolean;
  actionsloading: boolean;
  totalCount: number;
  lastCreatedActivityId: number;
  lastQuery: QueryParamsModel;
  showInitWaitingMessage: boolean;
}

export const adapter: EntityAdapter<ActivityListModel> = createEntityAdapter<
  ActivityListModel
  >();

export const initialActivitiesState: IActivitiesState = adapter.getInitialState({
  activityForEdit: null,
  listLoading: false,
  actionsloading: false,
  totalCount: 0,
  lastCreatedActivityId: undefined,
  lastQuery: new QueryParamsModel({}),
  showInitWaitingMessage: true,
});

export function activitiesReducer(
  state = initialActivitiesState,
  action: ActivityActions,
): IActivitiesState {
  switch (action.type) {
    case ActivityActionTypes.ActivitiesPageToggleLoading: {
      return {
        ...state,
        listLoading: action.payload.isLoading,
        lastCreatedActivityId: undefined,
      };
    }
    case ActivityActionTypes.ActivityActionToggleLoading: {
      return {
        ...state,
        actionsloading: action.payload.isLoading,
      };
    }
    case ActivityActionTypes.ActivityUpdated:
      return adapter.updateOne(action.payload.partialActivity, state);
    case ActivityActionTypes.OneActivityDeleted:
      return adapter.removeOne(action.payload.id, state);
    case ActivityActionTypes.ActivitiesPageCancelled: {
      return {
        ...state,
        listLoading: false,
        lastQuery: new QueryParamsModel({}),
      };
    }
    case ActivityActionTypes.ActivitiesPageLoaded: {
      return adapter.addMany(action.payload.activities, {
        ...initialActivitiesState,
        totalCount: action.payload.totalCount,
        listLoading: false,
        lastQuery: action.payload.page,
        showInitWaitingMessage: false,
      });
    }
    default:
      return state;
  }
}

export const getActivityState = createFeatureSelector<ActivityListModel>(
  'activities',
);
