// NGRX
import { select, Store } from '@ngrx/store';
import { mergeMap, tap } from 'rxjs/operators';
// RxJS
import {
  delay,
  distinctUntilChanged,
  filter,
  map,
  skip,
  take,
} from 'rxjs/operators';
// CRUD
import { BaseDataSource, QueryResultsModel } from '../../../../_base/crud';
// State
import { IAppState } from '../../../../reducers';
import {
  selectActivitiesPageLoading,
  selectActivitiesShowInitWaitingMessage,
  selectActivitiesStateInStore,
} from '../selectors';

export class ActivitiesDataSource extends BaseDataSource {
  constructor(private store: Store<IAppState>) {
    super();

    this.loading$ = this.store.pipe(select(selectActivitiesPageLoading));

    this.isPreloadTextViewed$ = this.store.pipe(
      select(selectActivitiesShowInitWaitingMessage),
    );

    this.store
      .pipe(select(selectActivitiesStateInStore))
      .subscribe((response: QueryResultsModel) => {
        this.paginatorTotalSubject.next(response.totalCount);
        this.entitySubject.next(response.items);
      });
  }
}
