// Angular
import { Injectable } from '@angular/core';
// NGRX
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { forkJoin } from 'rxjs';
import { of } from 'rxjs';
// RxJS
import { delay, map, mergeMap, tap } from 'rxjs/operators';
// CRUD
import { QueryResultsModel } from '../../../../_base/crud';
import { QueryParamsModel } from '../../../../_base/crud';
// State
import { IAppState } from '../../../../reducers';
// Services
import { ActivitiesService } from '../../services';
// Actions
import {
  ActivitiesPageLoaded,
  ActivitiesPageRequested,
  ActivitiesPageToggleLoading,
  ActivityActionToggleLoading,
  ActivityActionTypes,
  ActivityUpdated,
  OneActivityDeleted,
} from '../actions';

@Injectable()
export class ActivityEffects {
  showPageLoadingDistpatcher = new ActivitiesPageToggleLoading({
    isLoading: true,
  });
  showActionLoadingDistpatcher = new ActivityActionToggleLoading({
    isLoading: true,
  });
  hideActionLoadingDistpatcher = new ActivityActionToggleLoading({
    isLoading: false,
  });

  @Effect()
  loadActivitiesPage$ = this.actions$.pipe(
    ofType<ActivitiesPageRequested>(
      ActivityActionTypes.ActivitiesPageRequested,
    ),
    mergeMap(({ payload }) => {
      this.store.dispatch(this.showPageLoadingDistpatcher);
      const requestToServer = this.activitiesService.getAllActivities(
        payload.page,
      );
      const lastQuery = of(payload.page);
      return forkJoin(requestToServer, lastQuery);
    }),
    map((response) => {
      const result: QueryResultsModel = response[0];
      const lastQuery: QueryParamsModel = response[1];
      const pageLoadedDispatch = new ActivitiesPageLoaded({
        activities: result.items,
        totalCount: result.totalCount,
        page: lastQuery,
      });
      return pageLoadedDispatch;
    }),
  );

  @Effect()
  deleteActivity$ = this.actions$.pipe(
    ofType<OneActivityDeleted>(ActivityActionTypes.OneActivityDeleted),
    mergeMap(({ payload }) => {
      this.store.dispatch(this.showActionLoadingDistpatcher);
      return this.activitiesService.deleteActivity(payload.id);
    }),
    map(() => {
      return this.hideActionLoadingDistpatcher;
    }),
  );

  @Effect()
  updateActivity$ = this.actions$.pipe(
    ofType<ActivityUpdated>(ActivityActionTypes.ActivityUpdated),
    mergeMap(({ payload }) => {
      this.store.dispatch(this.showActionLoadingDistpatcher);
      return this.activitiesService.updateActivities(payload.activity);
    }),
    map(() => {
      return this.hideActionLoadingDistpatcher;
    }),
  );

  constructor(
    private actions$: Actions,
    private activitiesService: ActivitiesService,
    private store: Store<IAppState>,
  ) {}
}
