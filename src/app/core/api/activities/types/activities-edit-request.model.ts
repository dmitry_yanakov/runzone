import { BaseModel } from '../../../_base/crud';
import { ActivitySource } from './activity-source.enum';
import { ActivityStatus } from './activity-status.enum';
import { ActivityType } from './activity-type.enum';

export class ActivitiesEditRequestModel extends BaseModel {
  id: string;
  type: ActivityType;
  sourceActivity: ActivitySource;
  vdot: number;
  createDate: string;
  completedDate: string;
  totalDistance: number;
  totalElevation: number;
  duration: number;
  pace: string;
  status: ActivityStatus;
  externalId: number;
  hr: number;

  clear() {
    this.id = '';
    this.type = null;
    this.sourceActivity = null;
    this.vdot = 0;
    this.createDate = '';
    this.completedDate = '';
    this.totalDistance = 0;
    this.totalElevation = 0;
    this.duration = 0;
    this.pace = '';
    this.status = null;
    this.externalId = 0;
    this.hr = 0;
  }
}
