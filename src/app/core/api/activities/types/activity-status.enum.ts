export enum ActivityStatus {
  Active,
  Revise,
  Incorrect,
  Disabled,
  Duplicated,
}
