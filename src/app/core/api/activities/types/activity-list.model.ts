import { BaseModel } from '../../../_base/crud';
import { ActivityStatus } from './activity-status.enum';

export class ActivityListModel extends BaseModel {
  id: string;
  name: string;
  time: string;
  username: string;
  distance: number;
  elevation: number;
  pace: string;
  dateCreate: string;
  dateDone: string;
  status: ActivityStatus;
  erun: number;

  clear(): void {
    this.id = '';
    this.name = '';
    this.time = '';
    this.username = '';
    this.distance = 0;
    this.elevation = 0;
    this.pace = '';
    this.dateCreate = '';
    this.dateDone = '';
    this.status = null;
    this.erun = 0;
  }
}
