// NGRX
import { createFeatureSelector, createSelector } from '@ngrx/store';
// Lodash
import { each } from 'lodash';
// CRUD
import { QueryResultsModel, HttpExtenstionsModel } from '../../_base/crud';
// State
import { ClanLevelsState } from '../_reducers/clan-level.reducers';
import { ClanLevelsModel } from '../_models/clan-levels.model';

export const selectClanLevelsState = createFeatureSelector<ClanLevelsState>('clanLevels');

export const selectClanLevelById = (clanLevelId: number) => createSelector(
  selectClanLevelsState,
  clanLevelsState => clanLevelsState.entities[clanLevelId]
);

export const selectClanLevelsPageLoading = createSelector(
  selectClanLevelsState,
  clanLevelsState => clanLevelsState.listLoading
);

export const selectClanLevelsActionLoading = createSelector(
  selectClanLevelsState,
  clanLevelsState => clanLevelsState.actionsloading
);

export const selectLastCreatedClanLevelId = createSelector(
  selectClanLevelsState,
  clanLevelsState => clanLevelsState.lastCreatedClanLevelId
);

export const selectClanLevelsShowInitWaitingMessage = createSelector(
  selectClanLevelsState,
  clanLevelsState => clanLevelsState.showInitWaitingMessage
);

export const selectClanLevelsInStore = createSelector(
  selectClanLevelsState,
  clanLevelsState => {
    const items: ClanLevelsModel[] = [];
    each(clanLevelsState.entities, element => {
      items.push(element);
    });
    const httpExtension = new HttpExtenstionsModel();
    const result: ClanLevelsModel[] = httpExtension.sortArray(items, clanLevelsState.lastQuery.sortField, clanLevelsState.lastQuery.sortOrder);
    return new QueryResultsModel(result, clanLevelsState.totalCount, '');
  }
);
