// NGRX
import { createFeatureSelector, createSelector } from '@ngrx/store';
// Lodash
import { each } from 'lodash';
// CRUD
import { QueryResultsModel, HttpExtenstionsModel } from '../../_base/crud';
// State
import { EggsState } from '../_reducers/egg.reducers';
import { EggModel } from '../_models/egg.models';

export const selectEggsState = createFeatureSelector<EggsState>('eggs');

export const selectEggById = (eggId: number) => createSelector(
  selectEggsState,
  eggsState => eggsState.entities[eggId]
);

export const selectEggsPageLoading = createSelector(
  selectEggsState,
  eggsState => eggsState.listLoading
);

export const selectEggsActionLoading = createSelector(
  selectEggsState,
  eggsState => eggsState.actionsloading
);

export const selectLastCreatedEggId = createSelector(
  selectEggsState,
  eggsState => eggsState.lastCreatedEggId
);

export const selectEggsShowInitWaitingMessage = createSelector(
  selectEggsState,
  eggsState => eggsState.showInitWaitingMessage
);

export const selectEggsInStore = createSelector(
  selectEggsState,
  eggsState => {
    const items: EggModel[] = [];
    each(eggsState.entities, element => {
      items.push(element);
    });
    const httpExtension = new HttpExtenstionsModel();
    const result: EggModel[] = httpExtension.sortArray(items, eggsState.lastQuery.sortField, eggsState.lastQuery.sortOrder);
    return new QueryResultsModel(result, eggsState.totalCount, '');
  }
);
