// NGRX
import { createFeatureSelector, createSelector } from '@ngrx/store';
// Lodash
import { each } from 'lodash';
// CRUD
import { QueryResultsModel, HttpExtenstionsModel } from '../../_base/crud';
// State
import { CustomizesState } from '../_reducers/customize.reducers';
import { CustomizeModel } from '../_models/customize.model';

export const selectCustomizesState = createFeatureSelector<CustomizesState>('customizes');

export const selectCustomizeById = (customizeId: number) => createSelector(
  selectCustomizesState,
  customizesState => customizesState.entities[customizeId]
);

export const selectCustomizesPageLoading = createSelector(
  selectCustomizesState,
  customizesState => customizesState.listLoading
);

export const selectCustomizesActionLoading = createSelector(
  selectCustomizesState,
  customizesState => customizesState.actionsloading
);

export const selectLastCreatedCustomizeId = createSelector(
  selectCustomizesState,
  customizesState => customizesState.lastCreatedCustomizeId
);

export const selectCustomizesShowInitWaitingMessage = createSelector(
  selectCustomizesState,
  customizesState => customizesState.showInitWaitingMessage
);

export const selectCustomizesInStore = createSelector(
  selectCustomizesState,
  customizesState => {
    const items: CustomizeModel[] = [];
    each(customizesState.entities, element => {
      items.push(element);
    });
    const httpExtension = new HttpExtenstionsModel();
    const result: CustomizeModel[] = httpExtension.sortArray(items, customizesState.lastQuery.sortField, customizesState.lastQuery.sortOrder);
    return new QueryResultsModel(result, customizesState.totalCount, '');
  }
);
