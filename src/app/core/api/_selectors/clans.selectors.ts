// NGRX
import { createFeatureSelector, createSelector } from '@ngrx/store';
// Lodash
import { each } from 'lodash';
// CRUD
import { QueryResultsModel, HttpExtenstionsModel } from '../../_base/crud';
// State
import { ClansState } from '../_reducers/clans.reducers';
import { ClansModel } from '../_models/clans.module';

export const selectClansState = createFeatureSelector<ClansState>('clans');

export const selectClanById = (clanId: number) => createSelector(
  selectClansState,
  clansState => clansState.entities[clanId]
);

export const selectClansPageLoading = createSelector(
  selectClansState,
  clansState => clansState.listLoading
);

export const selectClansActionLoading = createSelector(
  selectClansState,
  clansState => clansState.actionsloading
);

export const selectLastCreatedClanId = createSelector(
  selectClansState,
  clansState => clansState.lastCreatedClanId
);

export const selectClansShowInitWaitingMessage = createSelector(
  selectClansState,
  clansState => clansState.showInitWaitingMessage
);

export const selectClansInStore = createSelector(
  selectClansState,
  clansState => {
    const items: ClansModel[] = [];
    each(clansState.entities, element => {
      items.push(element);
    });
    const httpExtension = new HttpExtenstionsModel();
    const result: ClansModel[] = httpExtension.sortArray(items, clansState.lastQuery.sortField, clansState.lastQuery.sortOrder);
    return new QueryResultsModel(result, clansState.totalCount, '');
  }
);
