// NGRX
import { createFeatureSelector, createSelector } from '@ngrx/store';
// Lodash
import { each } from 'lodash';
// CRUD
import { QueryResultsModel, HttpExtenstionsModel } from '../../_base/crud';
// State
import { UsersState } from '../_reducers/users.reducers';
import { UserModel } from '../_models/user.model';

export const selectUsersState = createFeatureSelector<UsersState>('users');

export const selectUserById = (customerId: number) => createSelector(
  selectUsersState,
  usersState => usersState.entities[customerId]
);

export const selectUsersPageLoading = createSelector(
  selectUsersState,
  customersState => customersState.listLoading
);

export const selectUsersActionLoading = createSelector(
  selectUsersState,
  customersState => customersState.actionsloading
);

export const selectLastCreatedUserId = createSelector(
  selectUsersState,
  customersState => customersState.lastCreatedCustomerId
);

export const selectUsersShowInitWaitingMessage = createSelector(
  selectUsersState,
  customersState => customersState.showInitWaitingMessage
);

export const selectUsersInStore = createSelector(
  selectUsersState,
  usersState => {
    const items: UserModel[] = [];
    each(usersState.entities, element => {
      items.push(element);
    });
    const httpExtension = new HttpExtenstionsModel();
    const result: UserModel[] = httpExtension.sortArray(items, usersState.lastQuery.sortField, usersState.lastQuery.sortOrder);
    return new QueryResultsModel(result, usersState.totalCount, '');
  }
);
