// NGRX
import { createFeatureSelector, createSelector } from '@ngrx/store';
// Lodash
import { each } from 'lodash';
// CRUD
import { QueryResultsModel, HttpExtenstionsModel } from '../../_base/crud';
// State
import { RanksState } from '../_reducers/rank.reducers';
import { RankModel } from '../_models/rank.model';

export const selectRanksState = createFeatureSelector<RanksState>('ranks');

export const selectRankById = (rankId: number) => createSelector(
  selectRanksState,
  ranksState => ranksState.entities[rankId]
);

export const selectRanksPageLoading = createSelector(
  selectRanksState,
  ranksState => ranksState.listLoading
);

export const selectRanksActionLoading = createSelector(
  selectRanksState,
  ranksState => ranksState.actionsloading
);

export const selectLastCreatedRankId = createSelector(
  selectRanksState,
  ranksState => ranksState.lastCreatedRankId
);

export const selectRanksShowInitWaitingMessage = createSelector(
  selectRanksState,
  ranksState => ranksState.showInitWaitingMessage
);

export const selectRanksInStore = createSelector(
  selectRanksState,
  ranksState => {
    const items: RankModel[] = [];
    each(ranksState.entities, element => {
      items.push(element);
    });
    const httpExtension = new HttpExtenstionsModel();
    const result: RankModel[] = httpExtension.sortArray(items, ranksState.lastQuery.sortField, ranksState.lastQuery.sortOrder);
    return new QueryResultsModel(result, ranksState.totalCount, '');
  }
);
