// NGRX
import { createFeatureSelector, createSelector } from '@ngrx/store';
// Lodash
import { each } from 'lodash';
// CRUD
import { HttpExtenstionsModel, QueryResultsModel } from '../../_base/crud';
import { ActivitiesModel } from '../_models/activities.model';
// State
import { ActivitiesState } from '../_reducers/activity.reducers';

export const selectActivitiesState = createFeatureSelector<ActivitiesState>('activities');

export const selectActivityById = (activityId: number) => createSelector(
  selectActivitiesState,
  (activitiesState) => activitiesState.entities[activityId],
);

export const selectActivitiesPageLoading = createSelector(
  selectActivitiesState,
  (activitiesState) => activitiesState.listLoading,
);

export const selectActivitiesActionLoading = createSelector(
  selectActivitiesState,
  (activitiesState) => activitiesState.actionsloading,
);

export const selectLastCreatedActivityId = createSelector(
  selectActivitiesState,
  (activitiesState) => activitiesState.lastCreatedActivityId,
);

export const selectActivitiesShowInitWaitingMessage = createSelector(
  selectActivitiesState,
  (activitiesState) => activitiesState.showInitWaitingMessage,
);

export const selectActivitiesStateInStore = createSelector(
  selectActivitiesState,
  (activitiesState) => {
    const items: ActivitiesModel[] = [];
    each(activitiesState.entities, (element) => {
      items.push(element);
    });
    const httpExtension = new HttpExtenstionsModel();
    const result: ActivitiesModel[] = httpExtension.sortArray(items, activitiesState.lastQuery.sortField, activitiesState.lastQuery.sortOrder);
    return new QueryResultsModel(result, activitiesState.totalCount, '');
  },
);
