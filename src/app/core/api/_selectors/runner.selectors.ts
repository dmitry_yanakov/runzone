// NGRX
import { createFeatureSelector, createSelector } from '@ngrx/store';
// Lodash
import { each } from 'lodash';
// CRUD
import { QueryResultsModel, HttpExtenstionsModel } from '../../_base/crud';
// State
import { RunnersState } from '../_reducers/runner.reducers';
import { RunnerModel } from '../_models/runner.models';

export const selectRunnersState = createFeatureSelector<RunnersState>('runners');

export const selectRunnerById = (runnerId: number) => createSelector(
  selectRunnersState,
  runnersState => runnersState.entities[runnerId]
);

export const selectRunnersPageLoading = createSelector(
  selectRunnersState,
  runnersState => runnersState.listLoading
);

export const selectRunnersActionLoading = createSelector(
  selectRunnersState,
  runnersState => runnersState.actionsloading
);

export const selectLastCreatedRunnerId = createSelector(
  selectRunnersState,
  runnersState => runnersState.lastCreatedRunnerId
);

export const selectRunnersShowInitWaitingMessage = createSelector(
  selectRunnersState,
  runnersState => runnersState.showInitWaitingMessage
);

export const selectRunnersInStore = createSelector(
  selectRunnersState,
  runnersState => {
    const items: RunnerModel[] = [];
    each(runnersState.entities, element => {
      items.push(element);
    });
    const httpExtension = new HttpExtenstionsModel();
    const result: RunnerModel[] = httpExtension.sortArray(items, runnersState.lastQuery.sortField, runnersState.lastQuery.sortOrder);
    return new QueryResultsModel(result, runnersState.totalCount, '');
  }
);
