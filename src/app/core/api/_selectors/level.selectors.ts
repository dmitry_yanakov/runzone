// NGRX
import { createFeatureSelector, createSelector } from '@ngrx/store';
// Lodash
import { each } from 'lodash';
// CRUD
import { QueryResultsModel, HttpExtenstionsModel } from '../../_base/crud';
// State
import { LevelsState } from '../_reducers/level.reducers';
import { LevelModel } from '../_models/level.model';

export const selectLevelsState = createFeatureSelector<LevelsState>('levels');

export const selectLevelById = (levelId: number) => createSelector(
  selectLevelsState,
  levelsState => levelsState.entities[levelId]
);

export const selectLevelsPageLoading = createSelector(
  selectLevelsState,
  levelsState => levelsState.listLoading
);

export const selectLevelsActionLoading = createSelector(
  selectLevelsState,
  levelsState => levelsState.actionsloading
);

export const selectLastCreatedLevelId = createSelector(
  selectLevelsState,
  levelsState => levelsState.lastCreatedLevelId
);

export const selectLevelsShowInitWaitingMessage = createSelector(
  selectLevelsState,
  levelsState => levelsState.showInitWaitingMessage
);

export const selectLevelsInStore = createSelector(
  selectLevelsState,
  levelsState => {
    const items: LevelModel[] = [];
    each(levelsState.entities, element => {
      items.push(element);
    });
    const httpExtension = new HttpExtenstionsModel();
    const result: LevelModel[] = httpExtension.sortArray(items, levelsState.lastQuery.sortField, levelsState.lastQuery.sortOrder);
    return new QueryResultsModel(result, levelsState.totalCount, '');
  }
);
