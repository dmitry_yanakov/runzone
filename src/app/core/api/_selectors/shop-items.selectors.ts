// NGRX
import { createFeatureSelector, createSelector } from '@ngrx/store';
// Lodash
import { each } from 'lodash';
// CRUD
import { QueryResultsModel, HttpExtenstionsModel } from '../../_base/crud';
// State
import { ShopItemsState } from '../_reducers/shop-items.reducers';
import { ShopItemModel } from '../_models/shop-item.model';

export const selectShopItemsState = createFeatureSelector<ShopItemsState>('shopItems');

export const selectShopItemById = (shopItemId: number) => createSelector(
  selectShopItemsState,
  shopItemsState => shopItemsState.entities[shopItemId]
);

export const selectShopItemsPageLoading = createSelector(
  selectShopItemsState,
  shopItemsState => shopItemsState.listLoading
);

export const selectShopItemsActionLoading = createSelector(
  selectShopItemsState,
  shopItemsState => shopItemsState.actionsloading
);

export const selectLastCreatedShopItemId = createSelector(
  selectShopItemsState,
  shopItemsState => shopItemsState.lastCreatedShopItemId
);

export const selectShopItemsShowInitWaitingMessage = createSelector(
  selectShopItemsState,
  shopItemsState => shopItemsState.showInitWaitingMessage
);

export const selectShopItemsInStore = createSelector(
  selectShopItemsState,
  shopItemsState => {
    const items: ShopItemModel[] = [];
    each(shopItemsState.entities, element => {
      items.push(element);
    });
    const httpExtension = new HttpExtenstionsModel();
    const result: ShopItemModel[] = httpExtension.sortArray(items, shopItemsState.lastQuery.sortField, shopItemsState.lastQuery.sortOrder);
    return new QueryResultsModel(result, shopItemsState.totalCount, '');
  }
);
