import { BaseModel } from '../../../_base/crud';
import { CountryModel } from '../../country/types';

export class ClanEditRequestModel extends BaseModel {
  name: string;
  id: string;
  country: CountryModel;
  liderId: string;
  trophies: number;
  level: number;
  xp: number;

  clear() {
    this.name = '';
    this.id = '';
    this.country = null;
    this.liderId = '';
    this.trophies = 0;
    this.level = 0;
  }
}
