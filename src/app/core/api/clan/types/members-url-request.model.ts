import { BaseModel } from '../../../_base/crud';

export class MembersUrlRequestModel extends BaseModel {
  clanId: string;
  userId: string;
}
