import { BaseModel } from '../../../_base/crud';

export class ClanLevelModel extends BaseModel {
  number: string;
  distance: number;
  accumulateDistance: number;
  upgradeForGems: number;
  maxMembers: number;

  clear() {
    this.number = '';
    this.distance = 0;
    this.accumulateDistance = 0;
    this.upgradeForGems = 0;
    this.maxMembers = 0;
  }
}
