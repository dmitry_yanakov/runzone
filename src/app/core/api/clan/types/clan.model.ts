import { BaseModel } from '../../../_base/crud';
import { FrendshipType } from '../../../_shared/types';
import { CountryModel } from '../../country/types';

export class ClanModel extends BaseModel {
  name: string;
  id: string;
  country: CountryModel;
  lider: string;
  liderId: string;
  trophies: number;
  level: number;
  xp: number;
  type: FrendshipType;

  clear() {
    this.name = '';
    this.id = '';
    this.country = null;
    this.lider = '';
    this.liderId = '';
    this.trophies = 0;
    this.level = 0;
    this.xp = 0;
    this.type = null;
  }
}
