import { BaseModel } from '../../../_base/crud';
import { CountryModel } from '../../country/types';
import { ClanStatusMember } from './clan-status.enum';

export class MembershipModel extends BaseModel {
  id: string;
  country: CountryModel;
  username: string;
  email: string;
  trophies: number;
  level: number;
  xp: number;
  dateJoined: string;
  status: ClanStatusMember;

  clear() {
    this.id = '';
    this.country = null;
    this.username = '';
    this.email = '';
    this.trophies = 0;
    this.level = 0;
    this.xp = 0;
    this.dateJoined = '';
    this.status = null;
  }
}
