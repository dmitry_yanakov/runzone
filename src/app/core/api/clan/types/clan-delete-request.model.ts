import { BaseModel } from '../../../_base/crud';

export class ClanDeleteRequestModel extends BaseModel {
  id: string;
  email: string;

  clear() {
    this.id = '';
    this.email = '';
  }
}
