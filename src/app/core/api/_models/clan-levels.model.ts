import { BaseModel } from '../../_base/crud';


export class ClanLevelsModel  extends BaseModel {
  id: number;
  level: number;
  distance: number;
  accumulateDistance: number;
  upgradeGems: number;
  maxMembers: number;
  clear() {
   this.level = 0;
   this.distance = 0;
   this.accumulateDistance = 0;
   this.upgradeGems = 0;
   this.maxMembers = 0;
  }
}
