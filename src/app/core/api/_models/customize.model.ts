import { BaseModel } from '../../_base/crud';


export class CustomizeModel  extends BaseModel {
  id: number;
  name: string;
  image: string;
  unlockAtLevel: number;
  type: string;
  unlockAtEvent: number;
  cost: number;
  costCurrency: string;
  clear() {
    this.name = '';
   this.image = '';
   this.unlockAtLevel = 0;
   this.type = '';
   this.unlockAtEvent = 0;
   this.cost = 0;
   this.costCurrency = '';
  }
}
