import { BaseModel } from '../../_base/crud';


export class LevelModel  extends BaseModel {
  id: number;
  level: number;
  xpLevelUp: number;
  getGold: number;
  getGems: number;
  unlockEgg: number;
  clear() {
    this.level = 0;
    this.xpLevelUp = 0;
    this.getGold = 0;
    this.getGems = 0;
    this.unlockEgg = 0;
  }
}
