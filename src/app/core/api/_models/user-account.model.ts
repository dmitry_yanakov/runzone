import { BaseModel } from '../../_base/crud';


export class UserAccountModel  extends BaseModel {
  id: number;
  username: string;
  email: string;
  dateBirth: string;
  city: string;
  language: string;
  timeZone: string;
  gender: string;
  measurementUnits: string;
  firstDayOfWeek: string;
  height: number;
  weight: number;
  description: string;
  tracker: string;
  facebook: string;
  strava: string;

  clear() {
    this.dateBirth = '';
    this.city = '';
    this.language = '';
    this.timeZone = '';
    this.gender = '';
    this.measurementUnits = '';
    this.firstDayOfWeek = '';
    this.weight = 1;
    this.height = 1;
    this.tracker = '';
    this.facebook = '';
    this.strava = '';
  }
}
