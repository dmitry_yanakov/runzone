import { BaseModel } from '../../_base/crud';


export class UserModel  extends BaseModel {
  id: number;
  country : string;
  username : string;
  email : string;
  clan : string;
  trophies : number;
  level : number;
  xp : number;
  type : 1; // 0 - admin , 1 - active , 2 - verify , 3 - blocked , 4 - delete
  status: number; // 0 - online, 1 - offline

  clear() {
    this.country = '';
    this.username = '';
    this.email = '';
    this.clan = '';
    this.trophies = undefined;
    this.level = undefined;
    this.xp = undefined;
    this.type = 1;
    this.status = 1;
  }
}
