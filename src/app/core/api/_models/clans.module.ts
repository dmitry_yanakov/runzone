import { BaseModel } from '../../_base/crud';


export class ClansModel  extends BaseModel {
  id: number;
  country: string;
  name: string;
  members: number;
  lider: string;
  trophies: number;
  level: number;
  xp: number;
  type: number;// 0 - blocked , 1 - open , 2 closed , 3 - request , 4 - deleted
  status: number; // 0 - offline , 1 - online

  clear() {
   this.country = '';
   this.name = '';
   this.members = 0;
   this.lider = '';
   this.trophies = 0;
   this.level = 0;
   this.xp = 0;
   this.type = 1;
   this.status = 0;
  }
}
