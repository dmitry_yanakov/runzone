import { BaseModel } from '../../_base/crud';


export class UserGameProfileModel  extends BaseModel {
  id: number;
  clan : string;
  trophies : number;
  level : number;
  xp : number;
  totalXp: number
  type : 1; // 0 - admin , 1 - active , 2 - verify , 3 - blocked , 4 - delete
  status: number; // 0 - online, 1 - offline
  lastAuth: string;
  KM1: string;
  KM3: string;
  KM5: string;
  KM10: string;
  KM21: string;
  KM42: string;
  bestDistance: string
  coins: number;
  gems: number;

  clear() {
    this.clan = '';
    this.trophies = 0;
    this.level = 0;
    this.xp = 0;
    this.type = 1;
    this.status = 1;
    this.totalXp = 0;
    this.lastAuth = 'none';
    this.KM1 = 'none';
    this.KM3 = 'none';
    this.KM5 = 'none';
    this.KM10 = 'none';
    this.KM1 = 'none';
    this.KM21 = 'none';
    this.KM42 = 'none';
    this.bestDistance = 'none'
    this.coins = 0;
    this.gems = 0;
  }
}
