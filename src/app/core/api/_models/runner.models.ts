import { BaseModel } from '../../_base/crud';

export class RunnerModel  extends BaseModel {
  id: number;
  name: string;
  rarity: string;
  unlockAtLevel: number;
  type: string;
  location:string;
  speed: number;
  endurance:number;
  climb: number;

  clear() {
   this.name = '';
   this.rarity = '';
   this.unlockAtLevel = 0;
   this.type = '';
   this.location = '';
   this.speed = 0;
   this.endurance = 0;
   this.climb = 0;
  }
}
