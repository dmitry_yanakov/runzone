import { BaseModel } from '../../_base/crud';


export class ActivitiesModel  extends BaseModel {
  id: number;
  name: string;
  time: string;
  username: string;
  distance: number;
  elevation: number;
  pace: string;
  dateDone: string;
  erun: number;
  status: number; // 0 - revise, 1 - incorrect, 2 - duplicated, 3 - active , 4 - disabled

  clear() {
    this.name = '';
    this.time = '';
    this.username = '';
    this.distance = 0;
    this.elevation = 0;
    this.pace = '';
    this.dateDone = '';
    this.erun = 0;
    this.status = 1;
  }
}
