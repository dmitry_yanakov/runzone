import { BaseModel } from '../../_base/crud';


export class ShopItemModel extends BaseModel {
  id: number;
  name: string;
  quantityGoods: number;
  typeGoods: string;
  price: number;
  typePrice: string;
  rotate: boolean;
  priority: number;
  category: number;

  clear() {
   this.name = '';
   this.quantityGoods = 0;
   this.typeGoods = '';
   this.price = 0;
   this.typePrice = '';
   this.rotate = false;
   this.priority = 0;
   this.category = 0;
  }
}
