import { BaseModel } from '../../_base/crud';


export class EggModel  extends BaseModel {
  id: number;
  name: string;
  distance: number;
  unlockAtLevel: number;
  xpGained: number;
  goldGained: number;
  typeEgg: string;
  clear() {
    this.name = '';
    this.distance = 0;
    this.unlockAtLevel = 0;
    this.xpGained = 0;
    this.goldGained = 0;
    this.typeEgg = 'Normal';
  }
}
