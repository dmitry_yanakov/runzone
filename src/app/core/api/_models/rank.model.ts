import { BaseModel } from '../../_base/crud';


export class RankModel  extends BaseModel {
  id: number;
  rank: number;
  rarity: string;
  numberOfCopies: number;
  goldRequired: number;
  xpGained: number;
  clear() {
   this.rank = 0;
   this.rarity = 'Common';
   this.numberOfCopies = 0;
   this.goldRequired = 0;
   this.xpGained = 0;
  }
}
