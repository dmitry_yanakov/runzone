import { QueryParamsModel } from './../../_base/crud/models/query-models/query-params.model';
import { forkJoin } from 'rxjs';
// Angular
import { Injectable } from '@angular/core';
// RxJS
import { mergeMap, map, tap, delay } from 'rxjs/operators';
// NGRX
import { Effect, Actions, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
// CRUD
import { QueryResultsModel } from '../../_base/crud';
// Services
import { ClansService } from '../_services/clans.service.fake';
// State
import { IAppState } from '../../../core/reducers';
// Actions
import {
  ClanActionTypes,
  ClansPageRequested,
  ClansPageLoaded,
  ManyClansDeleted,
  OneClanDeleted,
  ClanActionToggleLoading,
  ClansPageToggleLoading,
  ClanUpdated,
  ClansStatusUpdated,
  ClanCreated,
  ClanOnServerCreated,
} from '../_actions/clans.actions';
import { of } from 'rxjs';

@Injectable()
export class ClanEffects {
  showPageLoadingDistpatcher = new ClansPageToggleLoading({ isLoading: true });
  showActionLoadingDistpatcher = new ClanActionToggleLoading({
    isLoading: true,
  });
  hideActionLoadingDistpatcher = new ClanActionToggleLoading({
    isLoading: false,
  });

  @Effect()
  loadClansPage$ = this.actions$.pipe(
    ofType<ClansPageRequested>(ClanActionTypes.ClansPageRequested),
    mergeMap(({ payload }) => {
      this.store.dispatch(this.showPageLoadingDistpatcher);
      const requestToServer = this.clansService.findClan(payload.page);
      const lastQuery = of(payload.page);
      return forkJoin(requestToServer, lastQuery);
    }),
    map((response) => {
      const result: QueryResultsModel = response[0];
      const lastQuery: QueryParamsModel = response[1];
      const pageLoadedDispatch = new ClansPageLoaded({
        clans: result.items,
        totalCount: result.totalCount,
        page: lastQuery,
      });
      return pageLoadedDispatch;
    })
  );

  @Effect()
  deleteClan$ = this.actions$.pipe(
    ofType<OneClanDeleted>(ClanActionTypes.OneClanDeleted),
    mergeMap(({ payload }) => {
      this.store.dispatch(this.showActionLoadingDistpatcher);
      return this.clansService.deleteClan(payload.id);
    }),
    map(() => {
      return this.hideActionLoadingDistpatcher;
    })
  );

  @Effect()
  deleteClans$ = this.actions$.pipe(
    ofType<ManyClansDeleted>(ClanActionTypes.ManyClansDeleted),
    mergeMap(({ payload }) => {
      this.store.dispatch(this.showActionLoadingDistpatcher);
      return this.clansService.deleteClans(payload.ids);
    }),
    map(() => {
      return this.hideActionLoadingDistpatcher;
    })
  );

  @Effect()
  updateClan$ = this.actions$.pipe(
    ofType<ClanUpdated>(ClanActionTypes.ClanUpdated),
    mergeMap(({ payload }) => {
      this.store.dispatch(this.showActionLoadingDistpatcher);
      return this.clansService.updateClan(payload.clan);
    }),
    map(() => {
      return this.hideActionLoadingDistpatcher;
    })
  );

  @Effect()
  updateClansStatus$ = this.actions$.pipe(
    ofType<ClansStatusUpdated>(ClanActionTypes.ClansStatusUpdated),
    mergeMap(({ payload }) => {
      this.store.dispatch(this.showActionLoadingDistpatcher);
      return this.clansService.updateStatusForClan(
        payload.clans,
        payload.status
      );
    }),
    map(() => {
      return this.hideActionLoadingDistpatcher;
    })
  );

  @Effect()
  createClan$ = this.actions$.pipe(
    ofType<ClanOnServerCreated>(ClanActionTypes.ClanOnServerCreated),
    mergeMap(({ payload }) => {
      this.store.dispatch(this.showActionLoadingDistpatcher);
      return this.clansService.createClan(payload.clan).pipe(
        tap((res) => {
          this.store.dispatch(new ClanCreated({ clan: res }));
        })
      );
    }),
    map(() => {
      return this.hideActionLoadingDistpatcher;
    })
  );

  constructor(
    private actions$: Actions,
    private clansService: ClansService,
    private store: Store<IAppState>
  ) {}
}
