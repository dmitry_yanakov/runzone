import { QueryParamsModel } from './../../_base/crud/models/query-models/query-params.model';
import { forkJoin } from 'rxjs';
// Angular
import { Injectable } from '@angular/core';
// RxJS
import { mergeMap, map, tap, delay } from 'rxjs/operators';
// NGRX
import { Effect, Actions, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
// CRUD
import { QueryResultsModel } from '../../_base/crud';
// Services
import { EggsService } from '../_services/egg.services.fake';
// State
import { IAppState } from '../../../core/reducers';
// Actions
import {
  EggActionTypes,
  EggsPageRequested,
  EggsPageLoaded,
  ManyEggsDeleted,
  OneEggDeleted,
  EggActionToggleLoading,
  EggsPageToggleLoading,
  EggUpdated,
  EggsStatusUpdated,
  EggCreated,
  EggOnServerCreated,
} from '../_actions/egg.actions';
import { of } from 'rxjs';

@Injectable()
export class EggEffects {
  showPageLoadingDistpatcher = new EggsPageToggleLoading({ isLoading: true });
  showActionLoadingDistpatcher = new EggActionToggleLoading({
    isLoading: true,
  });
  hideActionLoadingDistpatcher = new EggActionToggleLoading({
    isLoading: false,
  });

  @Effect()
  loadEggsPage$ = this.actions$.pipe(
    ofType<EggsPageRequested>(EggActionTypes.EggsPageRequested),
    mergeMap(({ payload }) => {
      this.store.dispatch(this.showPageLoadingDistpatcher);
      const requestToServer = this.eggsService.findEggs(payload.page);
      const lastQuery = of(payload.page);
      return forkJoin(requestToServer, lastQuery);
    }),
    map((response) => {
      const result: QueryResultsModel = response[0];
      const lastQuery: QueryParamsModel = response[1];
      const pageLoadedDispatch = new EggsPageLoaded({
        eggs: result.items,
        totalCount: result.totalCount,
        page: lastQuery,
      });
      return pageLoadedDispatch;
    })
  );

  @Effect()
  deleteEgg$ = this.actions$.pipe(
    ofType<OneEggDeleted>(EggActionTypes.OneEggDeleted),
    mergeMap(({ payload }) => {
      this.store.dispatch(this.showActionLoadingDistpatcher);
      return this.eggsService.deleteEgg(payload.id);
    }),
    map(() => {
      return this.hideActionLoadingDistpatcher;
    })
  );

  @Effect()
  deleteEggs$ = this.actions$.pipe(
    ofType<ManyEggsDeleted>(EggActionTypes.ManyEggsDeleted),
    mergeMap(({ payload }) => {
      this.store.dispatch(this.showActionLoadingDistpatcher);
      return this.eggsService.deleteEggs(payload.ids);
    }),
    map(() => {
      return this.hideActionLoadingDistpatcher;
    })
  );

  @Effect()
  updateEgg$ = this.actions$.pipe(
    ofType<EggUpdated>(EggActionTypes.EggUpdated),
    mergeMap(({ payload }) => {
      this.store.dispatch(this.showActionLoadingDistpatcher);
      return this.eggsService.updateEgg(payload.egg);
    }),
    map(() => {
      return this.hideActionLoadingDistpatcher;
    })
  );

  @Effect()
  createEgg$ = this.actions$.pipe(
    ofType<EggOnServerCreated>(EggActionTypes.EggOnServerCreated),
    mergeMap(({ payload }) => {
      this.store.dispatch(this.showActionLoadingDistpatcher);
      return this.eggsService.createEgg(payload.egg).pipe(
        tap((res) => {
          this.store.dispatch(new EggCreated({ egg: res }));
        })
      );
    }),
    map(() => {
      return this.hideActionLoadingDistpatcher;
    })
  );

  constructor(
    private actions$: Actions,
    private eggsService: EggsService,
    private store: Store<IAppState>
  ) {}
}
