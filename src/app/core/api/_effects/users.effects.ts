import { QueryParamsModel } from './../../_base/crud/models/query-models/query-params.model';
import { forkJoin } from 'rxjs';
// Angular
import { Injectable } from '@angular/core';
// RxJS
import { mergeMap, map, tap, delay } from 'rxjs/operators';
// NGRX
import { Effect, Actions, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
// CRUD
import { QueryResultsModel } from '../../_base/crud';
// Services
import { UsersService } from '../_services/';
// State
import { IAppState } from '../../../core/reducers';
// Actions
import {
  UserActionTypes,
  UsersPageRequested,
  UsersPageLoaded,
  ManyUsersDeleted,
  OneUserDeleted,
  UserActionToggleLoading,
  UsersPageToggleLoading,
  UserUpdated,
  UsersStatusUpdated,
  UserCreated,
  UserOnServerCreated,
} from '../_actions/users.actions';
import { of } from 'rxjs';

@Injectable()
export class UserEffects {
  showPageLoadingDistpatcher = new UsersPageToggleLoading({ isLoading: true });
  showActionLoadingDistpatcher = new UserActionToggleLoading({
    isLoading: true,
  });
  hideActionLoadingDistpatcher = new UserActionToggleLoading({
    isLoading: false,
  });

  @Effect()
  loadCustomersPage$ = this.actions$.pipe(
    ofType<UsersPageRequested>(UserActionTypes.UsersPageRequested),
    mergeMap(({ payload }) => {
      this.store.dispatch(this.showPageLoadingDistpatcher);
      const requestToServer = this.customersService.findCustomers(payload.page);
      const lastQuery = of(payload.page);
      return forkJoin(requestToServer, lastQuery);
    }),
    map((response) => {
      const result: QueryResultsModel = response[0];
      const lastQuery: QueryParamsModel = response[1];
      const pageLoadedDispatch = new UsersPageLoaded({
        customers: result.items,
        totalCount: result.totalCount,
        page: lastQuery,
      });
      console.log(pageLoadedDispatch);
      return pageLoadedDispatch;
    })
  );

  @Effect()
  deleteCustomer$ = this.actions$.pipe(
    ofType<OneUserDeleted>(UserActionTypes.OneUserDeleted),
    mergeMap(({ payload }) => {
      this.store.dispatch(this.showActionLoadingDistpatcher);
      return this.customersService.deleteCustomer(payload.id);
    }),
    map(() => {
      return this.hideActionLoadingDistpatcher;
    })
  );

  @Effect()
  deleteCustomers$ = this.actions$.pipe(
    ofType<ManyUsersDeleted>(UserActionTypes.ManyUsersDeleted),
    mergeMap(({ payload }) => {
      this.store.dispatch(this.showActionLoadingDistpatcher);
      return this.customersService.deleteCustomers(payload.ids);
    }),
    map(() => {
      return this.hideActionLoadingDistpatcher;
    })
  );

  @Effect()
  updateCustomer$ = this.actions$.pipe(
    ofType<UserUpdated>(UserActionTypes.UserUpdated),
    mergeMap(({ payload }) => {
      this.store.dispatch(this.showActionLoadingDistpatcher);
      return this.customersService.updateCustomer(payload.customer);
    }),
    map(() => {
      return this.hideActionLoadingDistpatcher;
    })
  );

  @Effect()
  updateCustomersStatus$ = this.actions$.pipe(
    ofType<UsersStatusUpdated>(UserActionTypes.UsersStatusUpdated),
    mergeMap(({ payload }) => {
      this.store.dispatch(this.showActionLoadingDistpatcher);
      return this.customersService.updateStatusForCustomer(
        payload.customers,
        payload.status
      );
    }),
    map(() => {
      return this.hideActionLoadingDistpatcher;
    })
  );

  @Effect()
  createCustomer$ = this.actions$.pipe(
    ofType<UserOnServerCreated>(UserActionTypes.UserOnServerCreated),
    mergeMap(({ payload }) => {
      this.store.dispatch(this.showActionLoadingDistpatcher);
      return this.customersService.createCustomer(payload.customer).pipe(
        tap((res) => {
          this.store.dispatch(new UserCreated({ customer: res }));
        })
      );
    }),
    map(() => {
      return this.hideActionLoadingDistpatcher;
    })
  );

  constructor(
    private actions$: Actions,
    private customersService: UsersService,
    private store: Store<IAppState>
  ) {}
}
