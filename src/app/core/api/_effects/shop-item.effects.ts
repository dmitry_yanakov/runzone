import { QueryParamsModel } from './../../_base/crud/models/query-models/query-params.model';
import { forkJoin } from 'rxjs';
// Angular
import { Injectable } from '@angular/core';
// RxJS
import { mergeMap, map, tap, delay } from 'rxjs/operators';
// NGRX
import { Effect, Actions, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
// CRUD
import { QueryResultsModel } from '../../_base/crud';
// Services
import { ShopItemsService } from '../_services/shop-item.service.fake';
// State
import { IAppState } from '../../../core/reducers';
// Actions
import {
  ShopItemActionTypes,
  ShopItemsPageRequested,
  ShopItemsPageLoaded,
  ManyShopItemsDeleted,
  OneShopItemDeleted,
  ShopItemActionToggleLoading,
  ShopItemsPageToggleLoading,
  ShopItemUpdated,
  ShopItemsStatusUpdated,
  ShopItemCreated,
  ShopItemOnServerCreated,
} from '../_actions/shop-item.actions';
import { of } from 'rxjs';

@Injectable()
export class ShopItemEffects {
  showPageLoadingDistpatcher = new ShopItemsPageToggleLoading({
    isLoading: true,
  });
  showActionLoadingDistpatcher = new ShopItemActionToggleLoading({
    isLoading: true,
  });
  hideActionLoadingDistpatcher = new ShopItemActionToggleLoading({
    isLoading: false,
  });

  @Effect()
  loadShopItemsPage$ = this.actions$.pipe(
    ofType<ShopItemsPageRequested>(ShopItemActionTypes.ShopItemsPageRequested),
    mergeMap(({ payload }) => {
      this.store.dispatch(this.showPageLoadingDistpatcher);
      const requestToServer = this.shopItemsService.findShopItems(payload.page);
      const lastQuery = of(payload.page);
      return forkJoin(requestToServer, lastQuery);
    }),
    map((response) => {
      const result: QueryResultsModel = response[0];
      const lastQuery: QueryParamsModel = response[1];
      const pageLoadedDispatch = new ShopItemsPageLoaded({
        shopItems: result.items,
        totalCount: result.totalCount,
        page: lastQuery,
      });
      return pageLoadedDispatch;
    })
  );

  @Effect()
  deleteShopItem$ = this.actions$.pipe(
    ofType<OneShopItemDeleted>(ShopItemActionTypes.OneShopItemDeleted),
    mergeMap(({ payload }) => {
      this.store.dispatch(this.showActionLoadingDistpatcher);
      return this.shopItemsService.deleteShopItem(payload.id);
    }),
    map(() => {
      return this.hideActionLoadingDistpatcher;
    })
  );

  @Effect()
  deleteShopItems$ = this.actions$.pipe(
    ofType<ManyShopItemsDeleted>(ShopItemActionTypes.ManyShopItemsDeleted),
    mergeMap(({ payload }) => {
      this.store.dispatch(this.showActionLoadingDistpatcher);
      return this.shopItemsService.deleteShopItems(payload.ids);
    }),
    map(() => {
      return this.hideActionLoadingDistpatcher;
    })
  );

  @Effect()
  updateShopItem$ = this.actions$.pipe(
    ofType<ShopItemUpdated>(ShopItemActionTypes.ShopItemUpdated),
    mergeMap(({ payload }) => {
      this.store.dispatch(this.showActionLoadingDistpatcher);
      return this.shopItemsService.updateShopItem(payload.shopItem);
    }),
    map(() => {
      return this.hideActionLoadingDistpatcher;
    })
  );

  @Effect()
  createShopItem$ = this.actions$.pipe(
    ofType<ShopItemOnServerCreated>(
      ShopItemActionTypes.ShopItemOnServerCreated
    ),
    mergeMap(({ payload }) => {
      this.store.dispatch(this.showActionLoadingDistpatcher);
      return this.shopItemsService.createShopItem(payload.shopItem).pipe(
        tap((res) => {
          this.store.dispatch(new ShopItemCreated({ shopItem: res }));
        })
      );
    }),
    map(() => {
      return this.hideActionLoadingDistpatcher;
    })
  );

  constructor(
    private actions$: Actions,
    private shopItemsService: ShopItemsService,
    private store: Store<IAppState>
  ) {}
}
