import { QueryParamsModel } from './../../_base/crud/models/query-models/query-params.model';
import { forkJoin } from 'rxjs';
// Angular
import { Injectable } from '@angular/core';
// RxJS
import { mergeMap, map, tap, delay } from 'rxjs/operators';
// NGRX
import { Effect, Actions, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
// CRUD
import { QueryResultsModel } from '../../_base/crud';
// Services
import { LevelsService } from '../_services/level.service.fake';
// State
import { IAppState } from '../../../core/reducers';
// Actions
import {
  LevelActionTypes,
  LevelsPageRequested,
  LevelsPageLoaded,
  ManyLevelsDeleted,
  OneLevelDeleted,
  LevelActionToggleLoading,
  LevelsPageToggleLoading,
  LevelUpdated,
  LevelsStatusUpdated,
  LevelCreated,
  LevelOnServerCreated,
} from '../_actions/level.actions';
import { of } from 'rxjs';

@Injectable()
export class LevelEffects {
  showPageLoadingDistpatcher = new LevelsPageToggleLoading({ isLoading: true });
  showActionLoadingDistpatcher = new LevelActionToggleLoading({
    isLoading: true,
  });
  hideActionLoadingDistpatcher = new LevelActionToggleLoading({
    isLoading: false,
  });

  @Effect()
  loadLevelsPage$ = this.actions$.pipe(
    ofType<LevelsPageRequested>(LevelActionTypes.LevelsPageRequested),
    mergeMap(({ payload }) => {
      this.store.dispatch(this.showPageLoadingDistpatcher);
      const requestToServer = this.levelsService.findLevels(payload.page);
      const lastQuery = of(payload.page);
      return forkJoin(requestToServer, lastQuery);
    }),
    map((response) => {
      const result: QueryResultsModel = response[0];
      const lastQuery: QueryParamsModel = response[1];
      const pageLoadedDispatch = new LevelsPageLoaded({
        levels: result.items,
        totalCount: result.totalCount,
        page: lastQuery,
      });
      return pageLoadedDispatch;
    })
  );

  @Effect()
  deleteLevel$ = this.actions$.pipe(
    ofType<OneLevelDeleted>(LevelActionTypes.OneLevelDeleted),
    mergeMap(({ payload }) => {
      this.store.dispatch(this.showActionLoadingDistpatcher);
      return this.levelsService.deleteLevel(payload.id);
    }),
    map(() => {
      return this.hideActionLoadingDistpatcher;
    })
  );

  @Effect()
  deleteLevels$ = this.actions$.pipe(
    ofType<ManyLevelsDeleted>(LevelActionTypes.ManyLevelsDeleted),
    mergeMap(({ payload }) => {
      this.store.dispatch(this.showActionLoadingDistpatcher);
      return this.levelsService.deleteLevels(payload.ids);
    }),
    map(() => {
      return this.hideActionLoadingDistpatcher;
    })
  );

  @Effect()
  updateLevel$ = this.actions$.pipe(
    ofType<LevelUpdated>(LevelActionTypes.LevelUpdated),
    mergeMap(({ payload }) => {
      this.store.dispatch(this.showActionLoadingDistpatcher);
      return this.levelsService.updateLevel(payload.level);
    }),
    map(() => {
      return this.hideActionLoadingDistpatcher;
    })
  );

  @Effect()
  updateLevelsStatus$ = this.actions$.pipe(
    ofType<LevelsStatusUpdated>(LevelActionTypes.LevelsStatusUpdated),
    mergeMap(({ payload }) => {
      this.store.dispatch(this.showActionLoadingDistpatcher);
      return this.levelsService.updateStatusForLevel(
        payload.levels,
        payload.status
      );
    }),
    map(() => {
      return this.hideActionLoadingDistpatcher;
    })
  );

  @Effect()
  createLevel$ = this.actions$.pipe(
    ofType<LevelOnServerCreated>(LevelActionTypes.LevelOnServerCreated),
    mergeMap(({ payload }) => {
      this.store.dispatch(this.showActionLoadingDistpatcher);
      return this.levelsService.createLevel(payload.level).pipe(
        tap((res) => {
          this.store.dispatch(new LevelCreated({ level: res }));
        })
      );
    }),
    map(() => {
      return this.hideActionLoadingDistpatcher;
    })
  );

  constructor(
    private actions$: Actions,
    private levelsService: LevelsService,
    private store: Store<IAppState>
  ) {}
}
