import { QueryParamsModel } from './../../_base/crud/models/query-models/query-params.model';
import { forkJoin } from 'rxjs';
// Angular
import { Injectable } from '@angular/core';
// RxJS
import { mergeMap, map, tap, delay } from 'rxjs/operators';
// NGRX
import { Effect, Actions, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
// CRUD
import { QueryResultsModel } from '../../_base/crud';
// Services
import { ClanLevelsService } from '../_services/clan-level.service.fake';
// State
import { IAppState } from '../../../core/reducers';
// Actions
import {
  ClanLevelActionTypes,
  ClanLevelsPageRequested,
  ClanLevelsPageLoaded,
  ManyClanLevelsDeleted,
  OneClanLevelDeleted,
  ClanLevelActionToggleLoading,
  ClanLevelsPageToggleLoading,
  ClanLevelUpdated,
  ClanLevelsStatusUpdated,
  ClanLevelCreated,
  ClanLevelOnServerCreated,
} from '../_actions/clan-level.actions';
import { of } from 'rxjs';

@Injectable()
export class ClanLevelEffects {
  showPageLoadingDistpatcher = new ClanLevelsPageToggleLoading({
    isLoading: true,
  });
  showActionLoadingDistpatcher = new ClanLevelActionToggleLoading({
    isLoading: true,
  });
  hideActionLoadingDistpatcher = new ClanLevelActionToggleLoading({
    isLoading: false,
  });

  @Effect()
  loadClanLevelsPage$ = this.actions$.pipe(
    ofType<ClanLevelsPageRequested>(
      ClanLevelActionTypes.ClanLevelsPageRequested
    ),
    mergeMap(({ payload }) => {
      this.store.dispatch(this.showPageLoadingDistpatcher);
      const requestToServer = this.clanLevelsService.findClanLevels(
        payload.page
      );
      const lastQuery = of(payload.page);
      return forkJoin(requestToServer, lastQuery);
    }),
    map((response) => {
      const result: QueryResultsModel = response[0];
      const lastQuery: QueryParamsModel = response[1];
      const pageLoadedDispatch = new ClanLevelsPageLoaded({
        clanLevels: result.items,
        totalCount: result.totalCount,
        page: lastQuery,
      });
      return pageLoadedDispatch;
    })
  );

  @Effect()
  deleteClanLevel$ = this.actions$.pipe(
    ofType<OneClanLevelDeleted>(ClanLevelActionTypes.OneClanLevelDeleted),
    mergeMap(({ payload }) => {
      this.store.dispatch(this.showActionLoadingDistpatcher);
      return this.clanLevelsService.deleteClanLevel(payload.id);
    }),
    map(() => {
      return this.hideActionLoadingDistpatcher;
    })
  );

  @Effect()
  deleteClanLevels$ = this.actions$.pipe(
    ofType<ManyClanLevelsDeleted>(ClanLevelActionTypes.ManyClanLevelsDeleted),
    mergeMap(({ payload }) => {
      this.store.dispatch(this.showActionLoadingDistpatcher);
      return this.clanLevelsService.deleteClanLevels(payload.ids);
    }),
    map(() => {
      return this.hideActionLoadingDistpatcher;
    })
  );

  @Effect()
  updateClanLevel$ = this.actions$.pipe(
    ofType<ClanLevelUpdated>(ClanLevelActionTypes.ClanLevelUpdated),
    mergeMap(({ payload }) => {
      this.store.dispatch(this.showActionLoadingDistpatcher);
      return this.clanLevelsService.updateClanLevel(payload.clanLevel);
    }),
    map(() => {
      return this.hideActionLoadingDistpatcher;
    })
  );

  @Effect()
  createClanLevel$ = this.actions$.pipe(
    ofType<ClanLevelOnServerCreated>(
      ClanLevelActionTypes.ClanLevelOnServerCreated
    ),
    mergeMap(({ payload }) => {
      this.store.dispatch(this.showActionLoadingDistpatcher);
      return this.clanLevelsService.createClanLevel(payload.clanLevel).pipe(
        tap((res) => {
          this.store.dispatch(new ClanLevelCreated({ clanLevel: res }));
        })
      );
    }),
    map(() => {
      return this.hideActionLoadingDistpatcher;
    })
  );

  constructor(
    private actions$: Actions,
    private clanLevelsService: ClanLevelsService,
    private store: Store<IAppState>
  ) {}
}
