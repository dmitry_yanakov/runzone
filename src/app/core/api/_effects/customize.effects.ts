import { QueryParamsModel } from './../../_base/crud/models/query-models/query-params.model';
import { forkJoin } from 'rxjs';
// Angular
import { Injectable } from '@angular/core';
// RxJS
import { mergeMap, map, tap, delay } from 'rxjs/operators';
// NGRX
import { Effect, Actions, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
// CRUD
import { QueryResultsModel } from '../../_base/crud';
// Services
import { CustomizesService } from '../_services/customize.services';
// State
import { IAppState } from '../../../core/reducers';
// Actions
import {
  CustomizeActionTypes,
  CustomizesPageRequested,
  CustomizesPageLoaded,
  ManyCustomizesDeleted,
  OneCustomizeDeleted,
  CustomizeActionToggleLoading,
  CustomizesPageToggleLoading,
  CustomizeUpdated,
  CustomizesStatusUpdated,
  CustomizeCreated,
  CustomizeOnServerCreated,
} from '../_actions/customize.actions';
import { of } from 'rxjs';

@Injectable()
export class CustomizeEffects {
  showPageLoadingDistpatcher = new CustomizesPageToggleLoading({
    isLoading: true,
  });
  showActionLoadingDistpatcher = new CustomizeActionToggleLoading({
    isLoading: true,
  });
  hideActionLoadingDistpatcher = new CustomizeActionToggleLoading({
    isLoading: false,
  });

  @Effect()
  loadCustomizesPage$ = this.actions$.pipe(
    ofType<CustomizesPageRequested>(
      CustomizeActionTypes.CustomizesPageRequested
    ),
    mergeMap(({ payload }) => {
      this.store.dispatch(this.showPageLoadingDistpatcher);
      const requestToServer = this.customizesService.findCustomizes(
        payload.page
      );
      const lastQuery = of(payload.page);
      return forkJoin(requestToServer, lastQuery);
    }),
    map((response) => {
      const result: QueryResultsModel = response[0];
      const lastQuery: QueryParamsModel = response[1];
      const pageLoadedDispatch = new CustomizesPageLoaded({
        customizes: result.items,
        totalCount: result.totalCount,
        page: lastQuery,
      });
      return pageLoadedDispatch;
    })
  );

  @Effect()
  deleteCustomize$ = this.actions$.pipe(
    ofType<OneCustomizeDeleted>(CustomizeActionTypes.OneCustomizeDeleted),
    mergeMap(({ payload }) => {
      this.store.dispatch(this.showActionLoadingDistpatcher);
      return this.customizesService.deleteCustomize(payload.id);
    }),
    map(() => {
      return this.hideActionLoadingDistpatcher;
    })
  );

  @Effect()
  deleteCustomizes$ = this.actions$.pipe(
    ofType<ManyCustomizesDeleted>(CustomizeActionTypes.ManyCustomizesDeleted),
    mergeMap(({ payload }) => {
      this.store.dispatch(this.showActionLoadingDistpatcher);
      return this.customizesService.deleteCustomizes(payload.ids);
    }),
    map(() => {
      return this.hideActionLoadingDistpatcher;
    })
  );

  @Effect()
  updateCustomize$ = this.actions$.pipe(
    ofType<CustomizeUpdated>(CustomizeActionTypes.CustomizeUpdated),
    mergeMap(({ payload }) => {
      this.store.dispatch(this.showActionLoadingDistpatcher);
      return this.customizesService.updateCustomize(payload.customize);
    }),
    map(() => {
      return this.hideActionLoadingDistpatcher;
    })
  );

  @Effect()
  updateCustomizesStatus$ = this.actions$.pipe(
    ofType<CustomizesStatusUpdated>(
      CustomizeActionTypes.CustomizesStatusUpdated
    ),
    mergeMap(({ payload }) => {
      this.store.dispatch(this.showActionLoadingDistpatcher);
      return this.customizesService.updateStatusForCustomize(
        payload.customizes,
        payload.status
      );
    }),
    map(() => {
      return this.hideActionLoadingDistpatcher;
    })
  );

  @Effect()
  createCustomize$ = this.actions$.pipe(
    ofType<CustomizeOnServerCreated>(
      CustomizeActionTypes.CustomizeOnServerCreated
    ),
    mergeMap(({ payload }) => {
      this.store.dispatch(this.showActionLoadingDistpatcher);
      return this.customizesService.createCustomize(payload.customize).pipe(
        tap((res) => {
          this.store.dispatch(new CustomizeCreated({ customize: res }));
        })
      );
    }),
    map(() => {
      return this.hideActionLoadingDistpatcher;
    })
  );

  constructor(
    private actions$: Actions,
    private customizesService: CustomizesService,
    private store: Store<IAppState>
  ) {}
}
