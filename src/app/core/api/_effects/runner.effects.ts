import { QueryParamsModel } from './../../_base/crud/models/query-models/query-params.model';
import { forkJoin } from 'rxjs';
// Angular
import { Injectable } from '@angular/core';
// RxJS
import { mergeMap, map, tap, delay } from 'rxjs/operators';
// NGRX
import { Effect, Actions, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
// CRUD
import { QueryResultsModel } from '../../_base/crud';
// Services
import { RunnersService } from '../_services/runner.service.fake';
// State
import { IAppState } from '../../../core/reducers';
// Actions
import {
  RunnerActionTypes,
  RunnersPageRequested,
  RunnersPageLoaded,
  ManyRunnersDeleted,
  OneRunnerDeleted,
  RunnerActionToggleLoading,
  RunnersPageToggleLoading,
  RunnerUpdated,
  RunnersStatusUpdated,
  RunnerCreated,
  RunnerOnServerCreated,
} from '../_actions/runner.actions';
import { of } from 'rxjs';

@Injectable()
export class RunnerEffects {
  showPageLoadingDistpatcher = new RunnersPageToggleLoading({
    isLoading: true,
  });
  showActionLoadingDistpatcher = new RunnerActionToggleLoading({
    isLoading: true,
  });
  hideActionLoadingDistpatcher = new RunnerActionToggleLoading({
    isLoading: false,
  });

  @Effect()
  loadRunnersPage$ = this.actions$.pipe(
    ofType<RunnersPageRequested>(RunnerActionTypes.RunnersPageRequested),
    mergeMap(({ payload }) => {
      this.store.dispatch(this.showPageLoadingDistpatcher);
      const requestToServer = this.runnersService.findRunners(payload.page);
      const lastQuery = of(payload.page);
      return forkJoin(requestToServer, lastQuery);
    }),
    map((response) => {
      const result: QueryResultsModel = response[0];
      const lastQuery: QueryParamsModel = response[1];
      const pageLoadedDispatch = new RunnersPageLoaded({
        runners: result.items,
        totalCount: result.totalCount,
        page: lastQuery,
      });
      return pageLoadedDispatch;
    })
  );

  @Effect()
  deleteRunner$ = this.actions$.pipe(
    ofType<OneRunnerDeleted>(RunnerActionTypes.OneRunnerDeleted),
    mergeMap(({ payload }) => {
      this.store.dispatch(this.showActionLoadingDistpatcher);
      return this.runnersService.deleteRunner(payload.id);
    }),
    map(() => {
      return this.hideActionLoadingDistpatcher;
    })
  );

  @Effect()
  deleteRunners$ = this.actions$.pipe(
    ofType<ManyRunnersDeleted>(RunnerActionTypes.ManyRunnersDeleted),
    mergeMap(({ payload }) => {
      this.store.dispatch(this.showActionLoadingDistpatcher);
      return this.runnersService.deleteRunners(payload.ids);
    }),
    map(() => {
      return this.hideActionLoadingDistpatcher;
    })
  );

  @Effect()
  updateRunner$ = this.actions$.pipe(
    ofType<RunnerUpdated>(RunnerActionTypes.RunnerUpdated),
    mergeMap(({ payload }) => {
      this.store.dispatch(this.showActionLoadingDistpatcher);
      return this.runnersService.updateRunner(payload.runner);
    }),
    map(() => {
      return this.hideActionLoadingDistpatcher;
    })
  );

  @Effect()
  createRunner$ = this.actions$.pipe(
    ofType<RunnerOnServerCreated>(RunnerActionTypes.RunnerOnServerCreated),
    mergeMap(({ payload }) => {
      this.store.dispatch(this.showActionLoadingDistpatcher);
      return this.runnersService.createRunner(payload.runner).pipe(
        tap((res) => {
          this.store.dispatch(new RunnerCreated({ runner: res }));
        })
      );
    }),
    map(() => {
      return this.hideActionLoadingDistpatcher;
    })
  );

  constructor(
    private actions$: Actions,
    private runnersService: RunnersService,
    private store: Store<IAppState>
  ) {}
}
