import { QueryParamsModel } from './../../_base/crud/models/query-models/query-params.model';
import { forkJoin } from 'rxjs';
// Angular
import { Injectable } from '@angular/core';
// RxJS
import { mergeMap, map, tap, delay } from 'rxjs/operators';
// NGRX
import { Effect, Actions, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
// CRUD
import { QueryResultsModel } from '../../_base/crud';
// Services
import { RanksService } from '../_services/rank.service.fake';
// State
import { IAppState } from '../../../core/reducers';
// Actions
import {
  RankActionTypes,
  RanksPageRequested,
  RanksPageLoaded,
  ManyRanksDeleted,
  OneRankDeleted,
  RankActionToggleLoading,
  RanksPageToggleLoading,
  RankUpdated,
  RanksStatusUpdated,
  RankCreated,
  RankOnServerCreated,
} from '../_actions/rank.actions';
import { of } from 'rxjs';

@Injectable()
export class RankEffects {
  showPageLoadingDistpatcher = new RanksPageToggleLoading({ isLoading: true });
  showActionLoadingDistpatcher = new RankActionToggleLoading({
    isLoading: true,
  });
  hideActionLoadingDistpatcher = new RankActionToggleLoading({
    isLoading: false,
  });

  @Effect()
  loadRanksPage$ = this.actions$.pipe(
    ofType<RanksPageRequested>(RankActionTypes.RanksPageRequested),
    mergeMap(({ payload }) => {
      this.store.dispatch(this.showPageLoadingDistpatcher);
      const requestToServer = this.ranksService.findRanks(payload.page);
      const lastQuery = of(payload.page);
      return forkJoin(requestToServer, lastQuery);
    }),
    map((response) => {
      const result: QueryResultsModel = response[0];
      const lastQuery: QueryParamsModel = response[1];
      const pageLoadedDispatch = new RanksPageLoaded({
        ranks: result.items,
        totalCount: result.totalCount,
        page: lastQuery,
      });
      return pageLoadedDispatch;
    })
  );

  @Effect()
  deleteRank$ = this.actions$.pipe(
    ofType<OneRankDeleted>(RankActionTypes.OneRankDeleted),
    mergeMap(({ payload }) => {
      this.store.dispatch(this.showActionLoadingDistpatcher);
      return this.ranksService.deleteRank(payload.id);
    }),
    map(() => {
      return this.hideActionLoadingDistpatcher;
    })
  );

  @Effect()
  deleteRanks$ = this.actions$.pipe(
    ofType<ManyRanksDeleted>(RankActionTypes.ManyRanksDeleted),
    mergeMap(({ payload }) => {
      this.store.dispatch(this.showActionLoadingDistpatcher);
      return this.ranksService.deleteRanks(payload.ids);
    }),
    map(() => {
      return this.hideActionLoadingDistpatcher;
    })
  );

  @Effect()
  updateRank$ = this.actions$.pipe(
    ofType<RankUpdated>(RankActionTypes.RankUpdated),
    mergeMap(({ payload }) => {
      this.store.dispatch(this.showActionLoadingDistpatcher);
      return this.ranksService.updateRank(payload.rank);
    }),
    map(() => {
      return this.hideActionLoadingDistpatcher;
    })
  );

  @Effect()
  updateRanksStatus$ = this.actions$.pipe(
    ofType<RanksStatusUpdated>(RankActionTypes.RanksStatusUpdated),
    mergeMap(({ payload }) => {
      this.store.dispatch(this.showActionLoadingDistpatcher);
      return this.ranksService.updateStatusForRank(
        payload.ranks,
        payload.status
      );
    }),
    map(() => {
      return this.hideActionLoadingDistpatcher;
    })
  );

  @Effect()
  createRank$ = this.actions$.pipe(
    ofType<RankOnServerCreated>(RankActionTypes.RankOnServerCreated),
    mergeMap(({ payload }) => {
      this.store.dispatch(this.showActionLoadingDistpatcher);
      return this.ranksService.createRank(payload.rank).pipe(
        tap((res) => {
          this.store.dispatch(new RankCreated({ rank: res }));
        })
      );
    }),
    map(() => {
      return this.hideActionLoadingDistpatcher;
    })
  );

  constructor(
    private actions$: Actions,
    private ranksService: RanksService,
    private store: Store<IAppState>
  ) {}
}
