import { QueryParamsModel } from './../../_base/crud/models/query-models/query-params.model';
import { forkJoin } from 'rxjs';
// Angular
import { Injectable } from '@angular/core';
// RxJS
import { mergeMap, map, tap, delay } from 'rxjs/operators';
// NGRX
import { Effect, Actions, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
// CRUD
import { QueryResultsModel } from '../../_base/crud';
// Services
import { ActivitiesService } from '../_services/activities.service.fake';
// State
import { IAppState } from '../../../core/reducers';
// Actions
import {
  ActivityActionTypes,
  ActivitiesPageRequested,
  ActivitiesPageLoaded,
  ManyActivitiesDeleted,
  OneActivityDeleted,
  ActivityActionToggleLoading,
  ActivitiesPageToggleLoading,
  ActivityUpdated,
  ActivitiesStatusUpdated,
  ActivityCreated,
  ActivityOnServerCreated,
} from '../_actions/activity.actions';
import { of } from 'rxjs';

@Injectable()
export class ActivityEffects {
  showPageLoadingDistpatcher = new ActivitiesPageToggleLoading({
    isLoading: true,
  });
  showActionLoadingDistpatcher = new ActivityActionToggleLoading({
    isLoading: true,
  });
  hideActionLoadingDistpatcher = new ActivityActionToggleLoading({
    isLoading: false,
  });

  @Effect()
  loadActivitiesPage$ = this.actions$.pipe(
    ofType<ActivitiesPageRequested>(
      ActivityActionTypes.ActivitiesPageRequested
    ),
    mergeMap(({ payload }) => {
      this.store.dispatch(this.showPageLoadingDistpatcher);
      const requestToServer = this.activitiesService.findActivities(
        payload.page
      );
      const lastQuery = of(payload.page);
      return forkJoin(requestToServer, lastQuery);
    }),
    map((response) => {
      const result: QueryResultsModel = response[0];
      const lastQuery: QueryParamsModel = response[1];
      const pageLoadedDispatch = new ActivitiesPageLoaded({
        activities: result.items,
        totalCount: result.totalCount,
        page: lastQuery,
      });
      console.log(pageLoadedDispatch);
      return pageLoadedDispatch;
    })
  );

  @Effect()
  deleteActivity$ = this.actions$.pipe(
    ofType<OneActivityDeleted>(ActivityActionTypes.OneActivityDeleted),
    mergeMap(({ payload }) => {
      this.store.dispatch(this.showActionLoadingDistpatcher);
      return this.activitiesService.deleteActivity(payload.id);
    }),
    map(() => {
      return this.hideActionLoadingDistpatcher;
    })
  );

  @Effect()
  deleteActivities$ = this.actions$.pipe(
    ofType<ManyActivitiesDeleted>(ActivityActionTypes.ManyActivitiesDeleted),
    mergeMap(({ payload }) => {
      this.store.dispatch(this.showActionLoadingDistpatcher);
      return this.activitiesService.deleteActivities(payload.ids);
    }),
    map(() => {
      return this.hideActionLoadingDistpatcher;
    })
  );

  @Effect()
  updateActivity$ = this.actions$.pipe(
    ofType<ActivityUpdated>(ActivityActionTypes.ActivityUpdated),
    mergeMap(({ payload }) => {
      this.store.dispatch(this.showActionLoadingDistpatcher);
      return this.activitiesService.updateActivity(payload.activity);
    }),
    map(() => {
      return this.hideActionLoadingDistpatcher;
    })
  );

  @Effect()
  updateActivitiesStatus$ = this.actions$.pipe(
    ofType<ActivitiesStatusUpdated>(
      ActivityActionTypes.ActivitiesStatusUpdated
    ),
    mergeMap(({ payload }) => {
      this.store.dispatch(this.showActionLoadingDistpatcher);
      return this.activitiesService.updateStatusForActivity(
        payload.activities,
        payload.status
      );
    }),
    map(() => {
      return this.hideActionLoadingDistpatcher;
    })
  );

  @Effect()
  createActivity$ = this.actions$.pipe(
    ofType<ActivityOnServerCreated>(
      ActivityActionTypes.ActivityOnServerCreated
    ),
    mergeMap(({ payload }) => {
      this.store.dispatch(this.showActionLoadingDistpatcher);
      return this.activitiesService.createActivityi(payload.activity).pipe(
        tap((res) => {
          this.store.dispatch(new ActivityCreated({ activity: res }));
        })
      );
    }),
    map(() => {
      return this.hideActionLoadingDistpatcher;
    })
  );

  constructor(
    private actions$: Actions,
    private activitiesService: ActivitiesService,
    private store: Store<IAppState>
  ) {}
}
