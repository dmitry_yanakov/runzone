export class PageConfig {
  public defaults: any = {
    '/admin/dashboard': {
      page: {
        title: 'Dashboard',
        desc: 'Latest updates and statistic charts',
      },
    },
    admin: {
      users: {
        page: { title: 'Users', desc: 'users desc' },
        activity: {
          page: { title: 'Activities', desc: 'activities desc' },
        },
      },
      dashboard: {
        page: { title: 'Dashboard', desc: 'dashboard desc' },
      },
      clans: {
        page: { title: 'Clans', desc: 'clans desc' },
        levels: {
          page: { title: 'Clan Levels', desc: 'levels desc' },
        },
      },
      achievement: {
        page: { title: 'Achievements', desc: 'achievements desc' },
      },
      shop: {
        page: { title: 'Shop Items', desc: 'shop desc' },
      },
      levels: {
        page: { title: 'User Levels', desc: 'user levels desc' },
      },
      eggs: {
        page: { title: 'Eggs', desc: 'eggs desc' },
      },
      runners: {
        page: { title: 'Runners', desc: 'runners desc' },
      },
      customizes: {
        page: { title: 'Customizes', desc: 'customizes desc' },
      },
      ranks: {
        page: { title: 'Ranks', desc: 'ranks desc' },
      },
    },
  };

  public get configs(): any {
    return this.defaults;
  }
}
