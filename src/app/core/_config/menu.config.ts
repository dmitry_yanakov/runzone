export class MenuConfig {
  public defaults: any = {
    header: {
      self: {},
      items: [
        {
          title: 'Dashboards',
          root: true,
          alignment: 'left',
          page: '/dashboard',
          translate: 'MENU.DASHBOARD',
        },
        {
          title: 'Features',
          root: true,
          alignment: 'left',
          toggle: 'click',
          submenu: [
            {
              title: 'eCommerce',
              bullet: 'dot',
              icon: 'flaticon-business',
              permission: 'accessToECommerceModule',
              submenu: [
                {
                  title: 'Customers',
                  page: '/ecommerce/customers',
                },
                {
                  title: 'Products',
                  page: '/ecommerce/products',
                },
              ],
            },
            {
              title: 'User Management',
              bullet: 'dot',
              icon: 'flaticon-user',
              submenu: [
                {
                  title: 'Users',
                  page: '/user-management/users',
                },
                {
                  title: 'Roles',
                  page: '/user-management/roles',
                },
              ],
            },
          ],
        },
        {
          title: 'Apps',
          root: true,
          alignment: 'left',
          page: '/dashboard',
          translate: 'MENU.DASHBOARD',
        },
      ],
    },
    aside: {
      //<i class="fas fa-shield-alt"></i>
      self: {},
      items: [
        {
          title: 'Dashboard',
          root: true,
          icon: 'fas fa-tachometer-alt',
          page: '/admin/dashboard',
          bullet: 'dot',
        },
        { section: 'Users' },
        {
          title: 'Users',
          root: true,
          icon: 'fas fa-users',
          page: '/admin/users',
          bullet: 'dot',
        },
        {
          title: 'Activities',
          root: true,
          icon: 'fas fa-list-ul',
          page: '/admin/users/activity',
          bullet: 'dot',
        },
        { section: 'Clans' },
        {
          title: 'Clans',
          root: true,
          icon: 'fas fa-shield-alt',
          page: '/admin/clans',
          bullet: 'dot',
        },
        {
          title: 'Clan levels',
          root: true,
          icon: 'fas fa-layer-group',
          page: '/admin/clans/levels',
          bullet: 'dot',
        },
        { section: 'Game Elements' },
        {
          title: 'Achievements',
          root: true,
          icon: 'fas fa-award',
          page: '/admin/achievement',
          bullet: 'dot',
        },
        {
          title: 'Shop items',
          root: true,
          icon: 'fas fa-gem',
          page: '/admin/shop',
          bullet: 'dot',
        },
        {
          title: 'User levels',
          root: true,
          icon: 'fas fa-signal',
          page: '/admin/levels',
          bullet: 'dot',
        },
        {
          title: 'Eggs',
          root: true,
          icon: 'fas fa-egg',
          page: '/admin/eggs',
          bullet: 'dot',
        },
        {
          title: 'Customize',
          root: true,
          icon: 'fas fa-tshirt',
          page: '/admin/customizes',
          bullet: 'dot',
        },
        { section: 'Cards' },
        {
          title: 'Runners',
          root: true,
          icon: 'fas fa-cat',
          page: '/admin/runners',
          bullet: 'dot',
        },
        {
          title: 'Ranks',
          root: true,
          icon: 'fas fa-star-half-alt',
          page: '/admin/ranks',
          bullet: 'dot',
        },
      ],
    },
  };

  public get configs(): any {
    return this.defaults;
  }
}
