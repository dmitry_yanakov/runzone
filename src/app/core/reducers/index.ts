// NGRX
import { routerReducer } from '@ngrx/router-store';
import { ActionReducerMap, MetaReducer } from '@ngrx/store';
import { storeFreeze } from 'ngrx-store-freeze';

import { environment } from '../../../environments/environment';

// tslint:disable-next-line:no-empty-interface
export interface IAppState {}

export const reducers: ActionReducerMap<IAppState> = { router: routerReducer };

export const metaReducers: Array<MetaReducer<
  IAppState
>> = !environment.production ? [storeFreeze] : [];
