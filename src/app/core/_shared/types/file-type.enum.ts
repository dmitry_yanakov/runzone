export enum FileType {
  JPG,
  PNG,
  GIF,
  MP4,
  _3GPP,
  M3U8,
}
