import { SortDirectionCollection } from '../../../_shared/types/sort-direction-collection.enum';

export class PaginationModel {
  PageNumber: number;
  PageSize: number;
  StartPosition: number;
  SortField: string;
  SortDirection: SortDirectionCollection;
  SortOrder: string;

  constructor(
    pageNumber: number = 1,
    pageSize: number = 1,
    startPosition: number = 1,
    sortField: string = '',
    sortDirection: SortDirectionCollection = 0,
    sortOrder: string = 'ASC'
  ) {
    this.PageNumber = pageNumber;
    this.PageSize = pageSize;
    this.StartPosition = startPosition;
    this.SortField = sortField;
    this.SortDirection = sortDirection;
    this.SortOrder = sortOrder;
  }
}
