// Angular
import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpResponse, HttpErrorResponse } from '@angular/common/http';
// RxJS
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

/**
 * More information there => https://medium.com/@MetonymyQT/angular-http-interceptors-what-are-they-and-how-to-use-them-52e060321088
 */
@Injectable()
export class InterceptService implements HttpInterceptor {
	intercept(
		req: HttpRequest<any>,
		next: HttpHandler
	  ): Observable<HttpEvent<any>> {
		const token = localStorage.getItem('token');

		const authReq = req.clone({
		  headers: req.headers.set('session', token.replace(/['"]+/g, '')),
		})
		
		return next.handle(authReq).pipe(
		  tap(
			(event) => {
			  if (event instanceof HttpResponse)
				console.log('Server response')
			},
			(err) => {
			  if (err instanceof HttpErrorResponse) {
				if (err.status == 401)
				  console.log('Unauthorized')
			  }
			}
		  )
		)
	}
}
