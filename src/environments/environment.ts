// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  isMockEnabled: false, // You have to switch this, when your real back-end is done
  // apiUrl: 'api',
  apiUrl: 'http://94.130.129.80:53083/api',
  authTokenKey: 'authce9d77b308c149d5992a80073637e4d5',
  configSocialAuth: {
    strava: {
      clientID: '42429',
      secretID: 'dc65ae561877468cd5251a1d6d269f11920726bc',
    },
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
