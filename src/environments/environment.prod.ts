export const environment = {
	production: true,
	isMockEnabled: true, // You have to switch this, when your real back-end is done
  	apiUrl: 'http://94.130.129.80:53083/api',
	authTokenKey: 'authce9d77b308c149d5992a80073637e4d5',
	configSocialAuth: {
		strava: {
		  clientID: '42429',
		  secretID: 'dc65ae561877468cd5251a1d6d269f11920726bc',
		},
	},
};
